package JavaClasses;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import JavaClasses.User;
import org.hibernate.Session;
/**
 *
 * @author User
 */
public class UserManager {
 private Session session = null;
 public UserManager(Session session) {
  if(session == null)
    throw new 
      RuntimeException("Invalid session object.");
  this.session = session;
 }
 public void saveUser(User user){
  session.save(user);
 }
 public void updateUser(User user){
  session.update(user);
 }
 public void deleteUser(User user) {
  session.delete(user);
 }
}
