/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaClasses;

/**
 *
 * @author User
 */
public class Message {
    private int messageId;
    private String userName;
    private String timeSent;
    private String messageContent;

     /**
     * getter
     * @return the message id
     */
    public int getMessageId(){
        return messageId;
    }
     /**
     * getter
     * @return the name of the user
     */
    public String getUserName(){
        return userName;
    }
    /**
     * getter
     * @return password
     */
    public String getTimeSent(){
        return timeSent;
    }
    /**
     * getter
     * @return the real name of the user
     */
    public String getMessageContent(){
        return this.messageContent;
    }
    /**
    * setter
    * @param message id 
    */
    public void setMessageId(int messageId){
        this.messageId = messageId;
    }
   /**
    * setter
    * @param name 
    */
    public void setUserName(String name){
        this.userName = name;
    }
   /**
    * setter
    * @param pass 
    */
    public void setTimeSent(String timeSent){
        this.timeSent = timeSent;
    }
    /**
     * setter
     * @param name 
     */
    public void setMessageContent(String messageContent){
        this.messageContent = messageContent;
    }
}
