package JavaClasses;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
 *
 * @author User
 */
public class ClientsManager {
    
    private static ClientsManager instance = new ClientsManager();
    private Map<String, String> connectedUsers; //user are connecting-username and session id
    
    private ClientsManager()
    {
        this.connectedUsers=new HashMap<String, String>();
    }
    public static ClientsManager getInstance()
    {
        return instance;
    }
    public  Session getSession() throws Exception
    {
        Configuration configuration = null;
        
        configuration = new Configuration().configure();
        
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                applySettings(configuration.getProperties());
        SessionFactory sessionFactory = null;
        sessionFactory = configuration.buildSessionFactory(builder.build());
        Session session = sessionFactory.openSession();
        return session;
    }
    public String getUserSessionID(String userName)
    {
        if(this.connectedUsers.containsKey(userName))
        {
            return this.connectedUsers.get(userName);
        }
        return null;
    }
    public  int isUserNameExist(String userName) throws Exception
    {
        
        Session session = getSession();
        String hqlSt =  "from User u where u.userName = :userName";
        Query query = session.createQuery(hqlSt);
        query.setParameter("userName",userName);
        //List<User> results = query.list();
        User user = (User)query.uniqueResult();
        session.flush();
        if(user == null)
            return 0;
        else
            return 1;
        
        
    }
    public  void addNewUser(String userName, String password, String realName, String email, int type) throws Exception
    {
        User user = new User();
        user.setUserName(userName);
        user.setPassword(password);
        user.setRealName(realName);
        user.setEmail(email);
        user.setType(type);
        Session session = getSession();
        UserManager manager = new UserManager(session);
        manager.saveUser(user);
        session.flush();
    }
    public  boolean isUsernameMatchPassword(String userName, String password) throws Exception
    {
        Session session = getSession();
        String hqlSt =  "from User u where u.userName = :userName";
        Query query = session.createQuery(hqlSt);
        query.setParameter("userName",userName);
        User user = (User)query.uniqueResult();
        session.flush();
        if(user.getPassword().equals(password))
            return true;
        else return false;
    }
    public  void connectUser(String username, String sessionID)
    {
        this.connectedUsers.put(username, sessionID);
    }
    public  void disconnectUser(String username)
    {
        this.connectedUsers.remove(username);
    }
    public  boolean isUserConnected(String userName)
    {
        return this.connectedUsers.containsKey(userName);
    }
}
