package JavaClasses;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class User {
    private String userName;
    private String password;
    private String realName;
    private String email;
    private int type;
     /**
     * getter
     * @return the name of the user
     */
    public String getUserName(){
        return userName;
    }
    /**
     * getter
     * @return password
     */
    public String getPassword(){
        return password;
    }
    /**
     * getter
     * @return the real name of the user
     */
    public String getRealName(){
        return realName;
    }
    
     /**
     * getter
     * @return the email of the user
     */
    public String getEmail() {
        return email;
    }
    
    /**
     * getter
     * @return the type of the user
     */
    public int getType() {
        return type;
    }
    
   /**
    * setter
    * @param name 
    */
    public void setUserName(String name){
        userName = name;
    }
   /**
    * setter
    * @param pass 
    */
    public void setPassword(String pass){
        password = pass;
    }
    /**
     * setter
     * @param name 
     */
    public void setRealName(String name){
        realName = name;
    }
    
    /**
     * setter
     * @param email 
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
   /**
    * setter
    * @param type 
    */
    public void setType(int type) {
        this.type =type;
    }
}
