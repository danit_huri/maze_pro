/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaClasses;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *give service of functions on the session user
 * @author עפרה
 */
public class SessionListener implements HttpSessionListener{

    private static HashMap<String, HttpSession> mySessions = new HashMap<String, HttpSession>();
    /**
     * occured when the session is create
     * @param se session event
     */
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        mySessions.put(se.getSession().getId(), se.getSession());
    }
/**
 * occured when the session is destroyed
 * @param se session event
 */
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        
        String sessionUsername = (se.getSession().getAttribute("user").toString());
       ClientsManager.getInstance().disconnectUser(sessionUsername);
    }
    
    /**
     * destroy the session
     * @param sessionID the sessoin id for deleting
     */
    public static void destroySession(String sessionID) {
        if (mySessions.containsKey(sessionID)) {
            mySessions.get(sessionID).invalidate();
        }
        mySessions.remove(sessionID);
    }
    
}
