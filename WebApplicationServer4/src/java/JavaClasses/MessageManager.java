/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaClasses;

import org.hibernate.Session;

/**
 *
 * @author User
 */
public class MessageManager {
     private Session session = null;
 public MessageManager(Session session) {
  if(session == null)
    throw new 
      RuntimeException("Invalid session object.");
  this.session = session;
 }
 public void saveMessage(Message mssg){
  session.save(mssg);
 }
 public void updateMessage(Message mssg){
  session.update(mssg);
 }
 public void deleteMessage(Message mssg) {
  session.delete(mssg);
 }
}
