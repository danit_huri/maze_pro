/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Servlets;

import JavaClasses.ClientsManager;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 *
 * @author User
 */
public class LoginServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>POST</code> method.
     * check if user exist in database - if does redirect to main menu,
     * otherwise - return request with error attribute
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pass = request.getParameter("password");
        String userName = request.getParameter("username");
        ClientsManager clientsManager = ClientsManager.getInstance();
        
        JSONObject obj = new JSONObject();
        boolean isMatch = false;
        try{
            isMatch = clientsManager.isUsernameMatchPassword(userName, pass);
        } catch (Exception e) {
            response.getWriter().write(obj.toString());
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("application/json");
            response.getWriter().flush();
        }
        
        if(isMatch)
        {
            HttpSession session =  request.getSession(false);
            if (session!=null) {
                session.invalidate();
            }
            session =  request.getSession(true);
            
            // session.setAttribute("user", clientsManager.getUserSessionID(userName));
            session.setAttribute("valid", "realSession");
            //connecting to the server
            clientsManager.connectUser(userName, session.getId());
            //send answer to mobile
            obj.put("Username", userName);
            obj.put("Password", pass);
        }
        
        response.getWriter().write(obj.toString());
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json");
        response.getWriter().flush();
        
    }
    
    
}
