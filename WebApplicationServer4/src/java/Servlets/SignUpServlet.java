package Servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import JavaClasses.ClientsManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 *
 * @author User
 */
public class SignUpServlet extends HttpServlet {


    /**
     * Handles the HTTP <code>GET</code> method.
     * if session is on - redirect to session is on in login page
     * if session is off - redirect to sign u form jsp
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    
        
      
      
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ClientsManager clientsManager =  ClientsManager.getInstance();
        //extracting form values
        
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        String realName = request.getParameter("realname");
        String email = request.getParameter("email");
        int type =1;//Integer.parseInt(request.getParameter("type"));
        
       /** String userName = "hi";
        String password = "hi";
        String realName = "hi";
        String email = "hi";
        int type =1;
        * **/
        JSONObject obj = new JSONObject();
        //hecking if username exist in database
         int isExist=-1;
        try{
        isExist = clientsManager.isUserNameExist(userName);
        }
        catch(Exception e)
        {
         String s = e.getMessage();
         String s2 ;
         obj.put("error", s);
         StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
       s2 = errors.toString();
         obj.put("trace", s2);
         response.getWriter().write(obj.toString());
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("application/json");
            response.getWriter().flush();
        }
        if(isExist == 0)
        {
            //creating new user and opening new session
            HttpSession session = request.getSession(false);
            if (session!=null) {
                //there is an active session
                session.invalidate();
            } 
            session = request.getSession(true);
            try{
            clientsManager.addNewUser(userName, password, realName, email, type);
            } catch(Exception e)
            {
                
            }
            clientsManager.connectUser(userName, session.getId());
            session.setAttribute("user", userName);
            session.setAttribute("valid", "realSession");
            //send answer to mobile
            obj.put("Username", userName);
            obj.put("Password", password);
        } else if (isExist == 1) {
            obj.put("error", "exist");
        }    else {
            obj.put("error", "sql");
        }
        response.getWriter().write(obj.toString());
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("application/json");
            response.getWriter().flush();
    }

}
