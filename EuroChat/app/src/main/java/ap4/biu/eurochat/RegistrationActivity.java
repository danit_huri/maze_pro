package ap4.biu.eurochat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import ap4.biu.androidclient.R;

public class RegistrationActivity extends AppCompatActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    //UI
    private Button mReset;
    private EditText mEmailView;
    private EditText mUsernameView;
    private EditText mPasswordView;
    private EditText mNameView;
    private View mProgressView;
    private View mLoginFormView;
    private RadioGroup mRadioGroup;
    private RadioButton[] mRadioButtons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setCustomView(R.layout.abs_layout);
                getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.ab_login_bg));
            }
        });
        setContentView(R.layout.activity_registration);


        mUsernameView = (EditText) findViewById(R.id.username);
        mPasswordView = (EditText) findViewById(R.id.password);
        mEmailView = (EditText) findViewById(R.id.email);
        mNameView = (EditText) findViewById(R.id.realname);

        mRadioButtons = new RadioButton[4];
        mRadioButtons[0] = (RadioButton)  findViewById(R.id.radio1);
        mRadioButtons[1] = (RadioButton)  findViewById(R.id.radio2);
        mRadioButtons[2] = (RadioButton)  findViewById(R.id.radio3);
        mRadioButtons[3] = (RadioButton)  findViewById(R.id.radio4);

        mRadioGroup = (RadioGroup) findViewById(R.id.radioG);

        mReset = (Button)findViewById(R.id.reset_button);

        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUsernameView.setText("");
                mPasswordView.setText("");
                mEmailView.setText("");
                mNameView.setText("");
                for (int k=0; k<4; k++) {
                    mRadioButtons[k].setChecked(false);
                }
            }
        });

        Button mSignUpButton = (Button) findViewById(R.id.rgs_button);
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegistartion();
            }
        });

        mLoginFormView = findViewById(R.id.signup_form);
        mProgressView = findViewById(R.id.signup_progress);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptRegistartion() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();
        String email = mEmailView.getText().toString();
        String name = mNameView.getText().toString();

        int iconNumber = mRadioGroup.getCheckedRadioButtonId();

        View focusView = null;

        focusView = validateForm(username, password, email, name, iconNumber);

        if (focusView!=null) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            //extract the right icon selected
            for (int k=0; k<4; k++) {
                if(mRadioButtons[k].getId() == iconNumber) {
                    iconNumber = k+1;
                }
            }

            showProgress(true);
            mAuthTask = new UserLoginTask(username, password, name, email, iconNumber);
            mAuthTask.execute((Void) null);
        }
    }

    private View validateForm(String username, String password, String email, String name, int iconNumber) {
        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            return mPasswordView;
        }
        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            return mUsernameView;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            return mEmailView;
        } else if (!email.contains("@")) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            return mEmailView;
        }

        // Check for a valid name.
        if (TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_field_required));
            return mNameView;
        }

        //check for icons
        if (iconNumber == -1)
        {
            return mRadioGroup;
        }

        return null;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    class User{
        private String name;
        private String pass;

        public User(JSONObject object){

            try {
                this.name = object.getString("Username");
                Log.i("username name", this.name);
                this.pass = object.getString("Password");
                Log.i("username password", this.pass);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public String getName() {
            return name;
        }

        public String getPassword() {
            return pass;
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, User> {

        private final String mUsername;
        private final String mPassword;
        private final String mName;
        private final String mEmail;
        private final int mIcon;


        UserLoginTask(String username, String password, String name, String email, int iconNumber) {
            mUsername = username;
            mPassword = password;
            mName = name;
            mEmail = email;
            mIcon = iconNumber;
        }

        @Override
        protected User doInBackground(Void... params) {

            try {
                // Simulate network access.
                URL url = new URL(getResources().getString(R.string.url_signUp));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    urlConnection.setConnectTimeout(4000);
                    urlConnection.setRequestMethod(getResources().getString(R.string.post));

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter(getResources().getString(R.string.prompt_username), mUsername)
                            .appendQueryParameter(getResources().getString(R.string.prompt_password), mPassword)
                            .appendQueryParameter(getResources().getString(R.string.prompt_realname_full), mName)
                            .appendQueryParameter(getResources().getString(R.string.prompt_email), mEmail)
                            .appendQueryParameter(getResources().getString(R.string.prompt_type), String.valueOf(mIcon));
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = urlConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, getResources().getString(R.string.utf)));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    Boolean worked = false;
                    InputStream in = new BufferedInputStream(new InputStream() {
                        @Override
                        public int read() throws IOException {
                            return 0;
                        }
                    });
                    while (!worked) {
                        try {
                            in = new BufferedInputStream(urlConnection.getInputStream());
                        } catch (EOFException e) {
                            e.printStackTrace();
                            worked = false;
                            continue;
                        }
                        worked = true;
                    }

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, getResources().getString(R.string.utf)));
                    StringBuilder responseStrBuilder = new StringBuilder();

                    String inputStr;
                    while ((inputStr = streamReader.readLine()) != null)
                        responseStrBuilder.append(inputStr);

                    JSONObject json = new JSONObject(responseStrBuilder.toString());

                    return new User(json);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }

                //Thread.sleep(2000);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final User user) {
            mAuthTask = null;
            showProgress(false);

            if (user != null) {
                if (user.getName() != null && user.getPassword() != null) {
                    // Store preferences
                    SharedPreferences.Editor settingsEditor = getSharedPreferences(getResources().getString(R.string.sh_preferences), 0).edit();
                    settingsEditor.putString(getResources().getString(R.string.prompt_username), user.getName());
                    settingsEditor.putString(getResources().getString(R.string.prompt_password), user.getPassword());
                    settingsEditor.commit();
                    Intent i = new Intent(RegistrationActivity.this, ForumActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(RegistrationActivity.this, getResources().getString(R.string.error_signUp), Toast.LENGTH_LONG).show();
                }
            } else
            {
                Toast.makeText(RegistrationActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }





}
