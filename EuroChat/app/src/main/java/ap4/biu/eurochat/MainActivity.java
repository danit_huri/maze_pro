package ap4.biu.eurochat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import ap4.biu.androidclient.R;

public class MainActivity extends AppCompatActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    Intent i;
    int counter;
    CharSequence[] loaderTxt;
    TextView loader;
    boolean firstTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        SharedPreferences mDefaultPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (mDefaultPreferences.getBoolean(getResources().getString(R.string.first), true))
        {
            mDefaultPreferences.edit().putBoolean(getResources().getString(R.string.first), false).commit();
            //Put the code here of your first launch
            firstTime = true;
        }
        else
        {
            //Not the first launch
            firstTime = false;
        }
        loaderTxt = new CharSequence[3];
        loaderTxt[0] = (getResources().getString(R.string.loader2));
        loaderTxt[1] = (getResources().getString(R.string.loader3));
        loaderTxt[2] = (getResources().getString(R.string.loader4));

        setContentView(R.layout.activity_main);

        loader = (TextView) findViewById(R.id.bar);

        startProgressBar();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void startProgressBar() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {


                //go to login activity by default
                i = new Intent(MainActivity.this, LoginActivity.class);
                if (firstTime) {
                    //go to explanation activity
                    i = new Intent(MainActivity.this, ExplanationActivity.class);
                } else {
                    //go to connection
                    SharedPreferences settings = getSharedPreferences(getResources().getString(R.string.sh_preferences), 0);
                    if (settings.getString(getResources().getString(R.string.prompt_username), null) != null){
                        //try to log in
                        Log.i("in main", settings.getString("username", null));
                        Log.i("in main", settings.getString("password", null));
                        mAuthTask = new UserLoginTask(settings.getString(getResources().getString(R.string.prompt_username), null), settings.getString(getResources().getString(R.string.prompt_password), null));
                        mAuthTask.execute((Void) null);
                    }
                }

                try {
                    Thread.sleep(1000);

                } catch (InterruptedException e) {}
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        loader.setText(loaderTxt[counter]);
                        synchronized(this)
                        {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {}
                            this.notify();
                        }
                    }
                };

                counter=0;

                while (counter<3) {

                    synchronized (myRunnable) {
                        runOnUiThread(myRunnable);
                        try {
                            myRunnable.wait(); // unlocks myRunable while waiting
                        } catch (Exception e) {
                        }
                    }
                    counter++;
                }


                startActivity(i);

            }
        });
        t.start();
    }


    class User{
        private String name;
        private String pass;

        public User(JSONObject object){

            try {
                this.name = object.getString(getResources().getString(R.string.json_username));
                Log.i("username name", this.name);
                this.pass = object.getString(getResources().getString(R.string.json_password));
                Log.i("username password", this.pass);

            } catch (JSONException e) {
            }
        }

        public String getName() {
            return name;
        }

        public String getPassword() {
            return pass;
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, User> {

        private final String mUsername;
        private final String mPassword;

        UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected User doInBackground(Void... params) {

            try {
                // Simulate network access.
                URL url = new URL(getResources().getString(R.string.url_login));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    urlConnection.setConnectTimeout(4000);
                    urlConnection.setRequestMethod(getResources().getString(R.string.post));

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter(getResources().getString(R.string.prompt_username), mUsername)
                            .appendQueryParameter(getResources().getString(R.string.prompt_password), mPassword);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = urlConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, getResources().getString(R.string.utf)));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, getResources().getString(R.string.utf)));
                    StringBuilder responseStrBuilder = new StringBuilder();

                    String inputStr;
                    while ((inputStr = streamReader.readLine()) != null)
                        responseStrBuilder.append(inputStr);

                    JSONObject json = new JSONObject(responseStrBuilder.toString());

                    return new User(json);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(final User user) {
            mAuthTask = null;

            if (user != null && user.getName() != null && user.getPassword() != null) {
                // Store preferences
                SharedPreferences.Editor settingsEditor = getSharedPreferences(getResources().getString(R.string.sh_preferences), 0).edit();
                settingsEditor.putString(getResources().getString(R.string.prompt_username), user.getName());
                settingsEditor.putString(getResources().getString(R.string.prompt_password), user.getPassword());
                settingsEditor.commit();
                i = new Intent(MainActivity.this, ForumActivity.class);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }
}
