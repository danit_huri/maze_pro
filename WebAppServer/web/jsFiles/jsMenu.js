// this file is for main menu page

/**
 * asks for name for the game from the user
 * @returns {Boolean} true= user enter a name, false= otherwise
 */
function getName() {
    var gName = prompt("Please enter your name for the game", "GAME NAME");
    
    if (gName !== null) {
        document.getElementById("gameName").value = gName;
        return true;
    } else {
        return false;
    }
}