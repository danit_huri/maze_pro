// this file is for signup form

/**
 * this function validates that all the input parts in the form are filled
 * @returns {Boolean} true= form is ok, false=missing parameters
 */
function validate()
{
    var error = "Missing parameter/s";
    var uName = document.getElementById("username");
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var rName = document.getElementById("realname");
    if( password.value === "" || uName.value=== "" || email.value === "" || rName.value === "" )
    {
        document.getElementById("pError").innerHTML = error;
        return false;
    } else if (document.getElementById("check1").checked || document.getElementById("check2").checked || document.getElementById("check3").checked)
    {
        return true;
    } else {
        document.getElementById("pError").innerHTML = error;
        return false;
    }
}

/**
 * reset the error messages if are shown
 * @returns {undefined}
 */
function resetErr(){
    var empty = " ";
    document.getElementById("pError").innerHTML = empty;
    document.getElementById("uError").innerHTML = empty;
}
/**
 * this function make sure that maximum one checkbox is selected at a time
 * @param {type} id = id of the chosen checkbox
 * @returns {undefined}
 */
function selectOnlyThis(id) {
    if (document.getElementById(id).checked === true){
        for (var i = 1;i <= 3; i++)
        {
            document.getElementById("check" + i).checked = false;
        }
        document.getElementById(id).checked = true;
    }
}


