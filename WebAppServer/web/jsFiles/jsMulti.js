// this file is for multiplay game

//indicates if game finished by one of the players
var winner = false;
var loser = false;

/**
 * this function starts when page load.
 * it askes for the json object to start the game.
 * when json recieved - parsing it into 2 boards.
 * and asks for json movement of opponent
 * @param {type} $
 * @returns {undefined}
 */
$(function($) {
    function getMyGame() {
        $.getJSON('../secured/multiplay', function(multiplay) {
            //inserting the start and goal points into the maze string
            var m = 6, n=6;
            var maze = multiplay.You.Maze;
            var sYouPos = multiplay.You.Start.Row *( 2* m-1 ) * 2 + multiplay.You.Start.Col * 2;
            var sOtherPos = multiplay.Other.Start.Row *( 2* m-1 ) * 2 + multiplay.Other.Start.Col * 2;
            var ePos = multiplay.You.End.Row * (2 * m - 1) * 2 + multiplay.You.End.Col * 2;
            var newYouMaze = "", newOtherMaze = "";  
            for (var i=0; i<maze.length; i++) {
                if (i === sYouPos) {
                    //4 for start point
                    newYouMaze += '4';
                    newOtherMaze += maze[i];
                } else if (i === ePos){
                    //5 for goal point
                    newYouMaze += '5';
                    newOtherMaze += '5';
                } else if (i === sOtherPos) {
                    newOtherMaze += '4';
                    newYouMaze += maze[i];
                } else {
                    newYouMaze += maze[i];
                    newOtherMaze += maze[i];
                }
            }            
            //parsing mazes strings into 2 tables
            var myYouTable = makeYouTable(newYouMaze);
            var myOtherTable = makeOtherTable(newOtherMaze);
            //change loading imges to the tables
            $('#loadingOther').fadeOut('slow');
            $('#loading').fadeOut('slow', function() {
                $('#loadingOther').remove();
                $('#loading').remove();
                $('#divGame').append(myOtherTable);
                $('#divGame').append(myYouTable);
            });
            //call for waiting to opp's movement
            getOppMovement();
        });
    }
    getMyGame();
});

/**
 * parse this player string maze into table
 * @param {type} maze
 * @returns {makeYouTable.table|$} return parsed maze into table
 */
function makeYouTable(maze) {
    var table = $("<table id=\"mazeYouTbl\" cellspacing=\"0\">");
    for (var k=0,i= 0; i<11; i++ ) {
        //new line in table
        var row = $("<tr>");
        for (var j=0; j<11; j++,k+=1) {
            //new column in table
            if (maze[k] === '4') { 
                row.append($("<td class=\"cell\" id=\"startCell\"><img  id=\"imgR"+i+"C"+j+"\" class=\"cell43\"></label></td>"));
            } else if (maze[k] === '5')
            { 
                row.append($("<td class=\"cell\" id=\"endCell\"><img  id=\"imgR"+i+"C"+j+"\" class=\"cell5\"></label></td>"));
            } else { 
                row.append($("<td class=\"cell\"><img  id=\"imgR"+i+"C"+j+"\" class=\"cell"+maze[k]+"\"></label></td>"));
            }
        }
        row.append($("</tr>"));
        table.append(row);
    }
    table.append($("</table>"));
    return table;
}

/**
 * parse other player string maze into table
 * @param {type} maze
 * @returns {makeOtherTable.table|$} return parsed maze into table
 */
function makeOtherTable(maze) {
    var table = $("<table id=\"mazeOtherTbl\" cellspacing=\"0\">");
    for (var k=0,i= 0; i<11; i++ ) {
        //new line in table
        var row = $("<tr>");
        for (var j=0; j<11; j++,k+=1) {
            //new column in table
            if (maze[k] === '4') { 
                row.append($("<td class=\"cell\" id=\"oStartCell\"><img  id=\"oImgR"+i+"C"+j+"\" class=\"oCell4\"></label></td>"));
            } else if (maze[k] === '5')
            { 
                row.append($("<td class=\"cell\" id=\"oEndCell\"><img  id=\"oImgR"+i+"C"+j+"\" class=\"oCell5\"></label></td>"));
            } else { 
                row.append($("<td class=\"cell\"><img  id=\"oImgR"+i+"C"+j+"\" class=\"oCell"+maze[k]+"\"></label></td>"));
            }
        }
        row.append($("</tr>"));
        table.append(row);
    }
    table.append($("</table>"));
    return table;
}

/**
 * ask for the next opp movement
 * @returns {undefined}
 */
function getOppMovement(){ 
    $.getJSON('../secured/recieverServlet', function(oppMovement) {
        var move = oppMovement.Move;
        var tempCell = document.getElementsByClassName("oCell4");
        var currentOppCell; 
        if (tempCell.length !== 0) {
            currentOppCell = tempCell[0];
        }
        if (currentOppCell != null) {
            var colOffset = 0, lineOffset = 0;
            if (!winner && !loser) {
                switch (move) {
                    case "up\r\n": lineOffset =1;
                        break;
                    case "down\r\n": lineOffset = -1;
                        break;
                    case "right\r\n": colOffset = -1;
                        break;
                    case "left\r\n": colOffset = 1;
                        break;
                }
            }
            var id, line = "", column="";
            var cFound = false;
            var toCheckId = "oImgR";
            id = currentOppCell.id;
            for (var i=5; i<id.length; i++) {
                if (cFound) {
                    column += id[i];
                } else {
                    if(id[i] === 'C') {
                        cFound = true;
                        line = line - lineOffset;
                        toCheckId += line + id[i];   
                    } else {
                        line += id[i];
                    }
                }
            }
            column = column - colOffset;
            toCheckId += column;
        }
        //moving the opp player
        moveOppPlayer(currentOppCell, document.getElementById(toCheckId));
        //ask again if game did not end
        if (!winner && !loser) {
            getOppMovement();
        }
    });
}

/**
 * mmove opponent player to the new location
 * @param {type} currentLocation
 * @param {type} newLocation
 * @returns {undefined}
 */
function moveOppPlayer(currentLocation, newLocation) {
    currentLocation.className = "oCell2";
    if (newLocation.className !== "oCell5") {
        //regular cell
        newLocation.className = "oCell4"; 
    } else {
        //finish game
        newLocation.className = "oCell6";
        document.getElementById("finish").innerHTML = "GAME OVER! Opponent won the game!";
        loser = true;
    }
}

/**
 * ask for hint for this user
 * @param {type} param
 */
$("#hintBtn").click(function(){
    var myTable = document.getElementById("mazeYouTbl");
    var isFinish = document.getElementsByClassName("cell7");
    if (myTable !== null && isFinish.length === 0) {
        //get table into string
        var currentMaze = "";
        for (var i = 0, row; row = myTable.rows[i]; i++) {
            for (var j = 0, col; col = row.cells[j]; j++) {
                var val = (col.childNodes[0].className)[4];
                currentMaze += val;
            }  
        }
        
        //post current maze with and type of game for the hint servlet
        $.post("../secured/HintServlet",
        {
            currentMaze: currentMaze,
            type: "multi"
        },
        function(data, status){
            var hintImgId = "imgR";
            for (var i = 0; i<data.length-1; i++) {
                if (data[i] === ',') {
                    hintImgId += 'C';
                } else {
                    hintImgId += data[i];
                }
            }
            var hintImg = $("#"+hintImgId)[0];
            var preClassName = hintImg.className;
            
            
            var start ,limit;
            if (preClassName === "cell2" || preClassName === "cell3")  {
                start = 4;
                limit = 9;
            } else if (preClassName === "cell0" || preClassName === "cell5") {
                start = 0; 
                limit = 5;
            } else {
                return;
            }
            var counter = start;
            var flag = true;
            //display hint
            var interval = window.setInterval(function (){
                counter++;
                if(counter < limit) {
                    hintImg.className = "hint"+counter.toString();
                    if (counter === limit-1 && flag) {
                        counter = start;
                        flag = false;
                    }
                } else {
                    hintImg.className = preClassName;
                    clearInterval(interval);
                }
            }, 300);
            
        });
    }
});

/**
 * asks user if wants to restart game
 * @returns {undefined}
 */
function restart()
{
    var decision = popUp("restart the game");
    if (decision === true) {
        closeGame();
    } 
}
/**
 * asks user if wants to return to main menu
 * @returns {undefined}
 */
function returnHome()
{
    var decision = popUp("return to main menu");
    if (decision === true) {
        closeGame();
    }
}
/**
 * asking for closing multiplay game for this user
 * @returns {undefined}
 */
function closeGame(){
    //post to close game
    $.post("../secured/closeServlet", {},function(data, status) {});
    //go back to main menu
    history.go(-1);
}
/**
 * pop up a confirm window with the given message
 * @param {type} message to the confirm window
 * @returns {unresolved}
 */
function popUp (message){
    return confirm("Are you sure you want to " + message +"?");
}
/**
 * use this function when user click on keyboard 
 * @param {type} event
 * @returns {undefined}
 */
function keyPressed(event) {
    var currentCell = document.getElementById("startCell");
    var isFinish = document.getElementsByClassName("cell7");
    if (currentCell !== null && isFinish.length === 0) {
        //check if its a key for an arrow
        switch (event.code) {
            case "ArrowLeft": keyLeft();
                break;
            case "ArrowRight": keyRight();
                break;
            case "ArrowUp": keyUp();
                break;
            case "ArrowDown": keyDown();
                break;
        }
    }
}
/**
 * try to move this player from currentLocation to newLocation
 * @param {type} currentLocation 
 * @param {type} newLocation
 * @param {type} where = direction
 * @returns {Boolean} true = moved, false = stayed in same place 
 */
function movePlayer(currentLocation, newLocation, where) {
    if (!winner && !loser){
        if (newLocation.className !== "cell1" && newLocation.className !== "cell7") { 
            //can walk
            if (newLocation.className !== "cell5") {
                //regular cell
                if (newLocation.className === "cell0") {
                    currentLocation.className = "cell2";
                } else if (newLocation.className === "cell2"){
                    currentLocation.className = "cell3";
                } else if (newLocation.className === "cell3"){
                    currentLocation.className = "cell2";
                }
                newLocation.className = "cell4"+where; 
            } else {
                //finish game
                newLocation.className = "cell6";
                currentLocation.className = "cell7";
                document.getElementById("finish").innerHTML = "Congratulations! You solved the maze!";
                winner = true;
            }
            return true;
        } else if (newLocation.className === "cell1") {
            currentLocation.className = "cell4"+where;
        }
    }
    return false;
}
/**
 * user clicked left arrow key on keyboard
 * @returns {undefined}
 */
function keyLeft(){
    var currentCell; 
    for (var i=1; i<=4; i++){
        var classname = "cell4" + i.toString(); 
        var tempCell = document.getElementsByClassName(classname);
        if (tempCell.length !== 0) {
            currentCell = tempCell[0];
            break;
        } 
    }
    var sibNode = currentCell.parentNode.previousElementSibling;
    if (sibNode !== null) {
        var toCheckCell = sibNode.childNodes[0];
        if (toCheckCell !== null) {
            var moved = movePlayer(currentCell, toCheckCell, "4");
            if (moved) {
                //update server in movement
                $.post("../secured/senderServlet",
                {
                    movement: "left"
                }, function(data, status) {});
            }
        }
    } else {
        currentCell.className = "cell44";
    }
}
/**
 * user clicked right arrow key on keyboard
 * @returns {undefined}
 */
function keyRight(){
    var currentCell; 
    for (var i=1; i<=4; i++){
        var classname = "cell4" + i.toString(); 
        var tempCell = document.getElementsByClassName(classname);
        if (tempCell.length !== 0) {
            currentCell = tempCell[0];
            break;
        } 
    }
    var sibNode = currentCell.parentNode.nextElementSibling;
    if (sibNode !== null) {
        var toCheckCell = sibNode.childNodes[0];
        if (toCheckCell !== null) {
            var moved = movePlayer(currentCell, toCheckCell, "2");
            if (moved) {
                //update server in movement
                $.post("../secured/senderServlet",
                {
                    movement: "right"
                }, function(data, status) {});
            }
        }
    } else {
        currentCell.className = "cell42";
    }
}
/**
 * user clicked up arrow key on keyboard
 * @returns {undefined}
 */
function keyUp(){
    var currentCell; 
    for (var i=1; i<=4; i++){
        var classname = "cell4" + i.toString(); 
        var tempCell = document.getElementsByClassName(classname);
        if (tempCell.length !== 0) {
            currentCell = tempCell[0];
            break;
        } 
    }
    
    var id, line = "";
    var cFound = false;
    var toCheckId = "imgR";
    id = currentCell.id;
    for (var i=4; i<id.length; i++) {
        if (cFound) {
            toCheckId += id[i];
        } else {
            if(id[i] === 'C') {
                cFound = true;
                line = line - 1;
                toCheckId += line + id[i];   
            } else {
                line += id[i];
            }
        }
    }
    var toCheckCell = document.getElementById(toCheckId);
    if (toCheckCell !== null) {
        var moved = movePlayer(currentCell, toCheckCell, "1");
        if (moved) {
             //update server in movement
            $.post("../secured/senderServlet",
            {
                movement: "up"
            }, function(data, status) {});
        }
    } else {
        currentCell.className = "cell41";
    }
}
/**
 * user clicked down arrow key on keyboard
 * @returns {undefined}
 */
function keyDown(){
    var currentCell; 
    for (var i=1; i<=4; i++){
        var classname = "cell4" + i.toString(); 
        var tempCell = document.getElementsByClassName(classname);
        if (tempCell.length !== 0) {
            currentCell = tempCell[0];
            break;
        } 
    }
    var id, line = "";
    var cFound = false;
    var toCheckId = "imgR";
    id = currentCell.id;
    for (var i=4; i<id.length; i++) {
        if (cFound) {
            toCheckId += id[i];
        } else {
            if(id[i] === 'C') {
                cFound = true;
                line = line - 1;
                line += + 2;
                toCheckId += line + id[i];   
            } else {
                line += id[i];
            }
        }
    }
    var toCheckCell = document.getElementById(toCheckId);
    if (toCheckCell !== null) {
        var moved = movePlayer(currentCell, toCheckCell, "3");
        if (moved) {
            //update server in movement
            $.post("../secured/senderServlet",
            {
                movement: "down"
            }, function(data, status) {});
        }
    } else {
        currentCell.className = "cell43";
    }
}