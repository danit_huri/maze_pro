// this file is for login page when session is active

/**
 * redirect window to the main menu servlet
 * @returns {undefined}
 */
function logIn() {
    window.location.href = "secured/gameMenu";
}