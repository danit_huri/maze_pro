// this file is for singleplay game

/**
 * this function starts when page load.
 * it askes for the json object to start the game.
 * when json recieved - parsing it into 1 table
 * @param {type} $
 * @returns {undefined}
 */
$(function($) {
    function getMyGame() {
        $.getJSON('../secured/singleplay', function(singleplay) {
            
            var m = 6, n=6;
            var maze = singleplay.Maze;
            var sPos = singleplay.Start.Row *( 2* m-1 ) * 2 + singleplay.Start.Col * 2;
            var ePos = singleplay.End.Row * (2 * m - 1) * 2 + singleplay.End.Col * 2;
            var newMaze = "";
            for (var i=0; i<maze.length; i++) {
                if (i === sPos) {
                    //4 for start point
                    newMaze += '4';
                } else if (i === ePos){
                    //5 for goal point
                    newMaze += '5';
                } else {
                    newMaze += maze[i];
                }
            }            
            //parsing mazes strings into table
            myTable = makeTable(newMaze);
            //change loading img to the table
            $('#loading').fadeOut('slow', function() {
                $('#loading').remove();
                $('#divMaze').append(myTable);
            });
            
        });
    }
    getMyGame();
});

/**
 * parse this player string maze into table
 * @param {type} maze
 * @returns {makeTable.table|$} return parsed maze into table
 */
function makeTable(maze) {
    var table = $("<table id=\"mazeTbl\" cellspacing=\"0\">");
    for (var k=0,i= 0; i<11; i++ ) {
        //new line in table
        var row = $("<tr>");
        for (var j=0; j<11; j++,k+=1) {
            //new column in table
            if (maze[k] === '4') { 
                row.append($("<td class=\"cell\" id=\"startCell\"><img  id=\"imgR"+i+"C"+j+"\" class=\"cell43\"></label></td>"));
            } else if (maze[k] === '5')
            { 
                row.append($("<td class=\"cell\" id=\"endCell\"><img  id=\"imgR"+i+"C"+j+"\" class=\"cell5\"></label></td>"));
            } else { 
                row.append($("<td class=\"cell\"><img  id=\"imgR"+i+"C"+j+"\" class=\"cell"+maze[k]+"\"></label></td>"));
            }
        }
        row.append($("</tr>"));
        table.append(row);
    }
    table.append($("</table>"));
    return table;
}

/**
 * ask for hint for this user
 * @param {type} param
 */
$("#hintBtn").click(function(){
    var myTable = document.getElementById("mazeTbl");
    var isFinish = document.getElementsByClassName("cell7");
    if (myTable !== null && isFinish.length === 0) {
        //get table into string
        var currentMaze = "";
        for (var i = 0, row; row = myTable.rows[i]; i++) {
            for (var j = 0, col; col = row.cells[j]; j++) {
                var val = (col.childNodes[0].className)[4];
                currentMaze += val;
            }  
        }
        //post current maze with and type of game for the hint servlet
        $.post("../secured/HintServlet",
        {
            currentMaze: currentMaze,
            type: "single"
        },
        function(data, status){
            var hintImgId = "imgR";
            for (var i = 0; i<data.length-1; i++) {
                if (data[i] === ',') {
                    hintImgId += 'C';
                } else {
                    hintImgId += data[i];
                }
            }
            var hintImg = $("#"+hintImgId)[0];
            var preClassName = hintImg.className;
            
            var start ,limit;
            if (preClassName === "cell2" || preClassName === "cell3")  {
                start = 4;
                limit = 9;
            } else if (preClassName === "cell0" || preClassName === "cell5") {
                start = 0; 
                limit = 5;
            } else {
                return;
            }
            var counter = start;
            var flag = true;
            //display hint
            var interval = window.setInterval(function (){
                counter++;
                if(counter < limit) {
                    hintImg.className = "hint"+counter.toString();
                    if (counter === limit-1 && flag) {
                        counter = start;
                        flag = false;
                    }
                } else {
                    hintImg.className = preClassName;
                    clearInterval(interval);
                }
            }, 300);
            
        });
    }
});

/**
 * asks user if wants to restart game
 * @returns {undefined}
 */
function restart()
{
    var decision = popUp("restart the game");
    if (decision === true) {
        //restart table
        var start = document.getElementById("startCell");
        var end = document.getElementById("endCell");
        if (start !== null && end !== null){
            document.getElementById("finish").innerHTML = " ";
            for(var i=0; i<11; i++) {
                for(var j=0; j<11; j++) {
                    var cellIJ = document.getElementById("imgR"+i+"C"+j);
                    if (cellIJ.className === "cell3" || cellIJ.className === "cell2" || cellIJ.className === "cell7") {
                        cellIJ.className = "cell0";
                    } else if (cellIJ.className === "cell41" || cellIJ.className === "cell42" || cellIJ.className === "cell43" || cellIJ.className === "cell44") {
                        cellIJ.className = "cell0";
                    }
                }
            }
            //set back the start and goal images
            start.childNodes[0].className = "cell43";
            end.childNodes[0].className = "cell5";
        }
    } 
}
/**
 * asks user if wants to return to main menu
 * @returns {undefined}
 */
function returnHome()
{
    var decision = popUp("return to main menu");
    if (decision === true) {
        //go back to main menu
        history.go(-1);
    }
}
/**
 * pop up a confirm window with the given message
 * @param {type} message to the confirm window
 * @returns {unresolved}
 */
function popUp (message){
    return confirm("Are you sure you want to " + message +"?");
}
/**
 * use this function when user click on keyboard 
 * @param {type} event
 * @returns {undefined}
 */
function keyPressed(event) {
    var currentCell = document.getElementById("startCell");
    var isFinish = document.getElementsByClassName("cell7");
    if (currentCell !== null && isFinish.length === 0) {
        switch (event.code) {
            case "ArrowLeft": keyLeft();
                break;
            case "ArrowRight": keyRight();
                break;
            case "ArrowUp": keyUp();
                break;
            case "ArrowDown": keyDown();
                break;
        }
    }
}
/**
 * try to move this player from currentLocation to newLocation
 * @param {type} currentLocation 
 * @param {type} newLocation
 * @param {type} where = direction
 * @returns {Boolean} true = moved, false = stayed in same place 
 */
function movePlayer(currentLocation, newLocation, where) {
    if (newLocation.className !== "cell1" && newLocation.className !== "cell7") { 
        //can walk
        if (newLocation.className !== "cell5") {
            //regular cell
            if (newLocation.className === "cell0") {
                currentLocation.className = "cell2";
            } else if (newLocation.className === "cell2"){
                currentLocation.className = "cell3";
            } else if (newLocation.className === "cell3"){
                currentLocation.className = "cell2";
            }
            newLocation.className = "cell4"+where;
        } else {
            //finish game
            newLocation.className = "cell6";
            currentLocation.className = "cell7";
            document.getElementById("finish").innerHTML = "Congratulations! You solved the maze!";
        }
    } else if (newLocation.className === "cell1") {
        currentLocation.className = "cell4"+where;
    }
    
}
/**
 * user clicked left arrow key on keyboard
 * @returns {undefined}
 */
function keyLeft(){
    var currentCell; 
    for (var i=1; i<=4; i++){
        var classname = "cell4" + i.toString(); 
        var tempCell = document.getElementsByClassName(classname);
        if (tempCell.length !== 0) {
            currentCell = tempCell[0];
            break;
        } 
    }
    var sibNode = currentCell.parentNode.previousElementSibling;
    if (sibNode !== null) {
        var toCheckCell = sibNode.childNodes[0];
        if (toCheckCell !== null) {
            movePlayer(currentCell, toCheckCell, "4");
        } 
    } else {
        currentCell.className = "cell44";
    }
}
/**
 * user clicked right arrow key on keyboard
 * @returns {undefined}
 */
function keyRight(){
    var currentCell; 
    for (var i=1; i<=4; i++){
        var classname = "cell4" + i.toString(); 
        var tempCell = document.getElementsByClassName(classname);
        if (tempCell.length !== 0) {
            currentCell = tempCell[0];
            break;
        } 
    }
    var sibNode = currentCell.parentNode.nextElementSibling;
    if (sibNode !== null) {
        var toCheckCell = sibNode.childNodes[0];
        if (toCheckCell !== null) {
            movePlayer(currentCell, toCheckCell, "2");
        }
    } else {
        currentCell.className = "cell42";
    }
}
/**
 * user clicked up arrow key on keyboard
 * @returns {undefined}
 */
function keyUp(){
    var currentCell; 
    for (var i=1; i<=4; i++){
        var classname = "cell4" + i.toString(); 
        var tempCell = document.getElementsByClassName(classname);
        if (tempCell.length !== 0) {
            currentCell = tempCell[0];
            break;
        } 
    }
    
    var id, line = "";
    var cFound = false;
    var toCheckId = "imgR";
    id = currentCell.id;
    for (var i=4; i<id.length; i++) {
        if (cFound) {
            toCheckId += id[i];
        } else {
            if(id[i] === 'C') {
                cFound = true;
                line = line - 1;
                toCheckId += line + id[i];   
            } else {
                line += id[i];
            }
        }
    }
    var toCheckCell = document.getElementById(toCheckId);
    if (toCheckCell !== null) {
        movePlayer(currentCell, toCheckCell, "1");
    } else {
        currentCell.className = "cell41";
    }
}
/**
 * user clicked down arrow key on keyboard
 * @returns {undefined}
 */
function keyDown(){
    var currentCell; 
    for (var i=1; i<=4; i++){
        var classname = "cell4" + i.toString(); 
        var tempCell = document.getElementsByClassName(classname);
        if (tempCell.length !== 0) {
            currentCell = tempCell[0];
            break;
        } 
    }
    var id, line = "";
    var cFound = false;
    var toCheckId = "imgR";
    id = currentCell.id;
    for (var i=4; i<id.length; i++) {
        if (cFound) {
            toCheckId += id[i];
        } else {
            if(id[i] === 'C') {
                cFound = true;
                line = line - 1;
                line += + 2;
                toCheckId += line + id[i];   
            } else {
                line += id[i];
            }
        }
    }
    var toCheckCell = document.getElementById(toCheckId);
    if (toCheckCell !== null) {
        movePlayer(currentCell, toCheckCell, "3");
    } else {
        currentCell.className = "cell43";
    }
}