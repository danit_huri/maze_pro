
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@page session="false"%>
    <head>
        <title>MoomiMaze - Sign Up</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/signupStyle.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/iconsStyle.css"/>
    </head>
    <body>
        <!--form to enter user detailes to sign up for the website-->
        <div id="divForm" style="display: flex;">  
            <form id="subForm" action="${pageContext.request.contextPath}/signUp" method="post" onsubmit="javascript:return validate();">
                <h1>Sign Up</h1>
                <div class="line">
                    <div class="txt">Username:</div>
                    <input class="txtBox" type="text" name="username" id="username"/>
                </div>
                <div class="line">
                    <div class="txt">Password:</div>
                    <input class="txtBox" type="password" name="password" id="password" />
                </div>
                <div class="line">
                    <div class="txt">Name:</div>
                    <input class="txtBox" type="text" name="realname"  id="realname"/>
                </div>
                <div class="line">
                    <div class="txt">E-mail:</div>
                    <input class="txtBox" type="email" name="email"  id="email"/>
                </div>
                <div class="line">
                    <div class="txt">Icon:</div> 
                    <% for(int i=1; i<=3; i++) { %>
                        <div class="styleCheckbox">
                            <input type="checkbox" id="check<%=Integer.toString(i)%>" name="type<%=Integer.toString(i)%>" onclick='javascript:selectOnlyThis("check<%=Integer.toString(i)%>");'/>
                            <label for="check<%=Integer.toString(i)%>"></label>
                            <img class="icon" id="icon<%=Integer.toString(i)%>"/> 
                        </div>
                    <% } %>
                </div>
                <input class="btn" type="submit" name="submitBtn" value="submit"/>
                <input class="btn" type="reset" name="resetBtn" value="reset"  onclick='javascript:resetErr();'/>
                <div class="errorTxt" id="pError"></div>
                <% if (request.getAttribute("uError") != null &&
                    (Boolean)request.getAttribute("uError")){ %>
                <!--if request returned from server that there is other user with this username-->
                <div class="errorTxt" id="uError">This Username already exist</div>
                <% } %>
            </form> 
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath}/jsFiles/jsSignUp.js">
        </script>
    </body>
</html>
