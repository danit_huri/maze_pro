
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="false"%>
<!DOCTYPE html>
<html>
    <head>
        <title>MoomiMaze - Login</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/loginStyle.css"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/jsFiles/jsLogin.js"></script>
    </head>
    <body>
        <div id="divForm" style="display: flex;">
            <% if (request.getAttribute("conUser") != null &&
                    (Boolean)request.getAttribute("conUser")) { %>
                <!--form for when there is a session open-->
                <form id="subForm" action="${pageContext.request.contextPath}/secured/closeSession" method="post">
                    <div class="txt" id="div1">You are connected as:</div>
                    <div class="txt">${pageContext.request.getAttribute("username")}</div>
                    <input type="submit" value="Log out" class="btn"/>
                    <input type="button" action="action"  value="Log in" class="btn" onclick="logIn();"/>
                </form>
            <% } else { %>
            <!--form to login to the website-->
            <form id="subForm" action="${pageContext.request.contextPath}/login" method="post">
                <div class="txt" id="div1">Please enter user details:</div>
                <div class="txt">Username:</div>
                <input class="txtBox" type="text" name="username" />
                <div class="txt">Password:</div>
                <input class="txtBox" type="password" name="password" />
                <input type="submit" name="submitBtn" value="submit" class="btn"/>
                <input type="submit" name="signupBtn" value="sign up" class="btn"/>
                <% if (request.getAttribute("error") != null &&
                    (Boolean)request.getAttribute("error")){%>
                <div class="txtErr">Wrong username/password</div>
                <% out.println("\n");%>
                <div class="txtErr">Please try again</div>
                <% } %>
            </form>
            <% }%>
        </div>
    </body>
</html>