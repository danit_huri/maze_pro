<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
    <head>
        <!--userbean: detailes of the user-->
        <jsp:useBean id='user' scope='session' class='JavaBean.UserBean'/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>MoomiMaze - Main Menu</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/menuStyle.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/sessionFormStyle.css"/>  
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/iconsStyle.css"/>
    </head>
    <body>
        <div id="divSessionForm">
            <img id="logo"></h1>
            <!--form to show user detailes-->
            <form id="sessionForm" action="${pageContext.request.contextPath}/secured/closeSession" method="post">
                <div id="username"><jsp:getProperty name="user" property="name"/></div>
                <img class="icon" id='icon<jsp:getProperty name="user" property="type"/>' /> 
                <div> <input type="submit" id="logoutBtn" name="closeBtn" value="Log out" /> </div>
            </form>
        </div>
        <div id="divMainForm" style="display: flex;">
            <!--form to select type of game-->
            <form id="mainForm" action="${pageContext.request.contextPath}/secured/gameMenu" method="post" onsubmit="return getName();">
                <input class="btn" type="submit" name="singleBtn" value="Singleplay"/>
                <input class="btn" type="submit" name="multiBtn" value="Multiplay"/>
                <input type="hidden" id="gameName" name="gameName"/>
            </form> 
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath}/jsFiles/jsMenu.js">
        </script>       
    </body>
</html>


