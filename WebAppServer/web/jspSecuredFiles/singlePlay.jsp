<%@ page session="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:useBean id='user' scope='session' class='JavaBean.UserBean'/>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/singleStyle.css"/>  
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/sessionFormStyle.css"/>  
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/iconsStyle.css"/>
        <!--chooses the right css for this type of user-->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cssFiles/type<jsp:getProperty name="user" property="type"/>Style.css"/>
        <title>Moomimaze - Singleplay</title>
    </head>
    <body onkeydown="keyPressed(event)">
        <div id="divUp">
            <div id="singleTools">
                <!--upper toolbar-->
                <button class="btn" action="action" id="hintBtn">hint</button>
                <button class="btn" action="action" onclick="restart();"/>restart</button>
                <button class="btn" action="action" onclick="returnHome();"/>main menu</button>
            </div>
            <!--form to show user detailes-->
            <form id="sessionForm" action="${pageContext.request.contextPath}/secured/closeSession" method="post">
                <div id="username"><jsp:getProperty name="user" property="name"/></div>
                <img class="icon" id='icon<jsp:getProperty name="user" property="type"/>' /> 
                <div> <input type="submit" id="logoutBtn" name="closeBtn" value="Log out" /> </div>
            </form>
            <!--div to display message hen game over-->
            <div id="finish"></div>
        </div>
        <!--div to hold table/loding img-->
        <div id="divMaze" style="display: flex;">
            <img id="loading"/>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath}/jsFiles/jsSingle.js">
        </script>
    </body>
</html>
