/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package EnterpriseJavaBean;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import org.json.simple.JSONObject;

/**
 *contains the information of the current multiplay
 * @author User
 */
public class MultiPlay {
    private Play play;
    private final String userName;
    private Queue<String> toSend;
    private Queue<JSONObject> recieved;
    private boolean shouldStop;
    /**
     * constructor.
     * @param userName the name of the user that play in the multi play
     * @param name the name of the game
     */
    public MultiPlay(final String userName, String name)
    {
        this.userName=userName;
        this.play = new Play(name);
        this.toSend = new LinkedBlockingQueue<String>();
        this.recieved = new LinkedBlockingQueue<JSONObject>();
        this.shouldStop=false;
        
        new Thread() {
            @Override
            public void run() {
                try {         
                    while(!shouldStop)
                    {
                        while(!toSend.isEmpty())
                        {
                            Model.getInstance().sendMovement(userName, toSend.poll());
                            try{
                                Thread.sleep(250);
                            }catch(Exception e){ }
                        }
                    }                     /*
                        if(!toSend.isEmpty())
                        {
                            
                            Model.getInstance().sendMovement(userName, toSend.poll());
                        }
                        try{
                            Thread.sleep(400);
                        }catch(Exception e){ }
                    }

                }*/
                }catch(Exception e) {
                    System.out.println("error in start");
                }
            }
        }.start();
    }
    /**
     * getter
     * @return Play
     */
    public Play getPlay()
    {
        return this.play;
    }
    /**
     * getter
     * @return user name
     */
    public String getUserName()
    {
        return this.userName;
    }
    /**
     * adding new movevment to the send queue
     * @param move up\down\right\left
     */
    public void addMoveToSend(String move)
    {
        this.toSend.add(move);
    }
    /**
     * adding new movevment to the recieve queue
     * @param move up\down\right\left
     */
    public void addMoveToRecieved(JSONObject move)
    {
        this.recieved.add(move);
    }
    /**
     * getter
     * @return the next movement in the queue
     */
    public JSONObject getMovement()
    {
        return this.recieved.poll();
    }
    /**
     * return the next movement when the queue contains a value
     */
    public void newWaiting()
    {
        new Thread() {
            @Override
            public void run() {
                try {
                    
                    while(recieved.isEmpty() && !shouldStop)
                    {}
                    Model.getInstance().gotMovement(userName);
                } catch(Exception e) {
                    System.out.println("error in start");
                }
            }
        }.start();
    }
    /**
     * stop the threads are running background
     */
    public void stop()
    {
        this.shouldStop=true;
    }
}
