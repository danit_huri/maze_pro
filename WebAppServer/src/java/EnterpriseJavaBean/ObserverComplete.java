/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterpriseJavaBean;

/**
 *observer when the c# server returned a message and the message had processed 
 * @author User
 */
public interface ObserverComplete {
    /**
     *  abstract method of ObserverComplete interface
     * inform to the observer that the command that sent to the server is complete and ready
     */
    public void commandComplete(String tcommandType, String username);
}
