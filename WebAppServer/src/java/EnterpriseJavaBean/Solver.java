/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package EnterpriseJavaBean;

import JavaBean.PointBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *analyze the solution that got from the server to solver that can work with
 * @author עפרה
 */
public class Solver {
    
    private JSONObject mySolution;
    private int[][] mySolMatrix;
    private int rowSize;
    private int colSize;
    private PointBean start;
    
    /**
     * constructor
     * @param solutionJson the solution of the maze in JSON format
     */
    public Solver(JSONObject solutionJson) {
        rowSize=2*6-1;
        colSize=2*6-1;
        if(solutionJson!=null){
            mySolution = solutionJson;
            mySolMatrix = new int[rowSize][colSize];
        }
    }
    /**
     * initialize the parameters for the solution
     */
    public void initialize() {
        char[] mazeStr = ((String)mySolution.get("Maze")).toCharArray();
        JSONObject startJson = (JSONObject)mySolution.get("Start");
        
        String str = startJson.get("Row").toString();
        int row = Integer.parseInt(str);
        str = startJson.get("Col").toString();
        int col = Integer.parseInt(str);
        int sPos = row *( colSize ) * 2 + col * 2;
        mazeStr[sPos] = '4';
        
        for (int i = 0, k = 0; i < rowSize; i++)
        {
            for (int j = 0; j < colSize; j++, k++)
            {
                if (mazeStr[k] == '4')
                {
                    start = new PointBean(i, j);
                }
                mySolMatrix[i][j] = mazeStr[k] - '0';
            }
        }
        recursiveMarkSolution(3, start.getX(), start.getY());
    }
    
    /**
     * mark the solution with numbers
     * @param num the number to mark
     * @param currenti current row
     * @param currentj current col
     */
    private void recursiveMarkSolution(int num, int currenti, int currentj)
    {
        //stop condition
        if (mySolMatrix[currenti][currentj] == 5)
        {
            mySolMatrix[currenti][ currentj] = num;
            return;
        }
        mySolMatrix[currenti][currentj] = num;
        //go right
        if ((currentj + 1 < colSize) && (mySolMatrix[currenti][currentj + 1] == 2))
        {
            recursiveMarkSolution(num + 1, currenti, currentj + 1);
            return;
        }
        //go left
        if ((currentj - 1 >= 0) && (mySolMatrix[currenti][currentj - 1] == 2))
        {
            recursiveMarkSolution(num + 1, currenti, currentj - 1);
            return;
        }
        //go down
        if ((currenti + 1 < rowSize) && (mySolMatrix[currenti + 1][currentj] == 2))
        {
            recursiveMarkSolution(num + 1, currenti + 1, currentj);
            return;
        }
        //go up
        if ((currenti - 1 >= 0) && (mySolMatrix[currenti - 1][currentj] == 2))
        {
            recursiveMarkSolution(num + 1, currenti - 1, currentj);
            
        }
    }
    /**
     * solving the maze and give hint to the user
     * @param currentMaze the maze for solving
     * @return the next ideal point to the user
     */
    public PointBean solve(String currentMaze){
        char [] currentMazeArr = currentMaze.toCharArray();
        int [][] currentMazeMatrix = new int[rowSize][colSize];
        PointBean pPos = new PointBean();
        for (int i = 0, k = 0; i < rowSize; i++)
        {
            for (int j = 0; j < colSize; j++, k++)
            {
                if (currentMazeArr[k] == '4')
                {
                    pPos = new PointBean(i, j);
                }
                currentMazeMatrix[i][j] = currentMazeArr[k] - '0';
            }
        }
        
        PointBean hintPoint = new PointBean();
        
        //the user on the solution
        if (mySolMatrix[pPos.getX()][pPos.getY()] > 2)
        {
            //go right
            if ((pPos.getY() + 1 < colSize) && (mySolMatrix[pPos.getX()][pPos.getY() + 1] == mySolMatrix[pPos.getX()][pPos.getY()] + 1))
            {
                hintPoint.setX(pPos.getX());
                hintPoint.setY(pPos.getY() + 1);
            }
            //go left
            if ((pPos.getY() - 1 >= 0) && (mySolMatrix[pPos.getX()][ pPos.getY() - 1] == mySolMatrix[pPos.getX()][ pPos.getY()] + 1))
            {
                
                hintPoint.setX(pPos.getX());
                hintPoint.setY(pPos.getY() - 1);
            }
            //go down
            if ((pPos.getX() + 1 < rowSize) && (mySolMatrix[pPos.getX() + 1][ pPos.getY()] == mySolMatrix[pPos.getX()][ pPos.getY()] + 1))
            {
                hintPoint.setX(pPos.getX() + 1);
                hintPoint.setY(pPos.getY());
            }
            //go up
            if ((pPos.getX() - 1 >= 0) && (mySolMatrix[pPos.getX() - 1][ pPos.getY()] == mySolMatrix[pPos.getX()][ pPos.getY()] + 1))
            {
                hintPoint.setX(pPos.getX() - 1);
                hintPoint.setY(pPos.getY());
            }
        }
        else
        {
            //go right
            if ((pPos.getY() + 1 < colSize) && (currentMazeMatrix[pPos.getX()][pPos.getY() + 1] == 2))
            {
                hintPoint.setX(pPos.getX());
                hintPoint.setY(pPos.getY() + 1);
            }
            //go left
            if ((pPos.getY() - 1 >= 0) && (currentMazeMatrix[pPos.getX()][pPos.getY() - 1] == 2))
            {
                hintPoint.setX(pPos.getX());
                hintPoint.setY(pPos.getY() - 1);
            }
            //go down
            if ((pPos.getX() + 1 < rowSize) && (currentMazeMatrix[pPos.getX() + 1][pPos.getY()] == 2))
            {
                hintPoint.setX(pPos.getX() + 1);
                hintPoint.setY(pPos.getY());
            }
            //go up
            if ((pPos.getX() - 1 >= 0) && (currentMazeMatrix[pPos.getX() - 1][pPos.getY()] == 2))
            {
                hintPoint.setX(pPos.getX() - 1);
                hintPoint.setY(pPos.getY());
            }
        }
        
        return hintPoint;
    }
    
    
}