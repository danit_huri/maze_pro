/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package EnterpriseJavaBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * connect to the server socket and recieve messages from it in seperate thread
 * @author User
 */
public class TCPRecieve implements Runnable{
    private Socket serverSock;
    private String msgFromServer;
    private boolean shouldStop;
    private Observer myObserver;
    /**
     * constructor
     * @param sock the server socket
     * @param obs the observer
     */
    public TCPRecieve(Socket sock, Observer obs)
    {
        this.serverSock=sock;
        this.myObserver=obs;
    }
   /**
    * @return the message that recieved from the server
    */
     public String getMessage()
    {
        System.out.println(msgFromServer);
        return this.msgFromServer;
    }
     /**
     * stop the threads are running background
     */
    public  void stop()
    {
        this.shouldStop=true;
    }
/**
 * implement the abstract method of runnable interface- 
 * running a loop of recieving messages from the server
 */
    @Override
    public void run() {
        shouldStop=false;
        while(!shouldStop)
        {
            try{   
                try{Thread.sleep(250);}
                catch (InterruptedException ex) {
                    Logger.getLogger(TCPRecieve.class.getName()).log(Level.SEVERE, null, ex);
                }
                //create input stream attached to socket
                BufferedReader inFromServer = new BufferedReader(new InputStreamReader(this.serverSock.getInputStream()));
                //read line from server
                char[] buffer = new char[4096];
                String data = "";
                inFromServer.read(buffer);
                data = String.copyValueOf(buffer);
                this.msgFromServer=data;
                myObserver.gotMsgFromServer();
            }
            catch(IOException e)
            {
                e.printStackTrace();
                stop();
            }
        }
    }
}
