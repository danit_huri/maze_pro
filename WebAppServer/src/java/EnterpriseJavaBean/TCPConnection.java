/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package EnterpriseJavaBean;
import java.net.*;
import java.io.*;
/**
 * store the connection members with the server- the sender and reciver ckasses
 * @author User
 */
public class TCPConnection implements Observer {
    
    private Socket serverSock;
    private String IP;
    private int portNumber;
    private TCPRecieve reciever;
    private TCPSend sender;
    private Observer myObserver;
    /**
     * constructor.
     */
    public TCPConnection()
    {
       
    }
    /**
     * setter
     * @param obs the observer
     */
    public void setObserver(Observer obs)
    {
         this.myObserver=obs;
    }
    /**
     * connect to the server
     * @param IP ip number
     * @param portNumber server port number
     * @return true\false if the connection is succeed
     */
    public boolean connect(String IP, int portNumber)
    {
        this.IP=IP;
        this.portNumber=portNumber;
        try
        {
            serverSock = new Socket(IP, portNumber);
          //  (new Thread()).start();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
        return true;
    }
    /**
     * start the connection
     */
    public void start()
    {
         (new Thread( this.reciever=new TCPRecieve(serverSock, this))).start();   
         (new Thread( this.sender=new TCPSend(serverSock))).start();   
    }
    /**
     * writing command to the c# server
     * @param command for sending
     */
    public void write(String command)
    {
        this.sender.setCommand(command);
    }
    /**
     * reading from the c# server
     * @return the string that read
     */
    public String read()
    {  
       return (this.reciever.getMessage());
    }
     /**
     * close theconnection with the server
     */
    public void disconnect()
    {
        try{
            serverSock.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

/**
     * implementation of abstract method of Observer interface
     * inform to the observer that got message from the server
     */
    @Override
    public void gotMsgFromServer() {
        myObserver.gotMsgFromServer();
    }
}
