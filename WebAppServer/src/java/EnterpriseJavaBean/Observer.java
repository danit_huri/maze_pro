/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterpriseJavaBean;

/**
 *observer when the c# server returned a message
 * @author User
 */
public interface Observer {
    /**
     *  abstract method of Observer interface
     * inform to the observer that got message from the server
     */
    public void gotMsgFromServer();
}
