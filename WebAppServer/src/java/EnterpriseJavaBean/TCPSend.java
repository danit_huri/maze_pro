/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package EnterpriseJavaBean;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * connect to the server socket and send messages to it in seperate thread
 * @author User
 */
public class TCPSend implements Runnable{
    private Socket serverSock;
    private String commandToSend;
    private boolean shouldStop;
    private boolean changed;
     /**
     * constructor
     * @param sock the server socket
     */
    public TCPSend(Socket sock)
    {
        this.serverSock=sock;
    }
   /**
    * setter
    * @param command for sending to the client
    */
    public void setCommand(String command)
    {
        this.commandToSend= command;
        changed=true;
    }
    /**
     * stop the threads are running background
     */
    public  void stop()
    {
        this.shouldStop=true;
    }
/**
 * implement the abstract method of runnable interface- 
 * running a loop of sending messages to the server
 */
    @Override
    public void run() {
       shouldStop=false;
        while(!shouldStop){
           try{Thread.sleep(250);}
                catch (InterruptedException ex) {
                    Logger.getLogger(TCPRecieve.class.getName()).log(Level.SEVERE, null, ex);
                }
            if(changed)
            {
                try{
                    PrintWriter outToServer = new PrintWriter(this.serverSock.getOutputStream(),true);
                    outToServer.println(commandToSend);
                    changed=false;
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                    stop();
                }
            }
        }    }
}
