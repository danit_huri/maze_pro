/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterpriseJavaBean;
import java.lang.Thread;
/**
 *supply service to connect with the server
 * @author User
 */
public class ClientConnection implements  Observer{
    private TCPConnection connection;
    private Observer myObserver;
    /**
     * constructor.
     * @param conn the connection class with the server
     * @param obs  my observer-the model
     */
    public ClientConnection(TCPConnection conn, Observer obs)
    {
        this.connection=conn;
        this.myObserver = obs;
        this.connection.setObserver(this);
        
    }
    /**
     * connect to the server
     * @param ip ip number
     * @param port server port number
     */
    public void connect(String ip, int port)
    {
      if(connection.connect(ip,port))
      {
          start();
      }else
      {
          //problem in connection
           System.out.println("error in connection");
      }
    }
    /**
     * close theconnection with the server
     */
    public void disconnect()
    {
        connection.disconnect();
    }
    /**
     * start the connection
     */
    public void start()
    {
                   connection.start();
    }
    /**
     * opening single game
     * @param name the name of the game
     */ 
    public void openSingleGame(String name)
    {
            connection.write("generate " + name + " 1");
    }
    /**
     * request from the server to solve the maze
     * @param name the name of the game
     */
   public void solveGame(String name)
    {
         connection.write("solve " + name + " 1");
    }
   /**
    * opening multy game
    * @param name the name of the game
    */
   public void openMultiGame(String name)
    {
            connection.write("multiplayer " + name);
    }
   /**
    * sending the movement of the user
    * @param move up\down\left\right
    */
   public void sendMovement(String move)
   {
       connection.write("play " + move);
   }
   /**
    * read the message from the server 
    * @return the message
    */
    public String readMessage()
    {
        return connection.read();
    }
    /**
     * request from server to close the multigame
     * @param gamename the name of the game for closing
     */
    public void close(String gamename)
    {
        connection.write("close " + gamename);
    }
    /**
     * implementation of abstract method of Observer interface
     * inform to the observer that got message from the server
     */
    @Override
    public void gotMsgFromServer() {
       myObserver.gotMsgFromServer();
    }
}
