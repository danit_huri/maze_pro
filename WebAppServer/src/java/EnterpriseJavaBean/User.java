/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterpriseJavaBean;

/**
 * contains all the informationabout the user
 * contains it connection class with the server
 * @author User
 */
public class User implements Observer{
    private String userName;
    private String password;
    private String realName;
    private String email;
    private int type;
    private boolean isConnected;
    private ClientConnection myConnection;
    private EnterpriseJavaBean.Model theModel;
    private String sessionId; 
    /**
     * constructor.
     * @param userName
     * @param password
     * @param realName
     * @param email
     * @param type 
     */
    public User(String userName, String password, String realName, String email, int type) 
    {
        this.userName = userName;
        this.password =password;
        this.realName=realName;
        this.email=email;
        this.type=type;
        isConnected=false;
        //create the connection and adding the model as his observer
        this.myConnection =new ClientConnection(new TCPConnection(), this);
        this.theModel=Model.getInstance();
    }
   
    //accessors
    /**
     * getter
     * @return the name of the user
     */
    public String getUserName(){
        return userName;
    }
    /**
     * getter
     * @return password
     */
    public String getPassword(){
        return password;
    }
    /**
     * getter
     * @return the real name of the user
     */
    public String getRealName(){
        return realName;
    }
    /**
     * getter
     * @return the type of the user- moomin\groke\my
     */
    public int getType() {
        return type;
    }
    /**
     * @return true if the  user is connected else return false
     */
    public boolean isConnected(){
        return isConnected;
    }
/**
 * getter
 * @return the session id of the user
 */
    public String getSessionID() {
        return this.sessionId;
    } 
    
    //functions
     /**
     * close theconnection with the server
     */
    public void diconnect(){
        isConnected = false;
        this.myConnection.disconnect();
        this.sessionId = null;
    }
    
    /**
     * connect to the server
     * @param IP ip number
     * @param port server port number
     * @param ID session id
     */
    public void connect(final String IP,final  int port, String ID){
        this.sessionId = ID;
        new Thread() {
            @Override
            public void run() {
                try {
                    myConnection.connect(IP, port);
                } catch(Exception e) {
                    System.out.println("error in connect ");
                    isConnected = false;
                }
            }
        }.start();
        isConnected = true;
    }
    /**
     * opening single game
     * @param name the name of the game
     */ 
    public void openSingleGame(String name)
    {
        this.myConnection.openSingleGame(name);
    }
    /**
    * opening multy game
    * @param name the name of the game
    */
    public void openMultiGame(String name)
    {
        this.myConnection.openMultiGame(name);
    }
    /**
     * request from the server to solve the maze
     * @param name the name of the game
     */
    public void solveGame(String name)
    {
        this.myConnection.solveGame(name);
    }
    /**
    * sending the movement of the user
    * @param move up\down\left\right
    */
    public void sendMovement(String move)
    {
        this.myConnection.sendMovement(move);
    }
     /**
     * request from server to close the multigame
     * @param gamename the name of the game for closing
     */
    public void closeMultiGame(String gamename)
    {
        this.myConnection.close(gamename);
    }
    /**
     * implementation of abstract method of Observer interface
     * inform to the observer that got message from the server
     */
    @Override
    public void gotMsgFromServer() {
        //read the message from the c# server
        //update the model about that
       this.theModel.gotMessageFromServer(userName);    
    }
    /**
    * read the message from the server 
    * @return the message
    */
    public String readMessage()
    {
        return myConnection.readMessage();
    }
}
