/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterpriseJavaBean;

import JavaBean.PointBean;
import org.json.simple.JSONObject;

/**
 *contains all the information of a single play-maze his solution and his name
 * @author User
 */
public class Play {
    private JSONObject maze;
    private Solver solution;
    private String name;
    /**
     * constructor.
     * @param name the name of the maze
     */
    public Play(String name)
    {
        this.name=name;
    }
    /**
     * getter
     * @return the mazeJSON object
     */
    public JSONObject getMaze()
    {
        return this.maze;
    }
    /**
     * setter
     * @param maze for setting
     */
    public void setMaze(JSONObject maze)
    {
        this.maze=maze;
    }
    /**
     * getter
     * @return the soluton of the maze -JSON object
     */
    public Solver getSolution()
    {
        return this.solution;
    }
    /**
     * setter
     * @param maze for setting
     */
    public void setSolution(Solver maze)
    {
        this.solution=maze;
    }
   /**
     * getter
     * @return the name of the maze
     */
    public String getName()
    {
        return this.name;
    }
   /**
    * setter
    * @param name of the game
    */
    public void setName(String name)
    {
       this.name=name;
    }
    /**
     * getter
     * @param mazethe maze for hint
     * @return the next ideal point
     */
    public PointBean getHint(String maze){
        return solution.solve(maze);
    }
}
