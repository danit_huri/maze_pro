
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package EnterpriseJavaBean;

import JavaBean.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 *the model of the project/
 * provide service and connection with the server to the servlets
 * @author User
 */
public class Model{
    
    private static Model instance = new Model();
    private Map<String, User> users; //user detailes
    //private Map<String, ClientConnection> //user sockets
    private Map<String, Play> singleplays; //user singleplays
    private Map<String, MultiPlay> multiplays; //user multiplays
    private String IP;
    private int port;
    private List<ObserverComplete> observers;
    
    /**
     * singelton-constructor
     */
    private Model()
    {
        this.users=new HashMap<String, User>();
        this.singleplays = new HashMap<String, Play>();
        this.multiplays = new HashMap<String, MultiPlay>();
        this.observers = new ArrayList<ObserverComplete>();
        // read the ip and port from web.xml
        Context env;
        try {
            env = (Context)new InitialContext().lookup("java:comp/env");
            this.IP= (String)env.lookup("IP");
            this.port=Integer.parseInt((String)env.lookup("PORT"));
        } catch (NamingException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * singelton-give the single instance of the model
     * @return the single model
     */
    public static Model getInstance(){
        return instance;
    }
    /**
     * adding observer to the list for notifing
     * @param obs the observer to add
     */
    public void addObserver(ObserverComplete obs)
    {
        this.observers.add(obs);
    }
    /**
     * notify all the observers that command happened to specific user name
     * @param commandType multi\single\move
     * @param username the user name that the command happened to him
     */
    public void notifyAllObservers(String commandType, String username){
        for (ObserverComplete observer : observers) {
            observer.commandComplete(commandType, username);
        }
    }
    /**
     * cheking in the map of the users that the user name matc to the password
     * @param userName the user name
     * @param password the password he typed
     * @return true or false
     */
    public boolean isUsernameMatchPassword(String userName, String password)
    {
        if(users.containsKey(userName))
        {
            if(users.get(userName).getPassword().equals(password))
            {
                return true;
            }
        }
        return false;
    }
    /**
     * checking if the user name is exist in the map
     * @param userName user name for check
     * @return true\false
     */
    public boolean isUsernameExist(String userName)
    {
        return(users.containsKey(userName));
    }
    /**
     * checking if the user is connecting-his sesssion is true
     * @param userName user name for check
     * @return true\false
     */
    public boolean isUserConnected(String userName)
    {
        if(users.containsKey(userName)) {
            return users.get(userName).isConnected();
        }
        return false;
    }
    /**
     * return the session id of the user
     * @param userName 
     * @return the session id
     */
    public String getUserSessionID(String userName)
    {
        if(users.containsKey(userName)) {
            return users.get(userName).getSessionID();
        }
        return null;
    }
    /**
     *  get UserBean
     * @param username 
     * @return the user bean of the user
     */
    public UserBean getUserBean(String username) {
        String name = users.get(username).getRealName();
        int type = users.get(username).getType();
        return new UserBean(name, type, username);
    }
    /**
     * give the single play of the user
     * @param username user name that play the single game
     * @return play
     */
    public Play getSingleplay(String username) {
        return this.singleplays.get(username);
    }
     /**
     * give the multi play of the user
     * @param username user name that play the multi game
     * @return multiplay
     */
    public MultiPlay getMultiplay(String username) {
        return this.multiplays.get(username);
    }
    /**
     * insert new user to map
     * @param userName
     * @param password
     * @param realName
     * @param email
     * @param type 
     */
    public void insertNewUser(String userName, String password, String realName, String email, int type)
    {
        User newUser = new User(userName, password, realName, email, type);
        users.put(userName, newUser);
    }
    /**
     * create a single play to the user and insert to the map
     * @param username the name of the user
     * @param name the name of the single play
     */
    public void createSingleplay(String username, String name){
        if( this.singleplays.containsKey(username))
        {
            this.singleplays.remove(username);
        }
        this.singleplays.put(username, new Play(name));
        this.singleplays.get(username).setName(name);
        User user = this.users.get(username);
        user.openSingleGame(name);
        //send request to c# server - using map client connection
    }
     /**
     * create a multi play to the user and insert to the map
     * @param username the name of the user
     * @param name the name of the multi play
     */
    public void createMultiplay(String username, String name){
        if( this.multiplays.containsKey(username))
        {
            this.multiplays.remove(username);
        }
        this.multiplays.put(username, new MultiPlay(username, name));
        User user = this.users.get(username);
        user.openMultiGame(name);
        //send request to c# server - using map client connection
    }
    /**
     * close the multigame
     * @param username the name of the user
     */
    public void closeGame(String username)
    {
        if(multiplays.containsKey(username))
        {
            User user =this.users.get(username);
            MultiPlay multi = this.multiplays.get(username);
            String gamename = multi.getPlay().getName();
            multi.stop();
            user.closeMultiGame(gamename);
            this.multiplays.remove(username);
        }
    }
    /**
     * connecting user to the server
     * @param username the name of the user
     * @param ID the id of the session
     */
    public void connectUser(String username, String ID) {
        //connect user to the c# server
        final User user=  this.users.get(username);
        /*
        if(user.isConnected())
        {
        //send error to user that he is online
        //return false;
        }else{
        user.connect(IP, port, ID);
        }
        */
        user.connect(IP, port, ID);
        //return user.isConnected();
    }
    /**
     * disconnect the user from the server
     * @param username the name of the server
     */
    public void disconnectUser(String username) {
        User user=  this.users.get(username);
        user.diconnect();
        closeGame(username);
    }
 /**
  * adding new movement to the multiplay of the user
  * @param username the name of the user
  * @param move up\down\right\left
  */
    public void addNewMovement(String username, String move)
    {
        this.multiplays.get(username).addMoveToSend(move);
    }
    /**
  * sending the movement to the componenet of the multiplay of the user
  * @param username the name of the user to send
  * @param move up\down\right\left
  */
    public void sendMovement(String username, String move)
    {
        User user = this.users.get(username);
        user.sendMovement(move);
    }
    /**
     * inforn=m that anew movement got
     * @param username the name of the user
     */
    public void gotMovement(String username)
    {
        notifyAllObservers("move", username);
    }
    /**
     * asking for the movement that got
     * @param username the name of the user
     * @return json object that describe the movement
     */
    public JSONObject getMovement(String username)
    {
        MultiPlay multi = this.multiplays.get(username);
        return multi.getMovement();
    }
    /**
     * create new waiting to the movement
     * @param username the name of the user
     */
    public void addWaiting(String username)
    {
        MultiPlay multi = this.multiplays.get(username);
        multi.newWaiting();
    }
    /**
     * occured when a new message from the server arrived-deserialize the message
     * @param username the name of the user that got the message
     */
    public void gotMessageFromServer(String username) {
        User user =this.users.get(username);
        String msg = user.readMessage();
        int temp= msg.lastIndexOf('}');
        msg = msg.substring(0, temp+1);
        try {
            JSONObject json = (JSONObject)((new JSONParser()).parse(msg));
            //multiGame
            if (json.get("You") != null )
            {
                MultiPlay multi = this.multiplays.get(username);
                //multigame set name
                String gameName=json.get("Name").toString();
                String fullName = json.get("MazeName").toString();
                //String mazeName = fullName.substring(0, fullName.length()-4);
                gameName = gameName.substring(0, gameName.length()-2);
                if(multi.getPlay().getName().equals(gameName))
                {
                    //insert the maze to the muti game
                    multi.getPlay().setMaze(json);
                    user.solveGame(fullName);
                    return;
                }
            }
            else
            {
                //generate or solve
                if(json.get("Maze")!=null)
                {
                    
                    String maze = json.get("Maze").toString();
                    String mazeName =  json.get("Name").toString();
                    if(maze.contains("2"))
                    {
                        //solved multi
                        //remove the 2 last letters of the word _1\ _2
                        String gameName = mazeName.substring(0, mazeName.length() -4);
                        if(multiplays.containsKey(username) && multiplays.get(username).getPlay().getName().equals(gameName))
                        {
                            MultiPlay multi = multiplays.get(username);
                            //insert the solution
                            Solver sol = new Solver(json);
                            sol.initialize();
                            multi.getPlay().setSolution(sol);
                            if(multi.getPlay().getMaze()!= null)
                                notifyAllObservers("multi", username);
                            return;
                        } else if(singleplays.containsKey(username))
                        {
                            //solved single
                            Play single = singleplays.get(username);
                            if(single.getName().equals(mazeName))
                            {
                                //insert the solution to the single game
                                Solver sol = new Solver(json);
                                sol.initialize();
                                single.setSolution(sol);
                                if(single.getMaze()!= null)
                                    notifyAllObservers("single", username);
                                return;
                            }
                        }
                    }
                    else
                    {
                        //generate single
                        if(singleplays.containsKey(username))
                        {
                            Play single = singleplays.get(username);
                            if(single.getName().equals(mazeName))
                            {
                                //insert the maze to the single game
                                single.setMaze(json);
                                user.solveGame(mazeName);
                                return;
                            }
                        }
                        
                    }
                }else if (json.get("Move") != null )
                {
                    //movement message
                    MultiPlay multi = this.multiplays.get(username);
                    multi.addMoveToRecieved(json);
                    
                } else {
                    
                    //error message
                    return;
                }
            }
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * occured when the user ask for hint from the server
     * @param currentMaze the maze string
     * @param username the name of the user that asked for hint
     * @param mode
     * @return the next ideal point-javabean class
     */
    public PointBean getHint(String currentMaze, String username, int mode) {
        if (mode== 1) {
            Play userSingleplay = this.singleplays.get(username);
            return userSingleplay.getHint(currentMaze);
        } else if (mode== 2){
            MultiPlay userMultiplay = this.multiplays.get(username);
            return userMultiplay.getPlay().getHint(currentMaze);
        }
        return new PointBean();
    }
    
}
