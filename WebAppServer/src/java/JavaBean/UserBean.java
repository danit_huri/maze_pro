/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBean;

import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author עפרה
 */
public class UserBean implements Serializable {
    
    //members
    String name;
    String username;
    String type;
    
    private PropertyChangeSupport propertySupport;
    
    //ctors
    public UserBean() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    public UserBean(String realName, int type, String userName) {
        this.name = realName;
        this.type = Integer.toString(type);
        this.username = userName;
    }
    
    //get-set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    //listeners
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
}
