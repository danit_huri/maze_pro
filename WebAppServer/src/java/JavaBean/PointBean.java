/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBean;

import java.beans.*;
import java.io.Serializable;

/**
 *java bean class- represent point
 * @author עפרה
 */
public class PointBean implements Serializable {
    
    //members
    int x;
    int y;

    private PropertyChangeSupport propertySupport;
    
   /**
    * constructor
    */
    public PointBean() {
        propertySupport = new PropertyChangeSupport(this);
    }
    /**
     * constructor
     * @param x value of x
     * @param y value of y
     */
    public PointBean(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    //get-set
    /**
     * getter
     * @return value of x
     */
    public int getX() {
        return x;
    }
/**
 * setter
 * @param x value of x
 */
    public void setX(int x) {
        this.x = x;
    }
/**
 * getter
 * @return value of y
 */
    public int getY() {
        return y;
    }
/**
 * setter
 * @param y value of y 
 */
    public void setY(int y) {
        this.y = y;
    }
    
    //listeners
    /**
     * un implemented
     * @param listener 
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    /**
     * un implemented
     * @param listener 
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
}
