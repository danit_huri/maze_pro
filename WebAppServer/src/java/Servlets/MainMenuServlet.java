package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * handles the user request to start a game from main menu page
 */
public class MainMenuServlet extends HttpServlet {
    
    
    /**
     * Handles the HTTP <code>GET</code> method.
     * redirect to the main menu page
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath()+"/jspSecuredFiles/menu.jsp");
        }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     * save the name of the game in the session, and redirect user to wanted game
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String name = request.getParameter("gameName");
        if (request.getParameter("singleBtn") != null){
            //pressed singleplay button
            session.setAttribute("singleName", name);
            response.sendRedirect(request.getContextPath()+"/jspSecuredFiles/singlePlay.jsp");
        } else if (request.getParameter("multiBtn") != null){
            //pressed multiplay button
            session.setAttribute("multiName", name);
            response.sendRedirect(request.getContextPath()+"/jspSecuredFiles/multiPlay.jsp");
        }
    }
}
