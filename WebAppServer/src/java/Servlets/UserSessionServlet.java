package Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * this session handles user choice to log out of/close the session
 */
public class UserSessionServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     * invalidate the current session if not null
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession(false);
         if (session!= null) {
             session.invalidate();
         }
         //redirect to secured page so filter will redirect to login page
         response.sendRedirect("closeSession");
    }
}