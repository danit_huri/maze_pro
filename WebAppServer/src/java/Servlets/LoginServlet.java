package Servlets;

import EnterpriseJavaBean.*;
import JavaBean.UserBean;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * this servlet handles the 
 */
public class LoginServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>GET</code> method.
     * if session is on - redirect to session is on in login page
     * if session is off - redirect to login jsp file
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            //there is a connected user right now from this browser
            request.setAttribute("conUser", true);
            String sessionUsername = ((UserBean)session.getAttribute("user")).getUsername();
            request.setAttribute("username", sessionUsername);
            request.getRequestDispatcher("jspFiles/login.jsp").forward(request, response);
        } else {
          response.sendRedirect("jspFiles/login.jsp");  
        }
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     * check if user exist in database - if does redirect to main menu,
     * otherwise - return request with error attribute
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("submitBtn") != null){
            //pressed submit button
            String pass = request.getParameter("password");
            String userName = request.getParameter("username");
            EnterpriseJavaBean.Model myModel = EnterpriseJavaBean.Model.getInstance();
            if(myModel.isUsernameMatchPassword(userName, pass))
            {
                HttpSession session = request.getSession(false);
                if (session!= null) {
                    String sessionUsername = ((UserBean)session.getAttribute("user")).getUsername();
                    //session.setAttribute("user", myModel.getUserBean(userName));
                    if (sessionUsername.equals(userName)){
                        response.sendRedirect("secured/gameMenu");
                        return;
                    } else {
                        session.invalidate();
                    }
                } else {
                    if (myModel.isUserConnected(userName)) {
                        //this user is connected in other session/browser
                        SessionListener.destroySession(myModel.getUserSessionID(userName));
                    }
                }
                session = request.getSession(true);
                session.setAttribute("user", myModel.getUserBean(userName));
                session.setAttribute("valid", "realSession");
                //connecting to the server
                Model.getInstance().connectUser(userName, session.getId());
                response.sendRedirect("secured/gameMenu");
            } else{
                request.setAttribute("error", true);
                request.getRequestDispatcher("jspFiles/login.jsp").forward(request, response);
            }
        } else if (request.getParameter("signupBtn") != null) {
            //pressed sign up button
            response.sendRedirect("signUp");
        }     
    }
}
