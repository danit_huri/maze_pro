package Servlets;

import EnterpriseJavaBean.Model;
import EnterpriseJavaBean.ObserverComplete;
import EnterpriseJavaBean.Play;
import JavaBean.UserBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * handles user reuqest to recieve a movement from the opponent in multiplayer game
 */
public class RecieverServlet extends HttpServlet implements ObserverComplete{
    
    //map of waiting users and their asyncContext
    private HashMap<String, AsyncContext> asyncUsers;
    
    /**
     * initialize the users map, and adding this servlet as observer to the model
     * @throws ServletException 
     */
    @Override
    public void init() throws ServletException {
        asyncUsers = new HashMap<String, AsyncContext>();
        Model.getInstance().addObserver(this);
        
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     * adding this request to the waiting users map.
     * inform the model that this user is waiting for opp's movement
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);   
        if (session!=null) {
            request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
            AsyncContext async = request.startAsync();
            async.setTimeout(0);
            //add this request to the asyncUsers map
            String username = ((UserBean)session.getAttribute("user")).getUsername();
            asyncUsers.put(username, async);
            
            Model myModel = Model.getInstance();
            //infor model
            myModel.addWaiting(username);
        }
    }
    
    /**
     * notafication from the model that commandComplete.
     * if this is the "move" type command and user exist in map - 
     * send it the json of the movement
     * @param commandType 
     * @param username 
     */
    @Override
    public void commandComplete(String commandType, String username) {
        Model myModel = Model.getInstance();
        if(asyncUsers.containsKey(username))
        {
            AsyncContext userAsyncContext = asyncUsers.get(username);
            if(commandType.equals("move")){
               JSONObject movement = myModel.getMovement(username);
                try {
                    if (userAsyncContext != null) {
                        
                        HttpServletResponse peer = (HttpServletResponse)
                        userAsyncContext.getResponse();
                        peer.getWriter().write(movement.toString());
                        peer.setStatus(HttpServletResponse.SC_OK);
                        peer.setContentType("application/json");
                        asyncUsers.remove(username);
                        userAsyncContext.complete();
                        
                    }
                } catch (Exception e) {}
            }
        }   }
    
}
