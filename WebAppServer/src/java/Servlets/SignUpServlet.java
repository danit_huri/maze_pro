package Servlets;

import JavaBean.UserBean;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * this servlet handles user request to sign up
 */
public class SignUpServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>GET</code> method.
     * if session is on - redirect to session is on in login page
     * if session is off - redirect to sign u form jsp
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);
        if (session != null) {
            //there is a connected user right now from this browser
            request.setAttribute("conUser", true);
            String sessionUsername = ((UserBean)session.getAttribute("user")).getUsername();
            request.setAttribute("username", sessionUsername);
            request.getRequestDispatcher("jspFiles/login.jsp").forward(request, response);
        } else {
          response.sendRedirect("jspFiles/signUp.jsp");  
        }
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     * adding user to the system and redirect to the main menu
     * return to request if username already exist in the database
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //extracting form values
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        String realName = request.getParameter("realname");
        String email = request.getParameter("email");
        int type = 0;
        for (int i=1; i<=3; i++) {
            if(request.getParameter("type"+Integer.toString(i))!= null) {
                type = i;
                break;
            }
        }
        //checking if username exist in database
        EnterpriseJavaBean.Model myModel = EnterpriseJavaBean.Model.getInstance();
        if(myModel.isUsernameExist(userName))
        {
            //if user exist in database - back to sign up with error message
            request.setAttribute("uError", true);
            request.getRequestDispatcher("jspFiles/signUp.jsp").forward(request, response);
        }
        else
        {
            //creating new user and opening new session
            HttpSession session = request.getSession(false);
            if (session!=null) {
                //there is an active session
                session.invalidate();
            } 
            session = request.getSession(true);
            myModel.insertNewUser(userName, password, realName, email, type);
            myModel.connectUser(userName, session.getId());
            session.setAttribute("user", myModel.getUserBean(userName));
            session.setAttribute("valid", "realSession");
            response.sendRedirect("secured/gameMenu");
        }
    }
}
