package Servlets;

import JavaBean.PointBean;
import JavaBean.UserBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * handles hint request from user
 */
public class HintServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>POST</code> method.
     * asks model hint for the user for the game multi/single
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //ask model for hint
        HttpSession session = request.getSession(false);
        if (session!= null) {
            String currentMaze = request.getParameter("currentMaze");
            String type = request.getParameter("type");
            String username = ((UserBean)session.getAttribute("user")).getUsername();
            PointBean hint = new PointBean();
            //check the type of the game
            if (type.equals("single")) {
                hint = EnterpriseJavaBean.Model.getInstance().getHint(currentMaze, username, 1);
            } else if (type.equals("multi")) {
                hint = EnterpriseJavaBean.Model.getInstance().getHint(currentMaze, username, 2);
            }
            String hintStr = hint.getX()+","+hint.getY();
            //return answer
            try{
                PrintWriter out = response.getWriter();
                out.println(hintStr);
                out.flush();
            } catch (Exception ex) {}
        }
    }
}
