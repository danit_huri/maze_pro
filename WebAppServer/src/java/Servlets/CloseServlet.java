package Servlets;

import EnterpriseJavaBean.Model;
import JavaBean.UserBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * handles close multiplay request from user
 */
public class CloseServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     * ask model to close the multiplay game of this user
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String userName = ((UserBean)session.getAttribute("user")).getUsername();
        Model.getInstance().closeGame(userName);
    }

}
