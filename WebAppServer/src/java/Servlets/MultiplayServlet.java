package Servlets;

import EnterpriseJavaBean.Model;
import EnterpriseJavaBean.MultiPlay;
import EnterpriseJavaBean.ObserverComplete;
import JavaBean.UserBean;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 * this servlets handles requests for json of ultiplayer type
 */
public class MultiplayServlet extends HttpServlet implements ObserverComplete {
    
    //map of waiting users and their asyncContext
    private HashMap<String, AsyncContext> asyncUsers;
    
    /**
     * initialize the users map, and adding this servlet as observer to the model
     * @throws ServletException 
     */
    @Override
    public void init() throws ServletException {
        asyncUsers = new HashMap<String, AsyncContext>();
        Model.getInstance().addObserver(this);
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     * adding this request to the waiting users map.
     * asks the model to create new multiplay game
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        
        if (session!=null) {
            
            request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
            AsyncContext async = request.startAsync();
            async.setTimeout(0);
          
            //add this request to the asyncUsers map
            String username = ((UserBean)session.getAttribute("user")).getUsername();
            asyncUsers.put(username, async);
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {}
            
            EnterpriseJavaBean.Model myModel = EnterpriseJavaBean.Model.getInstance();
            //ask model to create new multiplay for this user
            myModel.createMultiplay(username, (String)session.getAttribute("multiName"));
        }
    }
    
    /**
     * notafication from the model that commandComplete.
     * if this is thee "multi" type command and user exist in map - 
     * send it the json of multiplay
     * @param commandType 
     * @param username 
     */
    @Override
    public void commandComplete(String commandType, String username) {
        //processRequest(request, response);
        EnterpriseJavaBean.Model myModel = EnterpriseJavaBean.Model.getInstance();
        
        if(asyncUsers.containsKey(username))
        {
            AsyncContext userAsyncContext = asyncUsers.get(username);
            if(commandType.equals("multi")){
                MultiPlay myGame = myModel.getMultiplay(username);
                try {
                    if (userAsyncContext != null) {
                        JSONObject obj = myGame.getPlay().getMaze();
                        HttpServletResponse peer = (HttpServletResponse)
                        userAsyncContext.getResponse();
                        peer.getWriter().write(obj.toString());
                        peer.setStatus(HttpServletResponse.SC_OK);
                        peer.setContentType("application/json");
                        asyncUsers.remove(username);
                        userAsyncContext.complete();
                    }
                } catch (Exception e) {}
            }
        }
    }
}
