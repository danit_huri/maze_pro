/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *confirm that just users with appropriate permission will be alowed to reach the secured pages
 * @author User
 */
public class LoginFilter implements Filter {
    /**
     * initialize the filter-un implemented
     * @param filterConfig
     * @throws ServletException 
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}
    /**
     * un implemented
     */
    @Override
    public void destroy() {}
    /**
     * check if the session of the user is not null and alow him to reach the secured page he asked for
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException 
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest)request).getSession(false);
        if (session != null) {
            chain.doFilter(request, response);
        }
        else {
            ((HttpServletResponse) response).sendRedirect(request.getServletContext().getContextPath()+"/login");
        }
    }
}  






