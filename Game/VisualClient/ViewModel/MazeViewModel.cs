﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using VisualClient.Model;

namespace VisualClient.ViewModel
{ 

    public class MazeViewModel: INotifyPropertyChanged
    {
        /// <summary>
        /// argument of my model
        /// </summary>
        private IModel<JsonMaze> myModel;
        /// <summary>
        /// event of lost connection with the server
        /// </summary>
        public event voidFunc lostConnection;
        //INotifyPropertyChanged implementation event
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="model"></param>
        public MazeViewModel(IModel<JsonMaze> model)
        {
            myModel = model;
            //catch the event property changed of the model
            myModel.PropertyChanged += delegate (object sender, PropertyChangedEventArgs prop)
            {
                //multyPlay game
                if (prop.PropertyName.Equals("MyMultiplay"))
                {
                    NotifyPropertyChanged(prop.PropertyName + "You");
                    Thread.Sleep(250);
                    NotifyPropertyChanged(prop.PropertyName + "Other");
                }
                //connection message
                else if (prop.PropertyName.Equals("IsConnected"))
                {
                    if (myModel.IsConnected == false)
                    {
                        lostConnection();
                    }
                } else
                {
                    NotifyPropertyChanged(prop.PropertyName);
                }
                
            };
        }
        /// <summary>
        /// default constructor
        /// </summary>
        public MazeViewModel() { }

        //properties implementation
        public int OtherMovement
        {
            get
            {
                return myModel.OtherMovement;
            }
        }
        /// <summary>
        /// propert-singlePlay
        /// </summary>
        public string MySingleplay
        {
            get
            {
                if (myModel.MySingleplay == null)
                {
                    return "";
                } else
                {
                    return myModel.MySingleplay.ToString();
                }
            }
        }
        /// <summary>
        /// property-multyPlay
        /// </summary>
        public string MyMultiplayYou
        {
            get
            {
                if (myModel.MyMultiplay == null)
                {
                    return "";
                }
                else
                {
                    return myModel.MyMultiplay.You.ToString();
                }
            }
        }
        /// <summary>
        /// property-multyPlay component
        /// </summary>
        public string MyMultiplayOther
        {
            get
            {
                if (myModel.MyMultiplay == null)
                {
                    return "";
                }
                else
                {
                    return myModel.MyMultiplay.Other.ToString();
                }
            }
        }
        /// <summary>
        /// activate the event propertyChanged
        /// </summary>
        /// <param name="propName"></param>
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        //methods for communication with model
        /// <summary>
        /// sending connect command to model
        /// </summary>
        public void connectToServer()
        {
            //getting ip and port number from app.config file
            string ip = ConfigurationManager.AppSettings["IP"];
            int portNumber = int.Parse(ConfigurationManager.AppSettings["PORT"]);
            myModel.connect(ip, portNumber);
            if (myModel.IsConnected)
            {
                myModel.start();
            }
            
        }

        /// <summary>
        /// asking model if its connected to a server
        /// </summary>
        /// <returns></returns>
        public bool isConnected()
        {
            if (myModel.IsConnected)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// open a new multyplayer game
        /// </summary>
        /// <param name="name">the name of the multyGame</param>
        public void openMultiplayGame(string name)
        {
            myModel.openMultiplayGame(name);
        }
        /// <summary>
        /// close the multyplayer game
        /// </summary>
        public void closeMultiplayGame()
        {
            myModel.closeMultiplayGame();
        }
        /// <summary>
        /// opening a new sinpleplayer game
        /// </summary>
        /// <param name="name">the name of the singleGame</param>
        public void openSingleplayGame(string name)
        {
            myModel.openSingleplayGame(name);
        }
        /// <summary>
        /// close the singleplayer game
        /// </summary>
        public void closeSingleplayGame()
        {
            myModel.closeSingleplayGame();
        }

        public void sendMovement(int direction)
        {
            myModel.sendMovement(direction);
        }
    }
}
