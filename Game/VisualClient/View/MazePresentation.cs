﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using VisualClient.general;

namespace VisualClient.View
{
    public class MazePresentation :INotifyPropertyChanged
    {
        /// <summary>
        /// number of rows
        /// </summary>
        private int rowSize;
        /// <summary>
        /// number of columns
        /// </summary>
        private int colSize;
        /// <summary>
        /// collection ofobservables- connect to the maze labels by data binding
        /// </summary>
        private ObservableCollection<ObservableCollection<char>> mazePuzzleColl;
        /// <summary>
        /// the solution of the maze
        /// </summary>
        private int[,] mazeSol;
        /// <summary>
        /// start point
        /// </summary>
        private general.Point start;
        /// <summary>
        /// end point
        /// </summary>
        private general.Point end;
        /// <summary>
        /// player location point
        /// </summary>
        private general.Point playerLocation;
        /// <summary>
        /// hint point
        /// </summary>
        private general.Point hint;
        /// <summary>
        /// boolean if the hint is apear on the screen
        /// </summary>
        private bool isOnHint = false;
        /// <summary>
        /// boolean if the user win
        /// </summary>
        private bool isWinner = false;
        /// <summary>
        /// boolean if the user is on game
        /// </summary>
        private bool onGame = false;
        /// <summary>
        /// save the prev char of the hint 
        /// </summary>
        private int playerMovement = 0;
        private char currentChar;

        /// <summary>
        /// for data binding
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// constructor
        /// </summary>
        public MazePresentation()
        {
            mazeSol = new int[rowSize, colSize];
            rowSize = int.Parse(ConfigurationManager.AppSettings["N"]) * 2 - 1;
            colSize = int.Parse(ConfigurationManager.AppSettings["M"]) * 2 - 1;
            int cellHeightSize = 400 / rowSize;
            int cellWidthSize = 400 / colSize;
            Properties.Settings.Default.cellHeight = cellHeightSize.ToString();
            Properties.Settings.Default.cellWidth = cellHeightSize.ToString();
            mazeSol = new int[rowSize, colSize];
            mazePuzzleColl = new ObservableCollection<ObservableCollection<char>>();
            hint = new general.Point();
            ///initialize the maze
            for (int i = 0, k = 0; i < rowSize; i++)
            {
                mazePuzzleColl.Add(new ObservableCollection<char>());
                for (int j = 0; j < colSize; j++, k++)
                {
                    mazeSol[i, j] = 0;
                    mazePuzzleColl[i].Add('0');
                }
            }
        }
        /// <summary>
        /// set the presentation of the maze by the order that given
        /// </summary>
        /// <param name="order">the order of the maze 011010</param>
        public void setPresentation(string order)
        {
            if (!order.Equals(""))
            {
                playerLocation = new general.Point();
                start = new general.Point();
                end = new general.Point();
                string puzzleStr = order.Substring(0, order.IndexOf(' '));
                string solStr = order.Substring(order.IndexOf(' ') + 1);
                bool noSolutionFlag = false;
                if (solStr.Equals(""))
                {
                    noSolutionFlag = true;
                }
                for (int i = 0, k = 0; i < rowSize; i++)
                {
                    for (int j = 0; j < colSize; j++, k++)
                    {
                        if (puzzleStr[k] == '*')
                        {
                            start.Row = i;
                            start.Col = j;
                            playerLocation.Row = i;
                            playerLocation.Col = j;
                        }
                        else if (puzzleStr[k] == '#')
                        {
                            end.Row = i;
                            end.Col = j;
                        }
                        if (!noSolutionFlag)
                        {
                            mazeSol[i, j] = int.Parse(solStr[k].ToString());
                        }
                        mazePuzzleColl[i][j] = puzzleStr[k];
                    }
                }
                if (!noSolutionFlag)
                {
                    RecursiveMarkSolution(3, start.Row, start.Col);
                }
                OnGame = true;
            }
        }
        /// <summary>
        /// reset the board
        /// </summary>
        public void resetBoard()
        {
            if (onGame)
            {
                if (isWinner)
                {
                    isWinner = false;
                }
                onGame = false;
                for (int i = 0, k = 0; i < rowSize; i++)
                {
                    for (int j = 0; j < colSize; j++, k++)
                    {
                        if (mazePuzzleColl[i][j] != '1')
                        {
                            mazePuzzleColl[i][j] = '0';
                        }
                    }
                }
                mazePuzzleColl[start.Row][start.Col] = '*';
                mazePuzzleColl[end.Row][end.Col] = '#';
                playerLocation.Row = start.Row;
                playerLocation.Col = start.Col;
                onGame = true;
            }
        }
        /// <summary>
        /// search for the ideal next step
        /// </summary>
        public void IdealPlayerNewLocation()
        {
            if (onGame && !isWinner)
            {
                onGame = false;
                //find the location..
                FindIdealPlayerNewLocation();
                isOnHint = true;
                currentChar = mazePuzzleColl[hint.Row][hint.Col];  
                mazePuzzleColl[hint.Row][hint.Col] = '^';
                onGame = true;
            }
            
        }
        /// <summary>
        /// search for the ideal next step
        /// </summary>
        public void FindIdealPlayerNewLocation()
        {
            int rowPos = playerLocation.Row;
            int colPos = playerLocation.Col;
            //the user on the solution
            if (mazeSol[rowPos, colPos] > 2)
            {
                //go right
                if ((colPos + 1 < colSize) && (mazeSol[rowPos, colPos + 1] == mazeSol[rowPos, colPos] + 1))
                {
                    hint.Row = rowPos;
                    hint.Col = colPos + 1;
                }
                //go left
                if ((colPos - 1 >= 0) && (mazeSol[rowPos, colPos - 1] == mazeSol[rowPos, colPos] + 1))
                {
                    hint.Row = rowPos;
                    hint.Col = colPos - 1;
                }
                //go down
                if ((rowPos + 1 < rowSize) && (mazeSol[rowPos + 1, colPos] == mazeSol[rowPos, colPos] + 1))
                {
                    hint.Row = rowPos + 1;
                    hint.Col = colPos;
                }
                //go up
                if ((rowPos - 1 >= 0) && (mazeSol[rowPos - 1, colPos] == mazeSol[rowPos, colPos] + 1))
                {
                    hint.Row = rowPos - 1;
                    hint.Col = colPos;
                }
            }
            else
            {
                //go right
                if ((colPos + 1 < colSize) && (mazePuzzleColl[rowPos][colPos + 1] == '2'))
                {
                    hint.Row = rowPos;
                    hint.Col = colPos + 1; 
                }
                //go left
                if ((colPos - 1 >= 0) && (mazePuzzleColl[rowPos][colPos - 1] == '2'))
                {
                    hint.Row = rowPos;
                    hint.Col = colPos - 1;
                }
                //go down
                if ((rowPos + 1 < rowSize) && (mazePuzzleColl[rowPos + 1][colPos] == '2'))
                {
                    hint.Row = rowPos + 1;
                    hint.Col = colPos;
                }
                //go up
                if ((rowPos - 1 >= 0) && (mazePuzzleColl[rowPos - 1][colPos] == '2'))
                {
                    hint.Row = rowPos - 1;
                    hint.Col = colPos;
                }
            }
        }

        /// <summary>
        /// mark the solution with numbers
        /// </summary>
        /// <param name="num">the number to mark</param>
        /// <param name="currenti">current roe</param>
        /// <param name="currentj">current col</param>
        private void RecursiveMarkSolution(int num, int currenti, int currentj)
        {
            //stop condition
            if (mazePuzzleColl[currenti][currentj] == '#')
            {
                mazeSol[currenti, currentj] = num;
                return;
            }
            mazeSol[currenti, currentj] = num;
            //go right
            if ((currentj + 1 < colSize) && (mazeSol[currenti, currentj + 1] == 2))
            {
                RecursiveMarkSolution(num + 1, currenti, currentj + 1);
                return;
            }
            //go left
            if ((currentj - 1 >= 0) && (mazeSol[currenti, currentj - 1] == 2))
                {
                RecursiveMarkSolution(num + 1, currenti, currentj - 1);
                return;
                }
            //go down
            if ((currenti + 1 < rowSize) && (mazeSol[currenti + 1, currentj] == 2))
            {
                RecursiveMarkSolution(num + 1, currenti + 1, currentj);
                return;
            }
            //go up
            if ((currenti - 1 >= 0) && (mazeSol[currenti - 1, currentj] == 2))
            {
                RecursiveMarkSolution(num + 1, currenti - 1, currentj);
                return;
        }
        }
        
        /// <summary>
        /// update the location of the user in the board
        /// </summary>
        /// <param name="gapRow">in how many points to add</param>
        /// <param name="gapCol">in how many points to add</param>
        private bool updatePlayerLocation(int gapRow, int gapCol)
        {
            int preRow, preCol, newRow, newCol;
            preRow = playerLocation.Row;
            preCol = playerLocation.Col;
            newRow = preRow + gapRow;
            newCol = preCol + gapCol;
            if(isOnHint)
            {
                mazePuzzleColl[hint.Row][hint.Col] = currentChar;
                isOnHint = false;
            }
            bool changedPlayerLocation = false;
            //can pass
            if (mazePuzzleColl[newRow][newCol] != '#')
            {
                if (mazePuzzleColl[newRow][newCol] == '0')
                {
                    mazePuzzleColl[preRow][preCol] = '2';
                    
                }
                //passed before
                else if (mazePuzzleColl[newRow][newCol] == '2')
                {
                    mazePuzzleColl[preRow][preCol] = '3';
                }
                else if (mazePuzzleColl[newRow][newCol] == '3')
                {
                    mazePuzzleColl[preRow][preCol] = '2';
                }
                if(mazePuzzleColl[newRow][newCol] != '1')
                {
                    mazePuzzleColl[newRow][newCol] = '*';
                    playerLocation.Row = newRow;
                    playerLocation.Col = newCol;
                    changedPlayerLocation = true;
                }
            }
            else
            {
                //the user win
                mazePuzzleColl[preRow][preCol] = '2';
                mazePuzzleColl[newRow][newCol] = '$';
                changedPlayerLocation = true;
                IsWinner = true;
            }
            return changedPlayerLocation;
        }
        /// <summary>
        /// key down pressed handle
        /// </summary>
        public void handleKeyDown()
        {
            bool flag = false;
            if (start != null && end != null)
            {
                if (playerLocation.Row < rowSize - 1)
                {
                    flag = updatePlayerLocation(1, 0);
                }
            }
            if (flag)
            {
                PlayerMovement = 3;
            }
        }
        /// <summary>
        /// key up pressed handle
        /// </summary>
        public void handleKeyUp()
        {
            bool flag = false;
            if (start != null && end != null)
            {
                if (playerLocation.Row > 0)
                {
                    flag = updatePlayerLocation(-1, 0);
                }
            }
            if (flag)
            {
                PlayerMovement = 1;
            }
        }
        /// <summary>
        /// key left pressed handle
        /// </summary>
        public void handleKeyLeft()
        {
            bool flag = false;
            if (start != null && end != null)
            {
                if (playerLocation.Col > 0)
                {
                    flag = updatePlayerLocation(0, -1);
                }
                }
            if (flag)
            {
                PlayerMovement = 4;
            }
        }
        /// <summary>
        /// key right pressed handle
        /// </summary>
        public void handleKeyRight()
        {
            bool flag = false;
            if (start != null && end != null)
            {
                if (playerLocation.Col < colSize -1)
                {
                    flag = updatePlayerLocation(0, 1);
                }
                }
            if (flag)
            {
                PlayerMovement = 2;
            }
        }

        
        /// <summary>
        /// for maze grid control
        /// </summary>
        public ObservableCollection<ObservableCollection<char>> MazePuzzleColl
        {
            get
            {
                return mazePuzzleColl;
            }
            set
            {
                mazePuzzleColl = value;
            }
        }

        /// <summary>
        /// property- isWinner
        /// </summary>
        public bool IsWinner
        {
            get
            {
                return isWinner;
            }

            set
            {
                isWinner = value;
                NotifyPropertyChanged("IsWinner");
            }
        }
        /// <summary>
        /// property- onGame
        /// </summary>
        public bool OnGame
        {
            get
            {
                return onGame;
            }

            set
            {
                onGame = value;
            }
        }

        public int PlayerMovement
        {
            get
            {
                return playerMovement;
            }

            set
            {
                playerMovement = value;
                NotifyPropertyChanged("PlayerMovement");
            }
        }

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

    }
}
