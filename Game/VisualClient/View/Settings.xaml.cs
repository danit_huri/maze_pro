﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Reflection;

namespace VisualClient.View
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        /// <summary>
        /// issaved bolean argument- if the user saved the changes
        /// </summary>
        private bool isSaved;

        /// <summary>
        /// constructor
        /// </summary>
        public Settings()
        {
            InitializeComponent();
            this.textBoxIP.Text = ConfigurationManager.AppSettings["IP"];
            this.textBoxPort.Text = ConfigurationManager.AppSettings["PORT"];
            isSaved = false;
        }
        /// <summary>
        /// property- isSaved
        /// </summary>
        public bool IsSaved
        {
            get
            {
                return isSaved;
            }

            set
            {
                isSaved = value;
            }
        }
        /// <summary>
        /// catch the event of ip textbox GotMouseCapture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxIP_GotMouseCapture(object sender, MouseEventArgs e)
        {
            textBoxIP.Text = "";
        }
        /// <summary>
        /// catch the event of port textbox GotMouseCapture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxPort_GotMouseCapture(object sender, MouseEventArgs e)
        {
            textBoxPort.Text = "";
        }

        /// <summary>
        /// catch the event of buttonSave click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            //Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            configuration.AppSettings.Settings["IP"].Value = textBoxIP.Text;
            configuration.AppSettings.Settings["PORT"].Value = textBoxPort.Text;
            configuration.Save();
            ConfigurationManager.RefreshSection("appSettings");
           
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["IP"].Value = textBoxIP.Text;
            config.AppSettings.Settings["PORT"].Value = textBoxPort.Text;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            
            // System.Configuration.ConfigurationManager.AppSettings.Set("IP", );
            // System.Configuration.ConfigurationManager.AppSettings.Set("PORT", textBoxPort.Text);
            this.isSaved = true;
            Close();
        }

        /// <summary>
        /// catch the event of port textbox LostFocus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxPort_LostFocus(object sender, RoutedEventArgs e)
        {
            if (textBoxPort.Text.Equals(""))
            {
                this.textBoxPort.Text = ConfigurationManager.AppSettings["PORT"];
            }
        }
         /// <summary>
        /// catch the event of ip textbox LostFocus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxIP_LostFocus(object sender, RoutedEventArgs e)
        {
            if (textBoxIP.Text.Equals(""))
            {
                this.textBoxIP.Text = ConfigurationManager.AppSettings["IP"];
            }
        }
    }
}
