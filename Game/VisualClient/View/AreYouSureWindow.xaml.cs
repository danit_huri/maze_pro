﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VisualClient.View
{
    /// <summary>
    /// Interaction logic for AreYouSureWindow.xaml
    /// </summary>
    public partial class AreYouSureWindow : Window
    {
        /// <summary>
        /// constructor
        /// </summary>
        public AreYouSureWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// catch event that occur when the user decide to restart the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restartGameBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
