﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using VisualClient.Model;
using VisualClient.ViewModel;
using System.ComponentModel;
using VisualClient.View.controls;


namespace VisualClient.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// argument of my view model
        /// </summary>
        private MazeViewModel mazeVM;
        /// <summary>
        /// argument of my menu-user control
        /// </summary>
        private MainMenu myMainMenu;
        /// <summary>
        /// constructor
        /// </summary>
        public MainWindow()
        {
            mazeVM = new MazeViewModel(new ClientMazeModel(new TCPClientConnection()));
            DataContext = mazeVM;
            myMainMenu = new MainMenu(mazeVM);
            InitializeComponent();
            //placing the main menu as the content
            this.Content = myMainMenu;
            
            mazeVM.connectToServer();
            //myGrid.Focus();
        }
        /// <summary>
        /// catch an even that occur when connected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bu_Click(object sender, RoutedEventArgs e)
        {
            bool isC = mazeVM.isConnected();
            
            mazeVM.openSingleplayGame("Focusing3");
            //myGrid.Focus();
        }
    }
}
