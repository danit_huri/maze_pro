﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using VisualClient.ViewModel;

namespace VisualClient.View.controls
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl
    {
        /// <summary>
        /// contains a view model that calling with the model
        /// </summary>
        private MazeViewModel myViewModel;
        /// <summary>
        /// sound media player that play tohe song
        /// </summary>
        System.Media.SoundPlayer player = new System.Media.SoundPlayer();

        /// <summary>
        /// constructor.
        /// </summary>
        /// <param name="mvm">get a view model argument</param>
        public MainMenu(MazeViewModel mvm)
        {
            InitializeComponent();
            myViewModel = mvm;
            //playing music background
            player.SoundLocation = "View/music/openSong.wav";
            player.PlayLooping();
        }

        /// <summary>
        /// event that accur when the user ask for single play
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_singlePlayer_Click(object sender, RoutedEventArgs e)
        {
            //change the music background
            player.SoundLocation = "View/music/musicMenu.wav";
            player.PlayLooping();
            SingleplayControl spc = new SingleplayControl(myViewModel);
            //show the single play control on the window
            Window.GetWindow(this).Content = spc;
            spc.start();
        }
        /// <summary>
        /// event that accur when the user ask for multy play
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_multiPlayer_Click(object sender, RoutedEventArgs e)
        {
            //change the music background
            player.SoundLocation = "View/music/musicMenu.wav";
            player.PlayLooping();
            MultiplayControl mul = new MultiplayControl(myViewModel);
            //show the single play control on the window
            Window.GetWindow(this).Content = mul;
            mul.start();
        }
        /// <summary>
        /// event that accur when the user ask for change the settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_settings_Click(object sender, RoutedEventArgs e)
        {
            Settings settingsWindow = new Settings();
            settingsWindow.ShowDialog();
            //if the user made changes i want to reconnect to the new server
            if(settingsWindow.IsSaved)
            {
                this.myViewModel.connectToServer();
            }
        }
        
    }
}
