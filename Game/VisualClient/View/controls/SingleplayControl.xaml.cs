﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;
using VisualClient.ViewModel;
public delegate void dataChanged();

namespace VisualClient.View.controls
{
    /// <summary>
    /// Interaction logic for SingleplayControl.xaml
    /// </summary>
    public partial class SingleplayControl : UserControl
    {
        /// <summary>
        /// argument of View-Model
        /// </summary>
        private MazeViewModel myViewModel;
        /// <summary>
        /// user control of maze
        /// </summary>
        private MazeGridControl myMazeGridControl;
        /// <summary>
        /// the tool bar user control
        /// </summary>
        private PlayToolbarControl myToolbarControl;
        
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mvm"></param>
        public SingleplayControl(MazeViewModel mvm)
        {
            myViewModel = mvm;
            InitializeComponent(); 
            myMazeGridControl = (MazeGridControl) myStack.Children[0];
            myToolbarControl = (PlayToolbarControl)myToolbar.Children[0];
            setElements();
            myStack.Focus();
        }
        /// <summary>
        /// calling to start the single play part-check we are online
        /// </summary>
        public void start()
        {
            Window.GetWindow(this).Background = Brushes.AliceBlue;
            if (myViewModel.isConnected())
            {
                startNewGame();
            }
            else
            {
                //message not connected dialog window
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    LostConnectionWindow lcw = new LostConnectionWindow(myViewModel);
                    lcw.Owner = Window.GetWindow(this);
                    lcw.ShowDialog();
                    if (lcw.DialogResult.Value)
                    {
                        //cancel did not pressed
                        if (lcw.CloseGame)
                        {
                            //return to main menu button clicked
                            closeGame();
                        }
                        else
                        {
                            //connected to server now
                            startNewGame();
                        }
                    }
                    else
                    {
                        myToolbarControl.button_clue.IsEnabled = false;
                        myToolbarControl.button_restart.IsEnabled = false;
                    }
                });
            }
        }

        /// <summary>
        /// when the user is online this function is called to start the game
        /// </summary>
        private void startNewGame()
        {
            //open new window and asks for name from user
            NameDialog nameWin = new NameDialog();
            nameWin.Owner = Window.GetWindow(this);
            nameWin.ShowDialog();
            if (nameWin.DialogResult.Value)
            {
                myToolbarControl.button_clue.IsEnabled = true;
                myToolbarControl.button_restart.IsEnabled = true;
                myViewModel.openSingleplayGame(nameWin.GameName);
                myViewModel.lostConnection += lostConnectionEventHandle;
                myStack.Focus();
            }
            else
            {
                //if the user presssed cancel it back to the menu
                button_home_Click(null, null);
            }
        }

        /// <summary>
        /// close the game
        /// </summary>
        private void closeGame()
        {
            myViewModel.closeSingleplayGame();
            Window.GetWindow(this).Content = new MainMenu(myViewModel);
        }
        /// <summary>
        /// set enlements in xamel file
        /// </summary>
        private void setElements()
        {
            //listen to event that  winner won
            myMazeGridControl.messageLbl.IsVisibleChanged += delegate (object sender, DependencyPropertyChangedEventArgs e)
            {
                if (myMazeGridControl.messageLbl.Visibility == Visibility.Hidden)
                {
                    //setting new background for winning
                    Window.GetWindow(this).Background = Brushes.AliceBlue;
                    myToolbarControl.button_clue.IsEnabled = true;
                } else if (myMazeGridControl.messageLbl.Visibility == Visibility.Visible)
                {
                    //setting new background for winning
                    Window parentWindow = Window.GetWindow(this);
                    if (parentWindow!= null)
                    {
                        parentWindow.Background = Brushes.Pink;
                    }
                    myToolbarControl.button_clue.IsEnabled = false;
                    myMazeGridControl.messageLbl.Content = "Congratulations! You solved the maze!";
                }
            };
            
            myToolbarControl.button_home.Click += button_home_Click;
            myToolbarControl.button_restart.Click += button_restart_Click;
            myToolbarControl.button_clue.Click += button_hint_Click;
        }
        /// <summary>
        /// hanndle the event that the user doent connect to the server- 
        /// show dialog window
        /// </summary>
        private void lostConnectionEventHandle()
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)delegate {

                    LostConnectionWindow lcw = new LostConnectionWindow(myViewModel);
                    lcw.Owner = Window.GetWindow(this);
                    lcw.ShowDialog();
                    if (lcw.DialogResult.Value)
                    {
                        //cancel did not pressed
                        if (lcw.CloseGame)
                        {
                            //return to main menu button clicked
                            closeGame();
                        }
                    }

                });
            } catch (Exception e)
            {
                //do nothing
            }


        }
        /// <summary>
        /// event that accur when the user ask for go back to menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_home_Click(object sender, RoutedEventArgs e)
        {
            AreYouSureWindow askWin = new AreYouSureWindow();
            askWin.Owner = Window.GetWindow(this);
            askWin.ShowDialog();
            if (askWin.DialogResult.Value)
            {
                //player wishes to return to main menu
                closeGame();
            }
            else
            {
                myStack.Focus();
            }
        }
        /// <summary>
        /// event that accur when the user ask restart the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_restart_Click(object sender, RoutedEventArgs e)
        {
            AreYouSureWindow askWin = new AreYouSureWindow();
            askWin.Owner = Window.GetWindow(this);
            askWin.ShowDialog();
            if (askWin.DialogResult.Value)
            {
                //player wishes to restart game
                myMazeGridControl.messageLbl.Visibility = Visibility.Hidden;
                myMazeGridControl.restartGrid();
            }
            myStack.Focus();
        }
        /// <summary>
        /// event that accur when the user ask for an hint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_hint_Click(object sender, RoutedEventArgs e)
        {
            myMazeGridControl.showIdealMovement();
            myStack.Focus();
        }

    }
}
