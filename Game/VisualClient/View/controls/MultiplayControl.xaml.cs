﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VisualClient.ViewModel;

namespace VisualClient.View.controls
{
    /// <summary>
    /// Interaction logic for MultiplayControl.xaml
    /// </summary>
    public partial class MultiplayControl : UserControl
    {
        private MazeViewModel myViewModel;
        private MazeGridControl myYouMazeGridControl;
        private MazeGridControl myOtherMazeGridControl;
        private PlayToolbarControl myToolbarControl;

        public MultiplayControl(MazeViewModel mvm)
        {
            InitializeComponent();
            myViewModel = mvm;
            InitializeComponent();
            myYouMazeGridControl = (MazeGridControl)myYouStack.Children[0];
            myOtherMazeGridControl = (MazeGridControl)myOtherStack.Children[0];
            myToolbarControl = (PlayToolbarControl)myToolbar.Children[0];
            setElements();
            myYouStack.Focus();
        }

        public void start()
        {
            Window.GetWindow(this).Background = Brushes.Orange;
            if (myViewModel.isConnected())
            {
                startNewGame();
            }
            else
            {
                //message not connected dialog window
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    LostConnectionWindow lcw = new LostConnectionWindow(myViewModel);
                    Window parentWindwo = Window.GetWindow(this);
                    lcw.Owner = Window.GetWindow(this);
                    try
                    {
                        lcw.ShowDialog();
                    } catch (Exception e) {}
                    if (lcw.DialogResult.Value)
                    {
                    //cancel did not pressed
                    if (lcw.CloseGame)
                        {
                        //return to main menu button clicked
                        closeGame();
                        }
                        else
                        {
                        //connected to server now
                        startNewGame();
                        }
                    }
                    else
                    {
                        myToolbarControl.button_clue.IsEnabled = false;
                        myToolbarControl.button_restart.IsEnabled = false;
                    }
                });
            }
        }

        private void startNewGame()
        {
            //open new window and asks for name from user
            try {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    NameDialog nameWin = new NameDialog();
                    nameWin.Owner = Window.GetWindow(this);
                    nameWin.ShowDialog();
                    if (nameWin.DialogResult.Value)
                    {
                        myToolbarControl.button_clue.IsEnabled = false;
                        myToolbarControl.button_restart.IsEnabled = true;
                        myViewModel.openMultiplayGame(nameWin.GameName);
                        myViewModel.lostConnection += lostConnectionEventHandle;
                        myOtherMazeGridControl.DontMove = true;
                        myYouMazeGridControl.newMovement += sendMyMovement;
                        myViewModel.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
                        {
                            if(e.PropertyName.Equals("OtherMovement")) {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    myOtherMazeGridControl.Movement = myViewModel.OtherMovement;
                                    myYouStack.Focus();
                                });
                            }
                            
                        };
                        myYouStack.Focus();
                    }
                    else
                    {
                        closeGame();
                    }
                });
            } catch(Exception e){ }
        }

        private void sendMyMovement(int move)
        {
            myViewModel.sendMovement(move);
            myYouStack.Focus();
        }

        private void closeGame()
        {
            myViewModel.closeMultiplayGame();
            Window.GetWindow(this).Content = new MainMenu(myViewModel);
        }

        private void setElements()
        {
            myYouMazeGridControl.messageLbl.IsVisibleChanged += delegate (object sender, DependencyPropertyChangedEventArgs e)
            {
                if (myYouMazeGridControl.messageLbl.Visibility == Visibility.Hidden)
                {
                    //setting new default
                    Window.GetWindow(this).Background = Brushes.AliceBlue;
                    myToolbarControl.button_clue.IsEnabled = true;
                }
                else if (myYouMazeGridControl.messageLbl.Visibility == Visibility.Visible)
                {
                    //setting new background for winning
                    Window parentWindow = Window.GetWindow(this);
                    if (parentWindow != null)
                    {
                        parentWindow.Background = Brushes.Pink;
                    }
                    myToolbarControl.button_clue.IsEnabled = false;
                    myYouMazeGridControl.messageLbl.Content = "Congratulations! You won the game!";
                }
            };
            myOtherMazeGridControl.messageLbl.IsVisibleChanged += delegate (object sender, DependencyPropertyChangedEventArgs e)
            {
                if (myOtherMazeGridControl.messageLbl.Visibility == Visibility.Hidden)
                {
                    //setting new default
                    Window.GetWindow(this).Background = Brushes.Orange;
                    myToolbarControl.button_clue.IsEnabled = true;
                }
                else if (myOtherMazeGridControl.messageLbl.Visibility == Visibility.Visible)
                {
                    //setting new background for loosing
                    Window parentWindow = Window.GetWindow(this);
                    if (parentWindow != null)
                    {
                        parentWindow.Background = Brushes.Honeydew;
                    }
                    myToolbarControl.button_clue.IsEnabled = false;
                    myOtherMazeGridControl.messageLbl.Content = "Game Over! Opponent solved the maze!";
                    myYouMazeGridControl.DontMove = true;
                }
            };
            waitLbl.IsVisibleChanged += delegate (object sender, DependencyPropertyChangedEventArgs e)
            {
                if (waitLbl.Visibility == Visibility.Hidden)
                {
                    myToolbarControl.button_clue.IsEnabled = true;
                }
            };
            myToolbarControl.button_home.Click += button_home_Click;
            myToolbarControl.button_restart.Click += button_home_Click;
            myToolbarControl.button_clue.Click += button_hint_Click;
        }

        private void lostConnectionEventHandle()
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)delegate {

                    LostConnectionWindow lcw = new LostConnectionWindow(myViewModel);
                    lcw.Owner = Window.GetWindow(this);
                    lcw.ShowDialog();
                    if (lcw.DialogResult.Value)
                    {
                        //cancel did not pressed
                        if (lcw.CloseGame)
                        {
                            //return to main menu button clicked
                            closeGame();
                            return;
                        }
                    }
                    //pressed cancel or reconnected to server
                    myYouMazeGridControl.DontMove = true;
                    myToolbarControl.button_clue.IsEnabled = false;
                });
            }
            catch (Exception e){}
        }

        private void button_home_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    AreYouSureWindow askWin = new AreYouSureWindow();
                    askWin.Owner = Window.GetWindow(this);
                    askWin.ShowDialog();
                    if (askWin.DialogResult.Value)
                    {
                        //player wishes to return to main menu
                        closeGame();
                    }
                    else
                    {
                        myYouStack.Focus();
                    }
                });
            }catch (Exception ex) {}
        }

        private void button_hint_Click(object sender, RoutedEventArgs e)
        {
            myYouMazeGridControl.showIdealMovement();
            myYouStack.Focus();
        }

    }
}
