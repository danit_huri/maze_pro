﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;
public delegate void voidInt(int input);

namespace VisualClient.View.controls
{
    /// <summary>
    /// Interaction logic for NewMazeGrid.xaml
    /// </summary>
    public partial class MazeGridControl : UserControl
    {
        private DispatcherTimer myTimer;
        public event voidInt newMovement;
        /// <summary>
        /// Register Dependency Property
        /// </summary>
        public static readonly DependencyProperty OrderDependency =
            DependencyProperty.Register("Order", typeof(string), typeof(MazeGridControl), new PropertyMetadata(OnOrderChanged));
        /// <summary>
        /// maze presention class- connecting with data binding
        /// </summary>
        private MazePresentation myMaze;
        /// <summary>
        /// aproperty that initialize the maze with data binding
        /// </summary>
        private string order;
        private bool dontMove = false;
        private int movement;

        /// <summary>
        /// constructor.
        /// </summary>
        public MazeGridControl()
        {
            myMaze = new MazePresentation();
            InitializeComponent();
            gridCollection.ItemsSource = myMaze.MazePuzzleColl;
            fullGrid.Focus();
            //listen to event when the user winn
            myMaze.PropertyChanged += delegate (object sender, PropertyChangedEventArgs prop) 
            {
                if (prop.PropertyName.Equals("IsWinner"))
                {
                    if (myMaze.IsWinner)
                    {
                        messageLbl.Visibility = Visibility.Visible;
                        gridCollection.Focusable = false;
                        
                    }
                }
                if (prop.PropertyName.Equals("PlayerMovement"))
                {
                    if (newMovement != null)
                    {
                        newMovement(myMaze.PlayerMovement);
                    }
                }
            };
        }

        /// <summary>
        /// property - order
        /// </summary>
        public string Order
        {
            get
            {
                return (string)GetValue(OrderDependency);
            }
            set
            {
                SetValue(OrderDependency, value);
            }
        }

        public int Movement
        {
            get
            {
                return movement;
            }
            set
            {
                movement = value;
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    switch (value)
                    {
                        case 1:
                            myMaze.handleKeyUp();
                            break;
                        case 3:
                            myMaze.handleKeyDown();
                            break;
                        case 4:
                            myMaze.handleKeyLeft();
                            break;
                        case 2:
                            myMaze.handleKeyRight();
                            break;
                        default: break;
                    }
                });
            }
        }

        /// <summary>
        /// property - DontMove
        /// </summary>
        public bool DontMove
        {
            get
            {
               return dontMove;
            }
            set
            {
                dontMove = value;
                if (value == false)
                {
                    this.Focusable = false;
                    fullGrid.Focusable = false;
                }
            }
        }
        /// <summary>
        /// call function in my maze that mark the ideal next movement- hint
        /// </summary>
        public void showIdealMovement()
        {
           myMaze.IdealPlayerNewLocation();
        }
        /// <summary>
        /// call function in my maze that restart the maze to start point- restart
        /// </summary>
        public void restartGrid()
        {
            myMaze.resetBoard();
        }

        /// <summary>
        /// event that occur in the start when the order changed
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private static void OnOrderChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if ((string)e.NewValue != null)
            {
                ((MazeGridControl)obj).myMaze.setPresentation((string)e.NewValue);
            }
        }
        /// <summary>
        /// event that occur when the player type a key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyDownEventHandler(object sender, KeyEventArgs e)
        {
            if (myMaze.OnGame && !myMaze.IsWinner && !dontMove) { 
                if (myTimer != null)
                    myTimer.Stop();

                myTimer = new DispatcherTimer();
                myTimer.Tick += (s, args) =>
                {
                    //fetch data here...
                    switch (e.Key)
                    {
                        case Key.Up:
                            myMaze.handleKeyUp();
                            break;
                        case Key.Down:
                            myMaze.handleKeyDown();
                            break;
                        case Key.Left:
                            myMaze.handleKeyLeft();
                            break;
                        case Key.Right:
                            myMaze.handleKeyRight();
                            break;
                        default: break;
                    }
                    this.Focus();
                    this.fullGrid.Focus();
                    e.Handled = true;

                    myTimer.Stop();
                };
                myTimer.Interval = TimeSpan.FromMilliseconds(50);
                myTimer.Start();
            }
        }
        
    }
}
