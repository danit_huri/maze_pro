﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace VisualClient.View
{
    /// <summary>
    /// Interaction logic for NameDialog.xaml
    /// </summary>
    public partial class NameDialog : Window
    {
        /// <summary>
        /// tha name of the game
        /// </summary>
        private string gameName;

        /// <summary>
        /// constructor
        /// </summary>
        public NameDialog()
        {
            InitializeComponent();
            this.textBoxName.Text = "Enter name";
        }
        /// <summary>
        /// catch the event of ip textboe GotMouseCapture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxIP_GotMouseCapture(object sender, MouseEventArgs e)
        {
            this.textBoxName.Text = "";
        }
        /// <summary>
        /// catch the event of ip textboe LostFocus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxIP_LostFocus(object sender, RoutedEventArgs e)
        {
            if(this.textBoxName.Text.Equals(""))
            {
                this.textBoxName.Text = "Enter name";
            }
        }
        /// <summary>
        /// catch the event of buttonSave click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            if(this.textBoxName.Text.Equals(""))
            {
                DialogResult = false;
            }
            else
            {
                this.gameName = this.textBoxName.Text;
                //nameChanged();
                DialogResult = true;
            }
            Close();
        }
        /// <summary>
        /// property-gameName
        /// </summary>
        public string GameName
        {
            get
            {
                return gameName;
            }

            set
            {
                gameName = value;
            }
        }
    }
}
