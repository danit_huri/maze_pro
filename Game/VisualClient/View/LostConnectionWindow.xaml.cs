﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VisualClient.ViewModel;

namespace VisualClient.View
{
    /// <summary>
    /// Interaction logic for LostConnectionWindow.xaml
    /// </summary>
    public partial class LostConnectionWindow : Window
    {
        /// <summary>
        /// argument of my view-model
        /// </summary>
        private MazeViewModel myViewModel;
        /// <summary>
        /// boolean property to close the game or not
        /// </summary>
        private bool closeGame;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mvm"></param>
        public LostConnectionWindow(MazeViewModel mvm)
        {
            InitializeComponent();
            myViewModel = mvm;
        }
        /// <summary>
        /// property- closeGame
        /// </summary>
        public bool CloseGame
        {
            get
            {
                return closeGame;
            }

            set
            {
                closeGame = value;
            }
        }
        /// <summary>
        /// catch an event the occur when the user ask for reconnect to the server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReconnectButton_Click(object sender, RoutedEventArgs e)
        {
            //connecting the server
            myViewModel.connectToServer();
            if (myViewModel.isConnected())
            {
                DialogResult = true;
                closeGame = false;
                this.Close();
            } else
            {
                //error massege that the connction faild
                MsgLbl.Content = "Failed connecting to server";
            }
        }
        /// <summary>
        /// catch an event the occur when the user ask for back to menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeGameBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            closeGame = true;
            Close();
        }
    }

}
