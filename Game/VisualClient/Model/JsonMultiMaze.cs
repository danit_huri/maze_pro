﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    public class JsonMultiMaze
    {
        /// <summary>
        /// name of mulyGame
        /// </summary>
        private string name;
        /// <summary>
        /// name of the maze
        /// </summary>
        private string mazeName;
        /// <summary>
        /// json maze of user
        /// </summary>
        private JsonMaze you;
        /// <summary>
        /// json maze of component
        /// </summary>
        private JsonMaze other;
        /// <summary>
        /// property-name
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        /// <summary>
        /// property-mazeName
        /// </summary>
        public string MazeName
        {
            get
            {
                return mazeName;
            }

            set
            {
                mazeName = value;
            }
        }
        /// <summary>
        /// property-you
        /// </summary>
        public JsonMaze You
        {
            get
            {
                return you;
            }

            set
            {
                you = value;
            }
        }
        /// <summary>
        /// property-other
        /// </summary>
        public JsonMaze Other
        {
            get
            {
                return other;
            }

            set
            {
                other = value;
            }
        }
    }
}
