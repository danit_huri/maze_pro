﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    public class JsonMessage
    {
        /// <summary>
        /// message
        /// </summary>
        private string message;
        /// <summary>
        /// property-message
        /// </summary>
        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }
    }
}
