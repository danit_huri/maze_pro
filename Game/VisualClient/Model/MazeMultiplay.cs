﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    public class MazeMultiplay :Multiplay<JsonMaze>
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name">the name of the multyplay game</param>
        public MazeMultiplay(string name):base(name) { }

    }
}
