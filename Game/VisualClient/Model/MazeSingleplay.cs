﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    public class MazeSingleplay : Singleplay<JsonMaze>
    {

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name">the name of the singleplay game</param>
        public MazeSingleplay(string name):base(name) {}

        /// <summary>
        /// ovveride tostring function 
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            string str;
            if (myObj == null)
            {
                return "";
            }
            else
            {
                str = myObj.Maze;
            }

            string strSol;
            //for cases when solution is not needed - like in multiplayer game(for other player)
            if (myObjSolution == null)
            {
                strSol = "";
            }
            else
            {
                strSol = myObjSolution.Maze;
            }

            string fullStr = str + " " + strSol;
            StringBuilder strB = new StringBuilder(fullStr);
            //adding * and # to the string full string
            int n = int.Parse(ConfigurationManager.AppSettings["N"]);
            int m = int.Parse(ConfigurationManager.AppSettings["M"]);
            int row = myObj.Start.Row *( 2* m-1 ) * 2;
            int col = myObj.Start.Col * 2;
            strB[row + col] = '*';
            row = myObj.End.Row * (2 * m - 1) * 2;
            col = myObj.End.Col * 2;
            strB[row + col] = '#';
            fullStr = strB.ToString();
            return fullStr;
        }

    }
}
