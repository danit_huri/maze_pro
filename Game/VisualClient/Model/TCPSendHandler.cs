﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    /// <summary>
    /// handles the TCP connection sending data to server
    /// </summary>
    public class TCPSendHandler
    {
        /// <summary>
        /// socket of the server
        /// </summary>
        private Socket server;
        /// <summary>
        /// the string to send to server
        /// </summary>
        private string commandToSend;
        /// <summary>
        /// indicate if somthing changed
        /// </summary>
        private bool changed;
        /// <summary>
        /// flag to stop the connnection
        /// </summary>
        private bool stopFlag;
        public event voidFunc lostConnection;


        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="sock">the socket of the server</param>
        public TCPSendHandler(Socket sock)
        {
            server = sock;
        }

        /// <summary>
        /// send masseges to the server, when "exit" massege sent we close the connection.
        /// </summary>
        public void handle()
        {
            stopFlag = false;
            while (!stopFlag)
            {
                if (changed)
                {
                    try {
                        server.Send(Encoding.ASCII.GetBytes(commandToSend));
                        changed = false;
                    } catch(Exception e)
                    {
                        //communication with server failed
                        stopFlag = true;
                        lostConnection();
                    }
                }       
            }
        }
        /// <summary>
        /// send a message to the server
        /// </summary>
        public string CommandToSend
        {
            set
            {
                commandToSend = value;
                changed = true;
            }
        }
        /// <summary>
        /// activate stop flag
        /// </summary>
        public void stop()
        {
            stopFlag = true;
        }
    }
}
