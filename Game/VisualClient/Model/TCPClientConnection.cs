﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace VisualClient.Model
{
    public class TCPClientConnection : IClientConnection
    {
        IPEndPoint ipep;
        /// <summary>
        /// the server socket
        /// </summary>
        Socket server;
        /// <summary>
        /// handler that recive messages from the server
        /// </summary>
        TCPReceiveHandler receiver;
        /// <summary>
        /// handler that send messages to the server
        /// </summary>
        TCPSendHandler sender;
        /// <summary>
        /// list of tasks- thread pool
        /// </summary>
        List<Task> myTasks;
        /// <summary>
        /// event that pop when got a message from the server
        /// </summary>
        public event voidFunc receivedFromServer;
        /// <summary>
        /// event that pop when the connection is lost
        /// </summary>
        public event voidFunc lostConnection;

        /// <summary>
        /// connecting to the server
        /// </summary>
        /// <param name="IP">ip of the server</param>
        /// <param name="portNumber">port number of the server</param>
        public bool connect(string IP, int portNumber)
        {
            ipep = new IPEndPoint(IPAddress.Parse(IP), portNumber);
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                server.Connect(ipep);
            }
            //error in connection
            catch (SocketException e)
            {
                //update that fail in connection
                return false;
            }

            receiver = new TCPReceiveHandler(server);
            receiver.recievedFromServer += delegate ()
            {
                receivedFromServer();
            };
            receiver.lostConnection += delegate ()
            {
                lostConnection();
            };
            sender = new TCPSendHandler(server);
            sender.lostConnection += delegate ()
            {
                lostConnection();
            };

            return true;
        }
        /// <summary>
        /// start communicate with the server
        /// </summary>
        public void start()
        {
            //tasks of the client for sending some commands one by one
            myTasks = new List<Task>();
            myTasks.Add(Task.Factory.StartNew(receiver.handle));
            myTasks.Add(Task.Factory.StartNew(sender.handle));
            //wating all the threads to finish
            Task.WaitAll(myTasks.ToArray());
        }
        /// <summary>
        /// write a message to the server
        /// </summary>
        /// <param name="command">the command message</param>
        public void write(string command)
        {
            sender.CommandToSend = command;
        }
        /// <summary>
        /// read a message from the server
        /// </summary>
        /// <returns>the message that accecpt</returns>
        public string read()
        {
            return this.receiver.StringFromServer;
        }
        /// <summary>
        /// close the connection with the server
        /// </summary>
        public void disconnect()
        {
            sender.stop();
            receiver.stop();
            server.Shutdown(SocketShutdown.Both);
            server.Close();
        }        
    }
}
