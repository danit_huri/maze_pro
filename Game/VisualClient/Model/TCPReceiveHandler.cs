﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    /// <summary>
    /// handles the TCP connection receiving data from server
    /// </summary>
    public class TCPReceiveHandler
    {
        /// <summary>
        /// socket of the server
        /// </summary>
        private Socket server;
        /// <summary>
        /// flag to stop the connnection
        /// </summary>
        private bool stopFlag;
        /// <summary>
        /// pop event that got a message from the server
        /// </summary>
        public event voidFunc recievedFromServer;
        /// <summary>
        /// pop event that lost connection with the server
        /// </summary>
        public event voidFunc lostConnection;
        /// <summary>
        /// the string that got from the server
        /// </summary>
        private string stringFromServer;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="sock">the socket of the server</param>
        public TCPReceiveHandler (Socket sock)
        {
            server = sock;
        }

        /// <summary>
        /// receive masseges from the server
        /// </summary>
        public void handle()
        {
            int recv;
            stopFlag = false;
            while (!stopFlag){
                byte[] data = new byte[65536];
                try
                {
                    recv = server.Receive(data);
                    stringFromServer = Encoding.ASCII.GetString(data, 0, recv);
                    recievedFromServer();
                } catch (Exception e)
                {
                    //handle loss of connection with server
                    stopFlag = true;
                    lostConnection();
                }
                
            }
        }
        /// <summary>
        /// activate stop flag
        /// </summary>
        public void stop()
        {
            stopFlag = true;
        }
        /// <summary>
        /// propert- return string message from server
        /// </summary>
        public string StringFromServer
        {
            get
            {
                return stringFromServer;
            }
        }

    }
}
