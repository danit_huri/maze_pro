﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace VisualClient.Model
{
    public class ClientMazeModel:IModel<JsonMaze>
    {
        /// <summary>
        /// argument of client connection
        /// </summary>
        private IClientConnection myConnection;
        /// <summary>
        /// boolean argument if we connected to the server
        /// </summary>
        private bool isConnected;
        /// <summary>
        /// argument of the single play
        /// </summary>
        private Singleplay<JsonMaze> mySingleplay;
        /// <summary>
        /// argument of the multy play
        /// </summary>
        private Multiplay<JsonMaze> myMultiplay;
        /// <summary>
        /// save the lat component movement
        /// </summary>
        private int otherMovement;

        //INotifyPropertyChanged implementation event
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="con">the client connection argument</param>
        public ClientMazeModel(IClientConnection con)
        {
            myConnection = con;
            //catch the event of recieving message fromm the server
            myConnection.receivedFromServer += delegate ()
            {
                string fromServer = myConnection.read();
                deserializeRead(fromServer);
            };
            //catch the event of lost connection with the server
            myConnection.lostConnection += delegate ()
            {
                IsConnected = false;
            };
            isConnected = false;
        }

        //IModel implementation functions
        /// <summary>
        /// connect to the server
        /// </summary>
        /// <param name="ip">ip number</param>
        /// <param name="port">server port number</param>
        public void connect(string ip, int port)
        {
            isConnected = myConnection.connect(ip, port);
        }
        /// <summary>
        /// reurn true or false if the user connected to the server
        /// </summary>
        /// <returns></returns>
        public bool isConnectedToServer()
        {
            return isConnected;
        }
        /// <summary>
        /// close theconnection with the server
        /// </summary>
        public void disconnect()
        {
            myConnection.disconnect();
        }
        /// <summary>
        /// start the connection
        /// </summary>
        public void start()
        {
            new Thread(delegate ()
            {
                myConnection.start();
            }).Start();
        }

        //properties implementation(from IModel)
        /// <summary>
        /// propert- otherMovement
        /// </summary>
        public int OtherMovement
        {
            get
            {
                return otherMovement;
            }
            set
            {
                otherMovement = value;
                NotifyPropertyChanged("OtherMovement");
            }
        }
        /// <summary>
        /// property-mySinglePlay
        /// </summary>
        public Singleplay<JsonMaze> MySingleplay
        {
            get
            {
                return mySingleplay;
            }

            set
            {
                mySingleplay = value;
                NotifyPropertyChanged("MySingleplay");
            }
        }
        /// <summary>
        /// property-myMultyPlay
        /// </summary>
        public Multiplay<JsonMaze> MyMultiplay
        {
            get
            {
                return myMultiplay;
            }

            set
            {
                myMultiplay = value;
                NotifyPropertyChanged("MyMultiplay");
            }
        }
        /// <summary>
        /// propert-isConnected
        /// </summary>
        public bool IsConnected
        {
            get
            {
                return isConnected;
            }

            set
            {
                isConnected = value;
                NotifyPropertyChanged("IsConnected");
            }
        }
        /// <summary>
        /// activate the event of propertChanged
        /// </summary>
        /// <param name="propName"></param>
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        //private methods
        /// <summary>
        /// deserialize the Json format of the maze
        /// </summary>
        /// <param name="str">string in Json format</param>
        private void deserializeRead(string str)
        {
            JavaScriptSerializer s = new JavaScriptSerializer();

            //check if it a multi maze json
            try
            {
                JsonMultiMaze multiMaze = s.Deserialize<JsonMultiMaze>(str);
                if (s.Serialize(multiMaze).Equals(str))
                {
                    if (myMultiplay != null && myMultiplay.GameName == multiMaze.Name)
                    {
                        //sychronized with current state of myMultiplay
                        MazeSingleplay other = new MazeSingleplay(multiMaze.Other.Name);
                        other.MyObj = multiMaze.Other;
                        myMultiplay.Other = other;
                        myMultiplay.ObjName = multiMaze.MazeName;
                        if (myMultiplay.You == null)
                        {
                            //multiplay game returned before the You solved maze
                            MazeSingleplay you = new MazeSingleplay(multiMaze.You.Name);
                            you.MyObj = multiMaze.You;
                            myMultiplay.You = you;
                        }
                        myConnection.write("solve " + multiMaze.You.Name + " 1");
                    }
                    return;
                }
            } catch (Exception e) { }
            try {
                //check if it a maze json
                JsonMaze oneMaze = s.Deserialize<JsonMaze>(str);
                if (s.Serialize(oneMaze).Equals(str))
                {
                    //check if it clear maze or solved maze
                    if (!oneMaze.Maze.Contains("2"))
                    {
                        //clear maze
                        if (mySingleplay != null && mySingleplay.Name == oneMaze.Name)
                        {
                            //if the maze is sychronized with the request for new maze
                            //set the singleplay maze as the json maze
                            mySingleplay.MyObj = oneMaze;

                            if (mySingleplay.MyObjSolution != null)
                            {
                                //if solve returned before generate
                                MySingleplay = mySingleplay;
                            }
                        }
                    }
                    else
                    {
                        //solved maze
                        if (mySingleplay != null && mySingleplay.Name == oneMaze.Name)
                        {
                            //if the maze is sychronized with the request for singleplay
                            mySingleplay.MyObjSolution = oneMaze;
                            if (mySingleplay.MyObj != null)
                            {
                                //if solve returned after generate
                                MySingleplay = mySingleplay;
                            }
                        }
                        else if (myMultiplay != null)
                        {
                            //multiplay game now open
                            if (myMultiplay.You != null)
                            {
                                //solved maze arrived after multiplay request returned a string
                                if (myMultiplay.You.Name == oneMaze.Name)
                                {
                                    myMultiplay.You.MyObjSolution = oneMaze;
                                    MyMultiplay = myMultiplay;
                                }
                            }
                        }
                    }
                    return;
                }
            } catch (Exception ex) { }
            try {
                //check if it a movement json
                JsonMovement movement = s.Deserialize<JsonMovement>(str);
                if (s.Serialize(movement).Equals(str))
                {
                    if (MyMultiplay != null)
                    {
                        if (movement.Name == MyMultiplay.GameName)
                        {
                            OtherMovement = getMovement(movement.Move);
                            Thread.Sleep(250);
                            OtherMovement = 5;
                        }
                    }
                    return;
                }
            } catch (Exception f) { }
            try { 
                //check if it a message json
                JsonMessage msg = s.Deserialize<JsonMessage>(str);
                if (s.Serialize(msg).Equals(str))
                {
                    //ignore messages right now
                    return;
                }
            }
            catch (Exception e) { }
        }
        /// <summary>
        /// the component made a movement
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        private int getMovement(string move)
        {
            switch (move)
            {
                case "up": return 1;
                case "right": return 2;
                case "down": return 3;
                case "left": return 4;
                default: return 0;
            }
        }
        /// <summary>
        /// opening single game
        /// </summary>
        /// <param name="name">the name of the game</param>
        public void openSingleplayGame(string name)
        {
            mySingleplay = new MazeSingleplay(name);
            myConnection.write("generate " + name + " 1");
            Thread.Sleep(250);
            myConnection.write("solve " + name + " 1");
        }
        /// <summary>
        /// close the single game
        /// </summary>
        public void closeSingleplayGame()
        {
            mySingleplay = null;
        }
        /// <summary>
        /// opening multy game
        /// </summary>
        /// <param name="name">the name of the game</param>
        public void openMultiplayGame(string name)
        {
            myMultiplay = new MazeMultiplay(name);
            myConnection.write("multiplayer " + name);
        }
        /// <summary>
        /// close the multy game
        /// </summary>
        public void closeMultiplayGame()
        {
            if (myMultiplay != null)
            {
                myConnection.write("close " + myMultiplay.GameName);
                myMultiplay = null;
            }
        
        }

        public void sendMovement(int direction)
        {
            string dirStr;
            switch (direction)
            {
                case 1:
                    dirStr = "up";
                    break;
                case 2:
                    dirStr = "right";
                    break;
                case 3: dirStr = "down";
                    break;
                case 4: dirStr = "left";
                    break;
                default: dirStr = "";
                    break;
            }
            if (!dirStr.Equals(""))
            {
                myConnection.write("play " + dirStr);
            }
        }
    }
}
