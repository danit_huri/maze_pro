﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualClient.general;

namespace VisualClient.Model
{
    public class JsonMaze
    {
        /// <summary>
        /// name of maze
        /// </summary>
        private string name;
        /// <summary>
        /// the sting maze
        /// </summary>
        private string maze;
        /// <summary>
        /// point of atsrt
        /// </summary>
        private Point start;
        /// <summary>
        /// point of end
        /// </summary>
        private Point end;
        /// <summary>
        /// property-name
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        /// <summary>
        /// property-maze
        /// </summary>
        public string Maze
        {
            get
            {
                return maze;
            }

            set
            {
                maze = value;
            }
        }
        /// <summary>
        /// property-start
        /// </summary>
        public Point Start
        {
            get
            {
                return start;
            }

            set
            {
                start = value;
            }
        }
        /// <summary>
        /// property-end
        /// </summary>
        public Point End
        {
            get
            {
                return end;
            }

            set
            {
                end = value;
            }
        }
    }
}
