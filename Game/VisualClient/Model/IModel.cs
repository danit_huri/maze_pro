﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public delegate void voidFunc();

namespace VisualClient.Model
{
    public interface IModel<T>: INotifyPropertyChanged
    {
        //connection to server
        /// <summary>
        /// connect to the server
        /// </summary>
        /// <param name="IP">server ip number</param>
        /// <param name="portNumber">server port number</param>
        /// <returns></returns>
        void connect(string ip, int port);
        /// <summary>
        /// close the connection with the server
        /// </summary>
        void disconnect();
        /// <summary>
        /// start communicate with the server
        /// </summary>
        void start();

        //properties
        /// <summary>
        /// argument of the single play
        /// </summary>
        Singleplay<T> MySingleplay { set; get; }
        /// <summary>
        /// argument of the multy play
        /// </summary>
        Multiplay<T> MyMultiplay { set; get; }
        /// <summary>
        /// save the lat component movement
        /// </summary>
        int OtherMovement { set; get; }
        /// <summary>
        /// boolean argument if we connected to the server
        /// </summary>
        bool IsConnected { set; get; }

        //commands functions
        /// <summary>
        /// opening single game
        /// </summary>
        /// <param name="name">the name of the game</param>
        void openSingleplayGame(string name);
        /// <summary>
        /// close the single game
        /// </summary>
        void closeSingleplayGame();
        /// <summary>
        /// opening multy game
        /// </summary>
        /// <param name="name">the name of the game</param>
        void openMultiplayGame(string name);
        /// <summary>
        /// close the multy game
        /// </summary>
        void closeMultiplayGame();
        void sendMovement(int direction);
    }
}
