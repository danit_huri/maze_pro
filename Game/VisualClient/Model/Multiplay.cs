﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    public abstract class Multiplay<T>
    {
        /// <summary>
        /// single play of user
        /// </summary>
        protected Singleplay<T> you;
        /// <summary>
        /// single play of component
        /// </summary>
        protected Singleplay<T> other;
        /// <summary>
        /// the name of the game
        /// </summary>
        protected string gameName;
        /// <summary>
        /// the object name
        /// </summary>
        protected string objName;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name">name of the game</param>
        public Multiplay(string name)
        {
            gameName = name;
        }
        /// <summary>
        /// property-you
        /// </summary>
        public Singleplay<T> You
        {
            get
            {
                return you;
            }

            set
            {
                you = value;
            }
        }
        /// <summary>
        /// property-other
        /// </summary>
        public Singleplay<T> Other
        {
            get
            {
                return other;
            }

            set
            {
                other = value;
            }
        }
        /// <summary>
        /// property-objName
        /// </summary>
        public string ObjName
        {
            get
            {
                return objName;
            }

            set
            {
                objName = value;
            }
        }
        /// <summary>
        /// property-gameName
        /// </summary>
        public string GameName
        {
            get
            {
                return gameName;
            }
        }
    }
}
