﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    public abstract class Singleplay<T>
    {
        /// <summary>
        /// my obect of game
        /// </summary>
        protected T myObj;
        /// <summary>
        /// my object solution of the game
        /// </summary>
        protected T myObjSolution;
        /// <summary>
        /// the nameof the game
        /// </summary>
        protected string name;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="mazeName">the name of the game</param>
        public Singleplay(string mazeName)
        {
            name = mazeName;
        }
        /// <summary>
        /// property-myObj
        /// </summary>
        public T MyObj
        {
            get
            {
                return myObj;
            }
            set
            {
                myObj = value;
            }
        }
        /// <summary>
        /// property-myObjSolution
        /// </summary>
        public T MyObjSolution
        {
            get
            {
                return myObjSolution;
            }
            set
            {
                myObjSolution = value;
            }
        }
        /// <summary>
        /// property-name
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }
    }
}
