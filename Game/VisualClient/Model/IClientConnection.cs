﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    public interface IClientConnection
    {
        /// <summary>
        /// event that pop when got a message from the server
        /// </summary>
        event voidFunc receivedFromServer;
        /// <summary>
        /// event that pop when the connection is lost
        /// </summary>
        event voidFunc lostConnection;
        /// <summary>
        /// connect to the server
        /// </summary>
        /// <param name="IP">server ip number</param>
        /// <param name="portNumber">server port number</param>
        /// <returns></returns>
        bool connect(string IP, int portNumber);
        /// <summary>
        /// start communicate with the server
        /// </summary>
        void start();
        /// <summary>
        /// write a message to the server
        /// </summary>
        /// <param name="command">the command message</param>
        void write(string command);
        /// <summary>
        /// read a message from the server
        /// </summary>
        /// <returns>the message that accecpt</returns>
        string read();
        /// <summary>
        /// close the connection with the server
        /// </summary>
        void disconnect();
    }
}
