﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualClient.Model
{
    public class JsonMovement
    {
        /// <summary>
        /// name
        /// </summary>
        private string name;
        /// <summary>
        ///  the move
        /// </summary>
        private string move;
        /// <summary>
        /// property-name
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        /// <summary>
        /// property-move
        /// </summary>
        public string Move
        {
            get
            {
                return move;
            }

            set
            {
                move = value;
            }
        }
    }
}
