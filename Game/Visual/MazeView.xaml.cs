﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Windows.Resources;
using System.Threading;

namespace Visual
{
    /// <summary>
    /// Interaction logic for MazeView.xaml
    /// </summary>
    public partial class MazeView : UserControl
    {
        private Label userLocation;
        private Label startLocation;
        private Label finishLocation;
        private Label[,] mazeLabels;
        private char[,] mazeNumbers;
        private int n;
        private int m;
        private int[,] solution; 
        private Brush passBrush;
        private Brush flashBrush;
        private Brush viaBrush;
        private Brush wallBrush;
        private Brush playerBrush;
        private Brush goalBrush;

        public MazeView(string st, string st2)
        {
            InitializeComponent();

            n = int.Parse(ConfigurationManager.AppSettings["N"]);
            m = int.Parse(ConfigurationManager.AppSettings["M"]);
            int n2 = 2 * n - 1, m2 = 2 * m - 1;
            n = n2;
            m = m2;
            
            DynamicGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
            DynamicGrid.VerticalAlignment = VerticalAlignment.Stretch;
            DynamicGrid.Background = new SolidColorBrush(Colors.White);

            for (int i = 0; i < n2; i++)
            {
                DynamicGrid.RowDefinitions.Add(new RowDefinition());
            }
            for (int j = 0; j < m2; j++)
            {
                DynamicGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            int index = 0;
            mazeLabels = new Label[n, m];
            mazeNumbers = new char[n, m];
            char c;
            wallBrush = (Brush)getImage("images/water.jpg");
            passBrush = (Brush)getImage("images/grass.jpg");
            flashBrush = (Brush)getImage("images/flashGrass.jpg");
            viaBrush = (Brush)getImage("images/yellowGrass.jpg");
            playerBrush = (Brush)getImage("images/player1.jpg");
            goalBrush = (Brush)getImage("images/player2.jpg");


            for (int i = 0; i < n2; i++)
            {
                for (int j = 0; j < m2; j++)
                {
                    c = st[index];
                    index++;
                    Label l = new Label();
                    
                    mazeLabels[i, j] = l;
                    mazeNumbers[i, j] = c;
                    

                    l.Name = "m" + i + j;
                    if (c == '1')
                    {
                        l.Background = wallBrush;
                    }
                    else if (c == '0')
                    {
                        l.Background = passBrush;
                    }
                    else if (c == '*')
                    {

                        l.Background = playerBrush;
                        userLocation = l;
                        startLocation = l;
                    }
                    else if (c == '#')
                    {
                        l.Background = goalBrush;
                        finishLocation = l;
                    }
                    Grid.SetRow(l, i);
                    Grid.SetColumn(l, j);
                    l.VerticalAlignment = VerticalAlignment.Stretch;
                    l.HorizontalAlignment = HorizontalAlignment.Stretch;
                    DynamicGrid.Children.Add(l);

                }
            }
            
            index = 0;
            solution = new int[n, m];
            for (int i = 0; i < n2; i++)
            {
                for (int j = 0; j < m2; j++)
                {
                    solution[i, j] = int.Parse(st2[index].ToString());
                    index++;
                }
            }

            MarkSolution();


            Loaded += UserControl_Loaded;
        }

        private Object getImage(string imagePath)
        {
            Uri resourceUri = new Uri(imagePath, UriKind.Relative);
            StreamResourceInfo streamInfo = Application.GetResourceStream(resourceUri);
            BitmapFrame tempBit = BitmapFrame.Create(streamInfo.Stream);
            var brush = new ImageBrush();
            brush.ImageSource = tempBit;
            return brush;
        }
        
       
        public void LeftPressed()
        {
            int colN = Grid.GetColumn(userLocation);
            int rowN = Grid.GetRow(userLocation);

            if (colN > 0)
            {
                updateUserLocation(rowN, colN - 1);
            }

        }
        public void RightPressed()
        {
            int colN = Grid.GetColumn(userLocation);
            int rowN = Grid.GetRow(userLocation);

            if (colN < m - 1)
            {
                updateUserLocation(rowN, colN + 1);
            }
        }
        public void UpPressed()
        {
            int colN = Grid.GetColumn(userLocation);
            int rowN = Grid.GetRow(userLocation);

            if (rowN > 0)
            {
                updateUserLocation(rowN - 1, colN);
            }
        }
        public void DownPressed()
        {
            int colN = Grid.GetColumn(userLocation);
            int rowN = Grid.GetRow(userLocation);

            if (rowN < n - 1)
            {
                updateUserLocation(rowN + 1, colN);
            }
          
        }
        private void updateUserLocation(int newRow, int newCol)
        {
            int preRow, preCol;
            preRow = Grid.GetRow(userLocation);
            preCol = Grid.GetColumn(userLocation);
            //can pass
            if (mazeNumbers[newRow, newCol] == '0')
            {
                userLocation.Background = viaBrush;
                mazeNumbers[preRow, preCol] = '2';
                userLocation = mazeLabels[newRow, newCol];
                userLocation.Background = playerBrush;
            }
            //passed before
            else if (mazeNumbers[newRow, newCol] == '2')
            {
                userLocation.Background = passBrush;
                mazeNumbers[preRow, preCol] = '0';
                userLocation = mazeLabels[newRow, newCol];
                userLocation.Background = playerBrush;
            }
            //the user win
            else if (mazeNumbers[newRow, newCol] == '#')
            {
                userLocation.Background = viaBrush;
                mazeNumbers[preRow, preCol] = '2';
                userLocation = mazeLabels[newRow, newCol];
                userLocation.Background = playerBrush;
                Window.GetWindow(this).Background = (Brush)getImage("images/winner.jpg");
            }
        }

        public void RestartMaze()
        {
            userLocation.Background = passBrush;

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if(mazeNumbers[i,j] == '2')
                    {
                        mazeLabels[i, j].Background = passBrush;
                        mazeNumbers[i, j] = '0';
                    }

                }
            }
            userLocation = startLocation;
            userLocation.Background = playerBrush;
            finishLocation.Background = goalBrush;
            int rf = Grid.GetRow(finishLocation);
            int cf = Grid.GetColumn(finishLocation);
            mazeNumbers[rf, cf] = '#';
            Window.GetWindow(this).Background = (Brush)getImage("images/background.jpg");
        }
        public int Hint()
        {
            //up-1 right-2 down-3 left-4
            int rowPos = Grid.GetRow(userLocation);
            int colPos = Grid.GetColumn(userLocation);
            //the user on the solution
            if (solution[rowPos,colPos]>2)
            {
                //go right
                if ((colPos + 1 < m) && (solution[rowPos, colPos + 1]== solution[rowPos, colPos] + 1))
                {
                    flashingLabel(mazeLabels[rowPos, colPos + 1]);
                    return 2;
                }
                //go left
                if ((colPos - 1 >= 0)&&(solution[rowPos, colPos - 1] == solution[rowPos, colPos] + 1))
                {
                    flashingLabel(mazeLabels[rowPos, colPos - 1]);
                    return 4;
                }
                //go down
                if ((rowPos + 1 < n) &&(solution[rowPos + 1, colPos] == solution[rowPos, colPos] + 1))
                {
                    flashingLabel(mazeLabels[rowPos + 1, colPos]);
                    return 3;
                }
                //go up
                if ((rowPos - 1 >= 0) && (solution[rowPos - 1, colPos] == solution[rowPos, colPos] + 1))
                {
                    flashingLabel(mazeLabels[rowPos - 1, colPos]);
                    return 1;
                }
            }
            else
            {

                //go right
                if ((colPos + 1 < m) && (mazeNumbers[rowPos, colPos + 1] =='2'))
                {
                    flashingLabel(mazeLabels[rowPos, colPos + 1]);
                    return 2;
                }
                //go left
                if ((colPos - 1 >= 0) && (mazeNumbers[rowPos, colPos - 1] == '2'))
                {
                    flashingLabel(mazeLabels[rowPos, colPos - 1]);
                    return 4;
                }
                //go down
                if ((rowPos + 1 < n) && (mazeNumbers[rowPos + 1, colPos] == '2'))
                {
                    flashingLabel(mazeLabels[rowPos + 1, colPos]);
                    return 3;
                }
                //go up
                if ((rowPos - 1 >= 0) && (mazeNumbers[rowPos - 1, colPos] == '2'))
                {
                    flashingLabel(mazeLabels[rowPos - 1, colPos]);
                    return 1;
                }
            }
            return 0;
        }
        private void MarkSolution()
        {
            int starti, startj;
            starti = Grid.GetRow(startLocation);
            startj = Grid.GetColumn(startLocation);
            RecursiveNumber(3, starti, startj);

        }
        private void RecursiveNumber(int num, int currenti, int currentj)
        {
            //stop condition
            if(mazeLabels[currenti, currentj] == finishLocation)
            {
                solution[currenti, currentj] = num;
                return;
            }
            solution[currenti, currentj] = num;
            //go right
            if ((currentj + 1 < m) && (solution[currenti, currentj + 1] == 2))
            {
                RecursiveNumber(num + 1, currenti, currentj + 1);
                return;
            }
            //go left
            if ((currentj - 1 >= 0) && (solution[currenti, currentj - 1] == 2))
            {
                RecursiveNumber(num + 1, currenti, currentj - 1);
                return;
            }
            //go down
            if ((currenti + 1 < n) && (solution[currenti + 1, currentj] == 2))
            {
                RecursiveNumber(num + 1, currenti + 1, currentj);
                return;
            }
            //go up
            if ((currenti - 1 >= 0) && (solution[currenti - 1, currentj] == 2))
            {
                RecursiveNumber(num + 1, currenti - 1, currentj);
                return;
            }

        }
        private void flashingLabel(Label l)
        {
            for (int i = 0; i < 3; i++)
            {
                l.Background = flashBrush;
                Thread.Sleep(1000);
                l.Background = passBrush;
                Thread.Sleep(1000);
            }
        } 

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    UpPressed();
                    break;
                case Key.Down:
                    DownPressed();
                    break;
                case Key.Left:
                    LeftPressed();
                    break;
                case Key.Right:
                    RightPressed();
                    break;
            }
        }
    }
}
