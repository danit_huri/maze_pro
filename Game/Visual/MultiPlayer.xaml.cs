﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Visual
{
    /// <summary>
    /// Interaction logic for MultiPlayer.xaml
    /// </summary>
    public partial class MultiPlayer : UserControl
    {
        private MazeView myMaze;
        private MazeView componentMaze;


        public MultiPlayer(string st, string st2, string stCom, string stCom2)
        {
            InitializeComponent();
           /* mainGrid.RowDefinitions.Add(new RowDefinition());
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
            Grid.SetColumn(mazeGridMe, 0);
            Grid.SetRow(mazeGridMe, 0);
            mainGrid.Children.Add(mazeGridMe);
            Grid.SetColumn(mazeGridOther, 1);
            Grid.SetRow(mazeGridOther, 0);
            mainGrid.Children.Add(mazeGridOther);
            */
            myMaze = new MazeView(st, st2);
            myMaze.HorizontalAlignment = HorizontalAlignment.Stretch;
            myMaze.VerticalAlignment = VerticalAlignment.Stretch;
            mazeGridMe.RowDefinitions.Add(new RowDefinition());
            mazeGridMe.ColumnDefinitions.Add(new ColumnDefinition());
            Grid.SetColumn(myMaze, 0);
            Grid.SetRow(myMaze, 0);
            mazeGridMe.Children.Add(myMaze);

            componentMaze = new MazeView(st, st2);
            componentMaze.HorizontalAlignment = HorizontalAlignment.Stretch;
            componentMaze.VerticalAlignment = VerticalAlignment.Stretch;
            mazeGridOther.RowDefinitions.Add(new RowDefinition());
            mazeGridOther.ColumnDefinitions.Add(new ColumnDefinition());
            Grid.SetColumn(componentMaze, 0);
            Grid.SetRow(componentMaze, 0);
            mazeGridOther.Children.Add(componentMaze);

           
            Loaded += Load;
        }

        private void Load(object sender, EventArgs e)
        {
            mazeGridMe.Focusable = true;
            Keyboard.Focus(mazeGridMe);
        }

        private void button_home_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Content = new MainMenu();
        }

        private void button_restart_Click(object sender, RoutedEventArgs e)
        {
            myMaze.RestartMaze();
        }

        private void button_clue_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonUp_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonLeft_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonDown_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonRight_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
