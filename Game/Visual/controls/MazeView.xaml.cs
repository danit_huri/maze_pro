﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;

namespace Visual.controls
{
    /// <summary>
    /// Interaction logic for MazeView.xaml
    /// </summary>
    public partial class MazeView : UserControl
    {
        private int n, m;
        public static readonly DependencyProperty OrderProperty;
        Label[,] myMaze;

        static MazeView()
        {
            OrderProperty = DependencyProperty.RegisterAttached("Order",
          typeof(string), typeof(MazeView),
          new FrameworkPropertyMetadata() { BindsTwoWayByDefault = true });
        }
        public MazeView()
        {
            InitializeComponent();
            

            n = int.Parse(ConfigurationManager.AppSettings["N"]);
            m = int.Parse(ConfigurationManager.AppSettings["M"]);
            int n2 = 2 * n - 1, m2 = 2 * m - 1;
            /*********creating grid*************/
            Grid DynamicGrid = new Grid();
            DynamicGrid.Width = 400;
            DynamicGrid.HorizontalAlignment = HorizontalAlignment.Left;
            DynamicGrid.VerticalAlignment = VerticalAlignment.Top;
            DynamicGrid.ShowGridLines = true;
            DynamicGrid.Background = new SolidColorBrush(Colors.LightSteelBlue);

            for (int i = 0; i < n2; i++)
            {
                DynamicGrid.RowDefinitions.Add(new RowDefinition());
            }
            for (int j = 0; j < m2; j++)
            {
                DynamicGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }
               
           
            myMaze = new Label[n, m];
           
            for (int i = 0; i < n2; i++)
            {
                for (int j = 0; j < m2; j++)
                {
                    Grid.SetRow(myMaze[i, j], i);
                    Grid.SetColumn(myMaze[i, j], j);
                    myMaze[i, j].Name = "m" + i + j;
                    myMaze[i, j].Content = "-";
                }
            }
        }

        public string Order
        {
            get
            {
                string s = "";
                foreach (Label l in myMaze)
                {
                    s += l.Content.ToString();
                }
                return s;
            }

            set
            {
                string s = value;
                int n2 = 2 * n - 1, m2 = 2 * m - 1;
                for (int i = 0; i < n2; i++)
                {
                    for (int j = 0; j < m2; j++)
                        foreach (char c in s)
                        {
                            if (c == '1')
                                myMaze[i, j].Background = Brushes.Black;
                            else if (c == '0')
                                myMaze[i, j].Background = Brushes.White;
                            else if (c == '*')
                                myMaze[i, j].Background = Brushes.Red;
                        }

                }
            }
    }

        
    }
}
