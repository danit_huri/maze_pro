﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Visual
{
    /// <summary>
    /// Interaction logic for HintMassage.xaml
    /// </summary>
    public partial class HintMassage : Window
    {
        public HintMassage(int move)
        {
            InitializeComponent();
            switch (move)
            {
                case 1:
                    labelMassage.Content = "Move Up!";
                    break;
                case 2:
                    labelMassage.Content = "Move Right!";
                    break;
                case 3:
                    labelMassage.Content = "Move Down!";
                    break;
                case 4:
                    labelMassage.Content = "Move Left!";
                    break;
            }
        }
    }
}
