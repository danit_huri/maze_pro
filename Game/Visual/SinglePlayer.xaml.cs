﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;

namespace Visual
{
    /// <summary>
    /// Interaction logic for SinglePlayer.xaml
    /// </summary>
    public partial class SinglePlayer : UserControl
    {
        private MazeView singleMaze;
        public SinglePlayer(string st, string st2)
        {
            InitializeComponent();
            singleMaze = new MazeView(st, st2);
            mazeGrid.RowDefinitions.Add(new RowDefinition());
            mazeGrid.ColumnDefinitions.Add(new ColumnDefinition());
            Grid.SetColumn(singleMaze, 0);
            Grid.SetRow(singleMaze, 0);
            mazeGrid.Children.Add(singleMaze);
        }

       
        private void button_clue_Click(object sender, RoutedEventArgs e)
        {
            int move = singleMaze.Hint();
            //HintMassage hintMas = new HintMassage(move);
            //hintMas.ShowDialog();
        }
       
        private void buttonRight_Click(object sender, RoutedEventArgs e)
        {
            singleMaze.RightPressed();
        }

        private void buttonLeft_Click(object sender, RoutedEventArgs e)
        {
            singleMaze.LeftPressed();
        }

        private void buttonDown_Click(object sender, RoutedEventArgs e)
        {
            singleMaze.DownPressed();
        }

        private void buttonUp_Click(object sender, RoutedEventArgs e)
        {
            singleMaze.UpPressed();
        }
        
        private void button_home_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Content = new MainMenu();
        }

        private void button_restart_Click(object sender, RoutedEventArgs e)
        {
            singleMaze.RestartMaze();
        }
    }
}
