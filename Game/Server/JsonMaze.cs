﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game;

namespace Server
{
    /// <summary>
    /// class for representing a maze in json format
    /// </summary>
    public class JsonMaze
    {
        /// <summary>
        /// the maze
        /// </summary>
        private Maze myMaze;
        /// <summary>
        /// name of the maze
        /// </summary>
        private string name;
        /// <summary>
        /// the maze in string format
        /// </summary>
        private string maze;
        /// <summary>
        /// the start of the maze
        /// </summary>
        private Cell start;
        /// <summary>
        /// the end of the maze
        /// </summary>
        private Cell end;

        /// <summary>
        /// default constructor
        /// </summary>
        public JsonMaze() { }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="myMaze">the maze</param>
        /// <param name="name">the name of the maze</param>
        public JsonMaze(Maze myMaze, string name)
        {
            this.myMaze = myMaze;
            this.name = name;
            this.maze = myMaze.ToString();
            this.start = myMaze.Start;
            this.end = myMaze.End;
        }

        /// <summary>
        /// setter and getter for Name member
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { name = value; }
        }
        /// <summary>
        /// setter and getter for Maze member
        /// </summary>
        public string Maze
        {
            get { return this.maze; }
            set { maze = value; }
        }
        /// <summary>
        /// setter and getter for Start member
        /// </summary>
        public Cell Start
        {
            get { return this.start; }
            set { start = value; }
        }
        /// <summary>
        /// setter and getter for End member
        /// </summary>
        public Cell End
        {
            get { return this.end; }
            set { end = value; }
        }
        /// <summary>
        /// return myMaze
        /// </summary>
        /// <returns>myMaze</returns>
        public Maze getMyMaze()
        {
            return this.myMaze;
        }
    }
}
