﻿using System;

namespace Server
{
    /// <summary>
    /// this command controls playing in multiplayer game. it communicates with the given model
    /// </summary>
    public class PlayCommand : ICommandable
    {
        /// <summary>
        /// up/down/right/left
        /// </summary>
        string movement;

        /// <summary>
        /// the name of the game
        /// </summary>
        string name;

        /// <summary>
        /// execute the command
        /// </summary>
        /// <param name="model">refernce to the model i work with</param>
        public void Execute(IModel model)
        {
            model.Play(name, this);
        }

        /// <summary>
        /// no need to implement in this command
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string GetData(IModel model)
        {
            //this function is not used by play
            return "";
        }

        /// <summary>
        /// set the argument the command need
        /// </summary>
        /// <param name="args">the movement of theplayer</param>
        public void SetArguments(string args)
        {
            movement = args;
        }

        /// <summary>
        /// setting new name for the game for this command
        /// </summary>
        /// <param name="gameName">new name for game</param>
        public void SetName(string gameName)
        {
            name = gameName;
        }

        /// <summary>
        /// returns the movement of this command
        /// </summary>
        /// <returns>the movement of the play</returns>
        public string getMovement()
        {
            return this.movement;
        }

    }
}