﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public delegate void func();

namespace Server
{
    /// <summary>
    /// the general interface for V of MVP method. the View communicates with the "outside world"
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// event that informer that the view changed
        /// </summary>
        event func ViewChanged;

        /// <summary>
        /// when the presenter get the massege that the view changed it call this function
        /// </summary>
        /// <param name="s">describe the data that the presenter want</param>
        void DisplayData(string s);

        /// <summary>
        /// 
        /// </summary>
        /// <returns>the request the client send to the server</returns>
        string getUserRequest();

    }
}
