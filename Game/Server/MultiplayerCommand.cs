﻿using System;
using System.Web.Script.Serialization;

namespace Server
{
    /// <summary>
    /// this command controls multiplaying game option. it communicates with the given model
    /// </summary>
    public class MultiplayerCommand : ICommandable
    {
        /// <summary>
        /// the name of the multigame
        /// </summary>
        private string gameName;
        /// <summary>
        /// the last move of the opponent
        /// </summary>
        private string opponentMove;
        /// <summary>
        /// the player of me
        /// </summary>
        private Player myPlayer;

        /// <summary>
        /// execute the command
        /// </summary>
        /// <param name="model">refernce to the model i work with</param>
        public void Execute(IModel model)
        {
            model.Multi(gameName, myPlayer);
        }

        /// <summary>
        /// calling the function getData of the model
        /// </summary>
        /// <param name="model">the model i work with</param>
        /// <returns>the data</returns>
        public string GetData(IModel model)
        {
            string data = "multi_" + myPlayer.IdNum +  "_" + gameName;
            return model.GetData(data);
        }

        /// <summary>
        /// set the argument the command need
        /// </summary>
        /// <param name="args">the name of the maze and the type</param>
        public void SetArguments(string args)
        {
            this.gameName = args;
        }
        /// <summary>
        /// property
        /// </summary>
        public string OpponentMove
        {
            get
            {
                return opponentMove;
            }

            set
            {
                opponentMove = value;
            }
        }

        /// <summary>
        /// set the player of this multiplayer command
        /// </summary>
        /// <param name="play">play command of player</param>
        /// /// <param name="close">close command of player</param>
        public void updatePlayer(PlayCommand play, CloseCommand close)
        {
            myPlayer = new Player(this, play, close);
        }

        /// <summary>
        /// reurn the opponent move in JSON pattern
        /// </summary>
        /// <returns>the last move of the opponent</returns>
        public string getOppMove()
        {
            JsonMove jm = new JsonMove(gameName, opponentMove);
            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Serialize(jm);
        }
    }
}