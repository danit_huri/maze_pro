﻿using System;

namespace Server
{
    /// <summary>
    /// this command controls solving object. it communicates with the given model
    /// </summary>
    public class SolveCommand : ICommandable
    {
        /// <summary>
        /// name of the maze for solving
        /// </summary>
        private string name;
        /// <summary>
        /// type of generation 0-Breadth first 1-Best first
        /// </summary>
        private int type;

        /// <summary>
        /// execute the command
        /// </summary>
        /// <param name="model">refernce to the model i work with</param>
        public void Execute(IModel model)
        {
            model.Solve(name, type, this);
        }

        /// <summary>
        /// calling the function getData of the model
        /// </summary>
        /// <param name="model">the model i work with</param>
        /// <returns>the data</returns>
        public string GetData(IModel model)
        {
            string data = "solve_" + name + type.ToString();
            return model.GetData(data);
        }

        /// <summary>
        /// set the argument the command need
        /// </summary>
        /// <param name="args">the name of the maze and the type</param>
        public void SetArguments(string args)
        {
            name = args.Substring(0, args.IndexOf(" "));
            type = int.Parse(args.Substring(args.IndexOf(" ") + 1));
        }
    }
}