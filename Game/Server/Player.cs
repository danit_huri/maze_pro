﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// A player for games. holds its multiplayer, play and close commands
    /// </summary>
    public class Player
    {
        /// <summary>
        /// his multiplayer command
        /// </summary>
        private MultiplayerCommand multiCom;
        /// <summary>
        /// his playing command
        /// </summary>
        private PlayCommand playCom;
        /// <summary>
        /// his closing command
        /// </summary>
        private CloseCommand closeCom;
        /// <summary>
        /// player 1/2
        /// </summary>
        private int idNum;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="multi">multiplayer command</param>
        /// <param name="play">player command</param>
        public Player (MultiplayerCommand multi, PlayCommand play, CloseCommand close)
        {
            this.multiCom = multi;
            this.playCom = play;
            this.closeCom = close;
            //Indication that the player is not in any game
            this.idNum = 0;
        }

        /// <summary>
        /// update this player with the movement of the opponent
        /// </summary>
        /// <param name="movement">opponents movement</param>
        public void OpponentPlayed(string movement)
        {
            this.MultiCom.OpponentMove = movement;
        }

        /// <summary>
        /// getter for MultiCom
        /// </summary>
        public MultiplayerCommand MultiCom
        {
            get
            {
                return multiCom;
            }
        }

        /// <summary>
        /// getter for PlayCom
        /// </summary>
        public PlayCommand PlayCom
        {
            get
            {
                return playCom;
            }
        }

        /// <summary>
        /// getter for CloseCom
        /// </summary>
        public CloseCommand CloseCom
        {
            get
            {
                return closeCom;
            }
        }

        /// <summary>
        /// getter and setter for IdNum
        /// </summary>
        public int IdNum
        {
            get
            {
                return idNum;
            }
            set
            {
                idNum = value;
            }
        }
        
    }
}
