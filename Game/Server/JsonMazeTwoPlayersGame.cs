﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game;

namespace Server
{
    /// <summary>
    /// class for representing a message in json format
    /// </summary>
    public class JsonMazeTwoPlayersGame
    {
        /// <summary>
        /// name of game
        /// </summary>
        private string name;
        /// <summary>
        /// name of maze
        /// </summary>
        private string mazeName;
        /// <summary>
        /// the player maze
        /// </summary>
        private JsonMaze you;
        /// <summary>
        /// the opponent's maze
        /// </summary>
        private JsonMaze other;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name">the name of the multigame</param>
        /// <param name="mazeName">the name of the maze</param>
        /// <param name="maze1">the maze of the first player</param>
        /// <param name="maze2">the maze of the second player</param>
        public JsonMazeTwoPlayersGame (string name, string mazeName, JsonMaze maze1, JsonMaze maze2)
        {
            this.name = name;
            this.mazeName = mazeName;
            this.you = maze1;
            this.other = maze2;
        }

        /// <summary>
        /// getter and setter for name
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// getter and setter for mazeName
        /// </summary>
        public string MazeName
        {
            get
            {
                return mazeName;
            }

            set
            {
                mazeName = value;
            }
        }

        /// <summary>
        /// getter and setter for you
        /// </summary>
        public JsonMaze You
        {
            get
            {
                return you;
            }

            set
            {
                you = value;
            }
        }

        /// <summary>
        /// getter and setter for other
        /// </summary>
        public JsonMaze Other
        {
            get
            {
                return other;
            }

            set
            {
                other = value;
            }
        }
    }
}
