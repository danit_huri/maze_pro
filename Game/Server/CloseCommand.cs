﻿using System;

namespace Server
{
    /// <summary>
    /// this command control closing multiplayer game option
    /// </summary>
    public class CloseCommand : ICommandable
    {
        /// <summary>
        /// name of the game to close
        /// </summary>
        private string name;

        /// <summary>
        /// execute the command
        /// </summary>
        /// <param name="model">refernce to the model i work with</param>
        public void Execute(IModel model)
        {
            model.Close(name, this);
        }

        /// <summary>
        /// no need to implement in this command
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string GetData(IModel model)
        {
            /*no need to implement*/
            return "";
        }

        /// <summary>
        /// set the argument the command need
        /// </summary>
        /// <param name="args">the name of the game for closing</param>
        public void SetArguments(string args)
        {
            this.name = args;
        }
    }
}