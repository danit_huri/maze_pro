﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// class for representing a move in json format
    /// </summary>
    public class JsonMove
    {
        /// <summary>
        /// name of the game
        /// </summary>
        private string name;
        /// <summary>
        /// the movement
        /// </summary>
        private string move;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="gameName">the name of the multigame</param>
        /// <param name="movement">up/down/right/left</param>
        public JsonMove(string gameName, string movement)
        {
            name = gameName;
            move = movement;
        }

        /// <summary>
        /// getter and setter for name
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        /// <summary>
        /// getter and setter for move
        /// </summary>
        public string Move
        {
            get
            {
                return move;
            }

            set
            {
                move = value;
            }
        }
    }
}
