﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Game;

namespace Server
{
    /// <summary>
    /// this format of IView. handles a client (socket)
    /// </summary>
    public class ClientHandler: IView
    {
        private Socket client;
        private string clientRequest;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="client">the socket of the client we want to handle</param>
        public ClientHandler(Socket client)
        { 

            this.client = client;
        }
        /// <summary>
        /// event that informer that the view changed
        /// </summary>
        public event func ViewChanged;

        /// <summary>
        /// when the presenter get the massege that the view changed it call this function
        /// </summary>
        /// <param name="s">describe the data that the presenter want</param>
        public void DisplayData(string s)
        {
            byte[] data = new byte[65536];
            data = Encoding.ASCII.GetBytes(s);
            client.Send(data, data.Length, SocketFlags.None);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>the request the client send to the server</returns>
        public string getUserRequest()
        {
            return this.clientRequest;
        }

        /// <summary>
        /// the connection with the user-send/recieve
        /// </summary>
        public void handle()
        {
            bool stop = false;
            while (!stop)
            {
                byte[] data = new byte[65536];
                try {
                    int recv = client.Receive(data);
                
                if (recv == 0)
                {
                    break;
                }
                //save the request in member
                clientRequest = Encoding.ASCII.GetString(data, 0, recv);
                ViewChanged();
                } catch (Exception e)
                {
                    //client closed connection
                    stop = true;
                }
            }
            client.Close();
        }

    }
}
