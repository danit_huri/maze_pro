﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game;

namespace Server
{
    /// <summary>
    /// TwoPlayersGame of type: maze
    /// </summary>
    public class MazeTwoPlayersGame : TwoPlayersGame<Maze>

    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="p">first player</param>
        /// <param name="gameName">the name of the multigame</param>
        public MazeTwoPlayersGame(Player p, string gameName) : base(p, gameName){}

        /// <summary>
        /// initialize the multigame, the 2 mazes and the status
        /// </summary>
        public override void initialize()
        {
            Generator<Cell, Maze> gen = new DepthFirstSearchAlgorithm<Cell, Maze>();
            IGenerable<Cell, Maze> iGen = new GenerableMaze();
            obj1 = gen.Generate(iGen);
            obj2 = new Maze(obj1);
            Obj1.randomNewStart();
            status = true;
        }

    }
}
