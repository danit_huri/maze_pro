﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// this is the interface for a general command for a user
    /// </summary>
    public interface ICommandable
    {

        /// <summary>
        /// set the argument the command need
        /// </summary>
        /// <param name="args">the appropriate arguments of the command</param>
        void SetArguments(string args);

        /// <summary>
        /// execute the command
        /// </summary>
        /// <param name="model">refernce to the model i work with</param>
        void Execute(IModel model);

        /// <summary>
        /// calling the function getData of the model
        /// </summary>
        /// <param name="model">the model i work with</param>
        /// <returns>the data</returns>
        string GetData(IModel model);
    }
}