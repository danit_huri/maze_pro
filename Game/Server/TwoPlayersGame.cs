﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// this is the abstract class of 2 players multiplayer game
    /// </summary>
    /// <typeparam name="T">the type of game object</typeparam>
    abstract public class TwoPlayersGame<T> : MultiplayerGame<T>
    {
        /// <summary>
        /// first player of the game
        /// </summary>
        protected Player player1;
        /// <summary>
        /// second player of the game
        /// </summary>
        protected Player player2;
        /// <summary>
        /// object for the first player
        /// </summary>
        protected T obj1;
        /// <summary>
        /// object for the second player
        /// </summary>
        protected T obj2;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="p">first player</param>
        /// <param name="gameName">the name of the game</param>
        public TwoPlayersGame(Player p, string gameName)
        {
            player1 = p;
            name = gameName;
            status = false;
        }

        /// <summary>
        /// checking wich player has gotten with the play command and return the opponent
        /// </summary>
        /// <param name="myExecuter">my play command</param>
        /// <returns>the opponent player</returns>
        public Player getOpponent(PlayCommand myExecuter)
        {
            if (player1.PlayCom.Equals(myExecuter))
            {
                return player2;
            } else if (player2.PlayCom.Equals(myExecuter))
            {
                return player1;
            }
            return null;
        }

        /// <summary>
        /// closing game zerox there id numbers and game's names
        /// </summary>
        public void CloseGame()
        {
            player1.IdNum = 0;
            player2.IdNum = 0;
            player1.PlayCom.SetName("");
            player2.PlayCom.SetName("");
        }

        /// <summary>
        /// checks if toCheck belongs to one of the players
        /// </summary>
        /// <param name="toCheck"></param>
        /// <returns>true = if belongs</returns>
        public bool isPlayer(ICommandable toCheck)
        {
            if (player1.MultiCom.Equals(toCheck) || player2.MultiCom.Equals(toCheck))
            {
                return true;
            }
            if (player1.PlayCom.Equals(toCheck) || player2.PlayCom.Equals(toCheck))
            {
                return true;
            }
            if (player1.CloseCom.Equals(toCheck) || player2.CloseCom.Equals(toCheck))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// getter and settter for player 2
        /// </summary>
        public Player Player2
        {
            get
            {
                return player2;
            }

            set
            {
                player2 = value;
            }
        }

        /// <summary>
        /// getter and settter for player 1
        /// </summary>
        public Player Player1
        {
            get
            {
                return player1;
            }

            set
            {
                player1 = value;
            }
        }

        /// <summary>
        /// getter and settter for Obj1
        /// </summary>
        public T Obj1
        {
            get
            {
                return obj1;
            }

            set
            {
                obj1 = value;
            }
        }

        /// <summary>
        /// getter and settter for Obj2
        /// </summary>
        public T Obj2
        {
            get
            {
                return obj2;
            }

            set
            {
                obj2 = value;
            }
        }


    }
}
