﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
using Game;
using System.Configuration;

namespace Server
{
    /// <summary>
    /// this class controls the TCP connection and its threadpool
    /// </summary>
    public class TCPServer
    {
        /// <summary>
        /// port number of the server- get from app confige
        /// </summary>
        int portNumber = int.Parse(ConfigurationManager.AppSettings["PORT"]);

        /// <summary>
        /// start- calling the function connectClient
        /// </summary>
        /// <param name="myModel">reference to the model</param>
        public void Start(IModel myModel)
        {
            connectClient(myModel);    
        }

        /// <summary>
        /// connecting to client and attendant him in new thread
        /// </summary>
        /// <param name="myModel">reference to the model</param>
        public void connectClient(IModel myModel)
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, portNumber);
            Socket newsock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            newsock.Bind(ipep);
            newsock.Listen(10);

            while (true)
            {
                Socket client = newsock.Accept();
                //creating a new presenter and view for any client
                ClientHandler handler = new ClientHandler(client);
                Presenter newPresenter = new Presenter();
                newPresenter.setModel(myModel);
                newPresenter.setView(handler);
                //tread pool
                Task.Factory.StartNew(handler.handle);
            }

        }

    }
}
