﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// class for representing a message in json format
    /// </summary>
    public class JsonMessage
    {
        /// <summary>
        /// the message to print
        /// </summary>
        private string message;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="newMsg"> the message of this object</param>
        public JsonMessage(string newMsg)
        {
            message = newMsg;
        }

        /// <summary>
        /// setter and getter for message member
        /// </summary>
        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }
    }
}
