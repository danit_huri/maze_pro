﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using Game;
using System.Web.Script.Serialization;


namespace Server
{
    /// <summary>
    /// IModel of type Maze
    /// </summary>
    public class MazeModel : IModel
    {
        /// <summary>
        /// constant values
        /// </summary>
        private const int RANDOM = 0;
        private const int DFS = 1;
        private const int BFS = 0;
        private const int BESTFIRST = 1;
        /// <summary>
        /// event of Model
        /// </summary>
        public event comFunc ModelChanged;
        public event playFunc playerPlayed;
        public event messageFunc errorOccurred;

        /// <summary>
        /// dictionaries for finding mazes and games
        /// </summary>
        Dictionary<string, JsonMaze> generatedMaze;
        Dictionary<string, JsonMaze> solvedMaze;
        Dictionary<string, MazeTwoPlayersGame> games;

        /// <summary>
        /// constuctor
        /// </summary>
        public MazeModel()
        {

            //insert to generated maze the items from the file
            JavaScriptSerializer s = new JavaScriptSerializer();
            string  input = "";
            
            if (File.Exists("solved.json"))
            {
                input = File.ReadAllText("solved.json");
                this.solvedMaze = s.Deserialize<Dictionary<string, JsonMaze>>(input);
            }
            else
            {
                solvedMaze = new Dictionary<string, JsonMaze>();
            }
            generatedMaze = new Dictionary<string, JsonMaze>();
            games = new Dictionary<string, MazeTwoPlayersGame>();
        }

        /// <summary>
        /// return the data according to the command
        /// </summary>
        /// <param name="str">the command</param>
        /// <returns>the data</returns>
        public string GetData(string str)
        {
            JsonMaze returnedMaze = null;
            string output = "";
            string commandType = str.Substring(0, str.IndexOf("_"));
            string argument = str.Substring(str.IndexOf("_") + 1);
            JavaScriptSerializer s = new JavaScriptSerializer();
            if (commandType.Equals("generate"))
            {
                if (!this.generatedMaze.TryGetValue(argument, out returnedMaze))
                {
                    output = s.Serialize(new JsonMessage("Did not found the maze: " + argument));
                }
                else
                {
                    output = s.Serialize(returnedMaze);
                }
            }
            else if (commandType.Equals("solve"))
            {
                if (!this.solvedMaze.TryGetValue(argument, out returnedMaze))
                {
                    output = s.Serialize(new JsonMessage("Did not found the solved maze: " + argument));
                }
                else
                {
                    output = s.Serialize(returnedMaze);
                }
            }
            else if (commandType.Equals("multi"))
            {
                JsonMazeTwoPlayersGame returnedJsonGame = null;
                MazeTwoPlayersGame returnedGame = null;
                string playerNum = argument.Substring(0, argument.IndexOf("_"));
                string name = argument.Substring(argument.IndexOf("_") + 1);

                if (!this.games.TryGetValue(name, out returnedGame))
                {
                    output = s.Serialize(new JsonMessage("Did not found the game: " + name));
                }
                else
                {
                    JsonMaze jMazeSelf;
                    JsonMaze jMazeOther;
                    if (playerNum == "1")
                    {
                        jMazeSelf = new JsonMaze(returnedGame.Obj1, name + "_1");
                        jMazeOther = new JsonMaze(returnedGame.Obj2, name + "_2");
                    }
                    else
                    {
                        jMazeSelf = new JsonMaze(returnedGame.Obj2, name + "_2");
                        jMazeOther = new JsonMaze(returnedGame.Obj1, name + "_1");
                    }

                    returnedJsonGame = new JsonMazeTwoPlayersGame(name, name + "_" + playerNum, jMazeSelf, jMazeOther);
                    output = s.Serialize(returnedJsonGame);
                }

            }

            return output;
        }

        /// <summary>
        /// generate a new maze
        /// </summary>
        /// <param name="name">the name of the maze</param>
        /// <param name="type">the type of the maze</param>
        /// <param name="myExecuter">generate commandable</param>
        public void Generate(string name, int type, ICommandable myExecuter)
        {
            //check if maze exist in dicionary
            if (this.generatedMaze.ContainsKey(name))
            {
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("There is already a maze with the name -" + name + "- in the database")));
                return;
            }
            //check if maze already solved in previous server use
            string solveNameBest = name + "1";
            string solveNameBreadth = name + "0";
            if (this.solvedMaze.ContainsKey(solveNameBest) && this.solvedMaze.ContainsKey(solveNameBreadth))
            {
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("There is already solved mazes (BreadthFS and BestFS) with the name -" + name + "- in the database")));
                return;
            } else if (this.solvedMaze.ContainsKey(solveNameBest))
            {
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("There is already a solved maze (BestFS) with the name -" + name + "- in the database")));
                return;
            } else if (this.solvedMaze.ContainsKey(solveNameBreadth))
            {
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("There is already a solved maze (BreadthFS) with the name -" + name + "- in the database")));
                return;
            }
            //search for games with this name. for cases that game created but not initialized yet.
            if (name.EndsWith("_1") || name.EndsWith("_2"))
            {
                string possibleGameName = name.Substring(0, name.Length - 2);
                if (this.games.ContainsKey(possibleGameName))
                {
                    JavaScriptSerializer errS = new JavaScriptSerializer();
                    errorOccurred(myExecuter, errS.Serialize(new JsonMessage("The name -" + name + "- already exists in the database")));
                    return;
                }
            }
            //maze doesnt exist in dictionary of solved/generated mazes
            if (type == RANDOM)
            {
                Generator<Cell, Maze> gen = new RandomMatrixAlgorithm<Cell, Maze>();
                IGenerable<Cell, Maze> iGen = new GenerableMaze();
                Maze m = gen.Generate(iGen);
                JsonMaze jm = new JsonMaze(m, name);
                this.generatedMaze.Add(name, jm);
            } else if (type == DFS)
            {
                Generator<Cell, Maze> gen = new DepthFirstSearchAlgorithm<Cell, Maze>();
                IGenerable<Cell, Maze> iGen = new GenerableMaze();
                Maze m = gen.Generate(iGen);
                JsonMaze jm = new JsonMaze(m, name);
                this.generatedMaze.Add(name, jm);
            } else
            {
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("Wrong type")));
                return;
            }
            ModelChanged(myExecuter);
        }

        /// <summary>
        /// solve exists maze
        /// </summary>
        /// <param name="name">the name of the maze</param>
        /// <param name="type">the type of solution</param>
        /// <param name="myExecuter">solve commandable</param>
        public void Solve(string name, int type, ICommandable myExecuter)
        {
            JsonMaze jm;
            //name of solved maze = name+type
            string solvedName = name + type.ToString();
            //if there is already a solved version of this maze, call event and return
            if (this.solvedMaze.TryGetValue(solvedName, out jm))
            {
                ModelChanged(myExecuter);
                return;
            }
            //else, if not solved yet - check if the maze is generated
            if (!this.generatedMaze.TryGetValue(name, out jm))
            {
                //if not generated: error event
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("Did not found maze: " + name)));
                return;
            }
            //extract maze from the json maze
            Maze mazeToSolve = jm.getMyMaze();
            //solve according to the type
            if (type == BFS)
            {
                IComparer<State<Cell>> mycomparer = new StateComparer<Cell>();
                Searcher<Cell, Maze> searcher = new BreadthFirstSearchAlgorithm<Cell, Maze>(mycomparer);
                ISearchable<Cell, Maze> iSer = new SearchableMaze(mazeToSolve);
                ISolution<Cell, Maze> sol = searcher.search(iSer);
                Maze m = sol.getObject();
                JsonMaze jm2 = new JsonMaze(m, name);
                this.solvedMaze.Add(solvedName, jm2);
            } else if (type == BESTFIRST)
            {
                IComparer<State<Cell>> mycomarer = new StateComparer<Cell>();
                Searcher<Cell, Maze> searcher = new BestFirstSearchAlgorithm<Cell, Maze>(mycomarer);
                ISearchable<Cell, Maze> iSer = new SearchableMaze(mazeToSolve);
                ISolution<Cell, Maze> sol = searcher.search(iSer);
                Maze m = sol.getObject();
                JsonMaze jm1 = new JsonMaze(m, name);
                this.solvedMaze.Add(solvedName, jm1);
            } else
            {
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("Wrong type")));
                return;
            }
            //save to file the generatedMaze dictionary
            JavaScriptSerializer s = new JavaScriptSerializer();
            string str = s.Serialize(this.solvedMaze);
            File.WriteAllText("solved.json", str);
            ModelChanged(myExecuter);
        }

        /// <summary>
        /// create a new multigame or adding player to exists multigame
        /// </summary>
        /// <param name="name">the name of the multigame</param>
        /// <param name="myExecuter">the player</param>
        public void Multi(string name, Player myExecuter)
        {
            string name2 = name + "_2";
            string name1 = name + "_1";
            if (games.ContainsKey(name))
            {
                //game exist in the dictionary
                MazeTwoPlayersGame currentGame = games[name];
                if (currentGame.isFull())
                {
                    //problem- there is a game with this name and it full.
                    JavaScriptSerializer errS = new JavaScriptSerializer();
                    string msg = "Cannot create game " + name + ". 2 players are already in the game";
                    errorOccurred(myExecuter.MultiCom, errS.Serialize(new JsonMessage(msg)));
                    return;
                }
                else
                {
                    myExecuter.PlayCom.SetName(name);
                    myExecuter.IdNum = 2;
                    currentGame.Player2 = myExecuter;
                    currentGame.initialize();
                    this.generatedMaze.Add(name1, new JsonMaze(currentGame.Obj1, name1));
                    this.generatedMaze.Add(name2, new JsonMaze(currentGame.Obj2, name2));
                    ModelChanged(currentGame.Player1.MultiCom);
                    ModelChanged(currentGame.Player2.MultiCom);
                }
            }
            else
            {
                //verify that there are no generated/solved mazes with this name
                string solvedName10 = name1 + "0";
                string solvedName20 = name2 + "0";
                string solvedName11 = name1 + "1";
                string solvedName21 = name2 + "1";
                if (generatedMaze.ContainsKey(name1) || generatedMaze.ContainsKey(name2) ||
                    solvedMaze.ContainsKey(solvedName10) || solvedMaze.ContainsKey(solvedName20) ||
                    solvedMaze.ContainsKey(solvedName11) || solvedMaze.ContainsKey(solvedName21))
                {
                    JavaScriptSerializer errS = new JavaScriptSerializer();
                    string msg = "Cannot create game " + name + ". Name already exists in database";
                    errorOccurred(myExecuter.MultiCom, errS.Serialize(new JsonMessage(msg)));
                    return;
                } 
                //creates new game
                myExecuter.IdNum = 1;
                myExecuter.PlayCom.SetName(name);
                MazeTwoPlayersGame newGame = new MazeTwoPlayersGame(myExecuter, name);
                this.games.Add(name, newGame);
            }
        }

        /// <summary>
        /// send a massage to the second player in the game that the first made a move
        /// </summary>
        /// <param name="name">the name of the miltigame</param>
        /// <param name="myExecuter">the play commandable</param>
        public void Play(string name, PlayCommand myExecuter)
        {
            if (games.ContainsKey(name))
            {
                //game exist in the dictionary
                MazeTwoPlayersGame currentGame = games[name];
                Player opp = currentGame.getOpponent(myExecuter);
                if (opp != null)
                {
                    opp.MultiCom.OpponentMove = myExecuter.getMovement();
                    playerPlayed(opp.MultiCom);
                }
            }
            else if (name.Equals(""))
            {
                //there is no game set for this player
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("Please start/connect to a game first")));
                return;
            }
            else
            {
                //did not found the game in the dictionary
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("Did not found game: " + name)));
                return;
            }
        }

        /// <summary>
        /// close a multigame
        /// </summary>
        /// <param name="name">the name of the multigame for closing</param>
        public void Close(string name, ICommandable myExecuter)
        {

            if (games.ContainsKey(name))
            {
                //game exist in the dictionary
                MazeTwoPlayersGame currentGame = games[name];
                if (currentGame.isPlayer(myExecuter))
                {
                    currentGame.CloseGame();
                    games.Remove(name);
                } else
                {
                    JavaScriptSerializer errS = new JavaScriptSerializer();
                    errorOccurred(myExecuter, errS.Serialize(new JsonMessage("You cannot close a game that you are not assigned to")));
                    return;
                }
                
            }
            else
            {
                //did not found the game in the dictionary
                JavaScriptSerializer errS = new JavaScriptSerializer();
                errorOccurred(myExecuter, errS.Serialize(new JsonMessage("Did not found game: " + name)));
                return;
            }
        }
    }
}
