﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game;
using System.Web.Script.Serialization;

public delegate void comFunc(Server.ICommandable com);
public delegate void playFunc(Server.MultiplayerCommand com);
public delegate void messageFunc(Server.ICommandable com, string msg);

namespace Server
{
    /// <summary>
    /// the is the P in the MVP method. it communicates with the given IModel and IView
    /// </summary>
    public class Presenter
    {
        /// <summary>
        /// the IView for this Presenter
        /// </summary>
        private IView view;
        /// <summary>
        /// the IModel for this Presenter
        /// </summary>
        private IModel model;
        /// <summary>
        /// dictionary of the 5 commands
        /// </summary>
        private Dictionary<string, ICommandable> myCommandsDict;

        /// <summary>
        /// constructor tat create the commandables and adding them to the dctionary
        /// </summary>
        public Presenter()
        {
            PlayCommand myPlayCom = new PlayCommand();
            myPlayCom.SetName("");
            CloseCommand myCloseCom = new CloseCommand();
            MultiplayerCommand myMultCom = new MultiplayerCommand();
            myMultCom.updatePlayer(myPlayCom, myCloseCom);
            myCommandsDict = new Dictionary<string, ICommandable>();
            myCommandsDict.Add("generate", new GenerateCommand());
            myCommandsDict.Add("solve", new SolveCommand());
            myCommandsDict.Add("multiplayer", myMultCom);
            myCommandsDict.Add("play", myPlayCom);
            myCommandsDict.Add("close", myCloseCom);
        }

        /// <summary>
        /// update the view we holds
        /// </summary>
        /// <param name="v">the new view</param>
        public void setView(IView v)
        {
            view = v;
            //adding function of the delegate to viewChanged event
            view.ViewChanged += delegate ()
            {
                string req = view.getUserRequest();

                if (!req.Equals(""))
                {
                    if (req.Contains(" "))
                    {
                        ICommandable myCom;
                        string reqStr = req.Substring(0, req.IndexOf(" "));
                        //tring to get the appropriate command 
                        if (myCommandsDict.TryGetValue(reqStr, out myCom))
                        {
                            string args = req.Substring(req.IndexOf(" ") + 1);
                            myCom.SetArguments(args);
                            myCom.Execute(model);
                        }
                        else
                        {
                            JavaScriptSerializer errS = new JavaScriptSerializer();
                            view.DisplayData(errS.Serialize(new JsonMessage("This request does not exist")));
                        }
                    } else
                    {
                        JavaScriptSerializer errS = new JavaScriptSerializer();
                        view.DisplayData(errS.Serialize(new JsonMessage("This request does not exist")));
                    }
                } else
                {
                    JavaScriptSerializer errS = new JavaScriptSerializer();
                    view.DisplayData(errS.Serialize(new JsonMessage("Empty request")));
                }

            };
        }

        /// <summary>
        /// update the model we holds
        /// </summary>
        /// <param name="v">the new model</param>
        public void setModel(IModel m)
        {
            model = m;
            //adding function of the delegate to viewChanged event
            model.ModelChanged += delegate (ICommandable com)
            {

                if (isMyCommandable(com))
                {
                    string data = com.GetData(model);
                    view.DisplayData(data);
                }
                
            };
            //adding function of the delegate to playerPlayed event
            model.playerPlayed += delegate (MultiplayerCommand multiCom)
            {
                if (isMyCommandable(multiCom))
                {
                    string data = multiCom.getOppMove();
                    view.DisplayData(data);
                }
            };
            //adding function of the delegate to errorOccurred event
            model.errorOccurred += delegate (ICommandable com, string msg)
            {
                if (isMyCommandable(com))
                {
                    view.DisplayData(msg);
                }
            };
        }

        /// <summary>
        /// indication if this is my commandble object
        /// </summary>
        /// <param name="com">any commandable</param>
        /// <returns>true/false</returns>
        private bool isMyCommandable(ICommandable com)
        {
            //check if it appears in my dictionary
            return myCommandsDict.ContainsValue(com);
        }
    }
}
