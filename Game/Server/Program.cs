﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// this is the main class of the Server project
    /// </summary>
    public class Program
    {
        /// <summary>
        /// this is the main function of the Server project. 
        /// it sets the model of this project and start a TCPconnection
        /// </summary>
        /// <param name="args">command line arguments</param>
        public static void Main(string[] args)
        {
            IModel myModel = new MazeModel();
            TCPServer tcpConnection = new TCPServer();
            tcpConnection.Start(myModel);
        }
    }
}
