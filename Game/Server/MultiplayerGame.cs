﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// abstract class for multiplayer games
    /// </summary>
    /// <typeparam name="T">type of object of the game</typeparam>
    public abstract class MultiplayerGame<T>
    {
        /// <summary>
        /// the name of the multiplayer game
        /// </summary>
        protected string name;

        /// <summary>
        /// the status of the game
        /// </summary>
        protected bool status;

        /// <summary>
        /// initialize the game for start
        /// </summary>
        abstract public void initialize();
        
        /// <summary>
        /// indication if there is a place for other players
        /// </summary>
        /// <returns>true-the game is full false-there is another place</returns>
        public bool isFull()
        {
            return status;
        }

    }
}
