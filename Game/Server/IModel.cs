﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// the general interface for M of MVP method. the Model communicates with all the data
    /// </summary>
    public interface IModel
    {
        /// <summary>
        /// event that informes that the data in the model have changed
        /// </summary>
        event comFunc ModelChanged;

        /// <summary>
        /// event that informes that one player played
        /// </summary>
        event playFunc playerPlayed;

        /// <summary>
        /// event that informes that something went wrong
        /// </summary>
        event messageFunc errorOccurred;

        /// <summary>
        /// return the data according to the command
        /// </summary>
        /// <param name="str">the command</param>
        /// <returns>the data</returns>
        string GetData(string str);

        /// <summary>
        /// generate a new maze
        /// </summary>
        /// <param name="name">the name of the maze</param>
        /// <param name="type">the type of the maze</param>
        /// <param name="myExecuter">generate commandable</param>
        void Generate(string name, int type, ICommandable myExecuter);

        /// <summary>
        /// solve exists maze
        /// </summary>
        /// <param name="name">the name of the maze</param>
        /// <param name="type">the type of solution</param>
        /// <param name="myExecuter">solve commandable</param>
        void Solve(string name, int type, ICommandable myExecuter);

        /// <summary>
        /// create a new multigame or adding player to exists multigame
        /// </summary>
        /// <param name="name">the name of the multigame</param>
        /// <param name="myExecuter">the player</param>
        void Multi(string name, Player myExecuter);

        /// <summary>
        /// send a massage to the second player in the game that the first made a move
        /// </summary>
        /// <param name="name">the name of the miltigame</param>
        /// <param name="myExecuter">the play commandable</param>
        void Play(string name, PlayCommand myExecuter);

        /// <summary>
        /// close a multigame
        /// </summary>
        /// <param name="name">the name of the multigame for closing</param>
        /// <param name="myExecuter">the close commandable</param>
        void Close(string name, ICommandable myExecuter);
    }
}
