﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// general interface for solution for search algorithms
    /// </summary>
    /// <typeparam name="T">object of state</typeparam>
    /// <typeparam name="K">object of solution</typeparam>
    public interface ISolution<T, K>
    {
        /// <summary>
        /// applying the solution according to the resolt of states from an outside algorithm
        /// </summary>
        /// <param name="sol">the list of states to apply</param>
        void applySolution(List<State<T>> sol);
        
        /// <summary>
        /// return the object with the solution in it
        /// </summary>
        /// <returns>the object ith solution</returns>
        K getObject();
    }
}
