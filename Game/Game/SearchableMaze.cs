﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// ISearchable for Maze
    /// </summary>
    public class SearchableMaze: ISearchable<Cell, Maze>
    {
        /// <summary>
        /// the maze to search on
        /// </summary>
        private Maze maze;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        public SearchableMaze(Maze m)
        {
            this.maze = m;
        }

        /// <summary>
        /// returns the initial state of the searching
        /// </summary>
        /// <returns>the start state</returns>
        public State<Cell> getInitialState() {
            State<Cell> initialCell = new State<Cell>(maze.Start);
            initialCell.Cost = 0;
            initialCell.CameFrom = null;
            return initialCell;
        }

        /// <summary>
        /// returns the goal state for the search
        /// </summary>
        /// <returns>the goal state</returns>
        public State<Cell> getGoalState() {
            State<Cell> goalCell = new State<Cell>(maze.End);
            return goalCell;
        }
        /// <summary>
        /// getting the successors of a state
        /// </summary>
        /// <param name="s">the state to use</param>
        /// <returns>the successors</returns>
        public List<State<Cell>> getAllPossibleStates(State<Cell> s) {
            Cell cell = s.getState();
            List<State<Cell>> posibleStates = new List<State<Cell>>();
            if (cell.canPassUp())
            {
                State<Cell> posibleState = new State<Cell>(maze.getCell(cell.Row - 1, cell.Col));
                posibleStates.Add(posibleState);
            }
            if (cell.canPassDown())
            {
                State<Cell> posibleState = new State<Cell>(maze.getCell(cell.Row + 1, cell.Col));
                posibleStates.Add(posibleState);
            }

            if (cell.canPassRight())
            {
                State<Cell> posibleState = new State<Cell>(maze.getCell(cell.Row, cell.Col + 1));
                posibleStates.Add(posibleState);
            }

            if (cell.canPassLeft())
            {
                State<Cell> posibleState = new State<Cell>(maze.getCell(cell.Row, cell.Col - 1));
                posibleStates.Add(posibleState);
            }

            return posibleStates;
        }

        /// <summary>
        /// return new ISolution for this cell,maze objects
        /// </summary>
        /// <returns>the new solution</returns>
        public ISolution<Cell, Maze> getInitialSolution()
        {
            return new MazeSolution(maze);
        }

        /// <summary>
        /// return this maze
        /// </summary>
        /// <returns>the maze</returns>
        public Maze getObject()
        {
            return maze;
        }
    }
}
