﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Game
{
    /// <summary>
    /// abstract class for search algorithms
    /// </summary>
    /// <typeparam name="T">the object of the state</typeparam>
    /// <typeparam name="K">the object we search on</typeparam>
    public abstract class Searcher<T,K>: IAlgorithm
    {
        /// <summary>
        /// the priority queue for the algorithm
        /// </summary>
        private PriorityQueue<State<T>, IComparer<State<T>>> openList;
        private int evaluatedNodes;

        /// <summary>
        /// the search algorithm
        /// </summary>
        /// <param name="searchable">the ISearchable to search with</param>
        /// <returns>the solution from the search</returns>
        public abstract ISolution<T,K> search(ISearchable<T,K> searchable);

        /// <summary>
        /// constructor
        /// </summary>
        public Searcher()
        {
            openList = new PriorityQueue<State<T>, IComparer<State<T>>>();
            evaluatedNodes = 0;
        }

        /// <summary>
        /// sets the IComparer of the priority queue
        /// </summary>
        /// <param name="comp">the IComparer</param>
        public void setComparer(IComparer<State<T>> comp){
            openList.setComparer(comp);
        }

        /// <summary>
        /// pops the head of the priority queue
        /// </summary>
        /// <returns></returns>
        protected State<T> popOpenList()
        {
            evaluatedNodes++;
            return openList.Dequeue();
        }

        /// <summary>
        /// adding state to the priority queue
        /// </summary>
        /// <param name="state">the state to add</param>
        protected void addOpenList(State<T> state)
        {
            openList.Enqueue(state);
        }

        /// <summary>
        /// check if the priority queue contains a state
        /// </summary>
        /// <param name="state">the state to check</param>
        /// <returns>true if contains</returns>
        protected bool openContains(State<T> state)
        {
            return openList.isContain(state);
        }

        /// <summary>
        /// return the size of the priority queue
        /// </summary>
        /// <returns>size of priority queue</returns>
        protected int openListSize()
        {
            return openList.Size();
        }

        /// <summary>
        /// return the state from the priority queue that equals to the given state
        /// </summary>
        /// <param name="state">the state to search</param>
        /// <returns>the equal state from priority queue</returns>
        protected State<T> getFromOpen(State<T> state)
        {
            return openList.FindItem(state);
        }

        /// <summary>
        /// sets the IComparer of the priority queue for inherit classes
        /// </summary>
        /// <param name="comp">the IComparer</param>
        protected void setOpenComparer(IComparer<State<T>> myComparer)
        {
            openList.setComparer(myComparer);
        }

    }
}
