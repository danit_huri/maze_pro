﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// Maze object
    /// </summary>
    public class Maze
    {
        //n- rows m- coulms
        private int n, m;
        /// <summary>
        /// array of cells, for the maze grid
        /// </summary>
        private Cell[,] arr;
        /// <summary>
        /// the start cell of the maze
        /// </summary>
        private Cell start;
        /// <summary>
        /// the goal cell of the maze
        /// </summary>
        private Cell end;
        /// <summary>
        /// the maze as an int matrix
        /// </summary>
        private int[,] matrixMaze;

        /// <summary>
        /// constructor for new maze
        /// </summary>
        /// <param name="n">size of rows</param>
        /// <param name="m">size of columns</param>
        public Maze(int n, int m)
        {
            this.n = n;
            this.m = m;
            this.arr = new Cell[n,m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    this.arr[i, j] = new Cell(i, j);
            this.ZeroxMaze();
            generateMatrixMaze();
        }

        /// <summary>
        /// constructor for deep copy
        /// </summary>
        /// <param name="maze">the maze to copy</param>
        public Maze(Maze maze)
        {
            this.m = maze.M;
            this.n = maze.N;
            this.arr = new Cell[n, m];
            for (int i = 0; i < m; i++)
                for (int j = 0; j < m; j++)
                {
                    this.arr[i, j] = new Cell(maze.getCell(i,j));
                }
            this.start = this.arr[maze.Start.Row, maze.Start.Col];
            this.end = this.arr[maze.End.Row, maze.End.Col];
            generateMatrixMaze();
        }

        /// <summary>
        /// getter for n member
        /// </summary>
        public int N
        {
            get { return this.n; }
        }
        /// <summary>
        /// getter for m member
        /// </summary>
        public int M
        {
            get { return this.m; }
        }
        /// <summary>
        /// getter and setter for start member
        /// </summary>
        public Cell Start
        {
            get
            {
                return this.start;
            }
            set { start = value; }
        }
        /// <summary>
        /// getter and setter for end member
        /// </summary>
        public Cell End
        {
            get { return this.end; }
            set { end = value; }
        }
        /// <summary>
        /// getter for Arr member
        /// </summary>
        public Cell[,] Arr
        {
            get { return this.arr; }
            
        }

        /// <summary>
        /// returns a specific cell from the maze
        /// the cell in the location (row, col)
        /// </summary>
        /// <param name="row">row number</param>
        /// <param name="col">column number</param>
        /// <returns>the specific cell</returns>
        public Cell getCell(int row, int col)
        {
            return this.arr[row, col];
        }

        /// <summary>
        /// initializing the Maze with zeros and setting the from
        /// </summary>
        private void ZeroxMaze()
        {
           
            //set up & down frame to 2
            for (int i = 0; i < m; i++)
            {
                this.arr[0, i].SetUpFrame();
                this.arr[n - 1, i].SetDownFrame();
            }

            //set right & left frame to 2
            for (int i = 0; i < n; i++)
            {
                this.arr[i, 0].SetLeftFrame();
                this.arr[i, m-1].SetRightFrame();
            }

        }

        /// <summary>
        /// extracting the maze's grid information into string format
        /// </summary>
        /// <returns>the maze as a string</returns>
        public override string ToString()
        {
            int n2 = 2 * n - 1, m2 = 2 * m - 1;
            string st = "";
            for (int i = 0; i < n2; i++)
            {
                for (int j = 0; j < m2; j++)
                    st += matrixMaze[i, j];
                
            }

            return st;
        }

       /// <summary>
       /// generates the int matrix from the cell matrix
       /// </summary>
        public void generateMatrixMaze()
        {
            int n2 = 2 * n - 1, m2 = 2 * m - 1;
            matrixMaze = new int[n2, m2];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    //matrixMaze[i * 2, j * 2] = 0;
                    setMatrixCell(i * 2, j * 2, 0);
                    if (arr[i, j].canPassRight())
                    {
                        //matrixMaze[i * 2, j * 2 + 1] = 0;
                        setMatrixCell(i * 2, j * 2 + 1, 0);
                    }
                    else
                    {
                        if (arr[i, j].isHasWallRight())
                        {
                            //matrixMaze[i * 2, j * 2 + 1] = 1;
                            setMatrixCell(i * 2, j * 2 + 1, 1);
                        }
                    }
                    if (arr[i, j].canPassDown())
                    {
                        //matrixMaze[i * 2 + 1, j * 2] = 0;
                        setMatrixCell(i * 2 + 1, j * 2, 0);
                    }
                    else
                    {
                        if (arr[i, j].isHasWallDown())
                        {
                            //matrixMaze[i * 2 + 1, j * 2] = 1;
                            setMatrixCell(i * 2 + 1, j * 2, 1);
                        }
                    }
                    if ((arr[i, j].canPassRight() || arr[i, j].isHasWallRight()) && (arr[i, j].canPassDown() || arr[i, j].isHasWallDown()))
                    {
                        //matrixMaze[2 * i + 1, 2 * j + 1] = 1;
                        setMatrixCell(2 * i + 1, 2 * j + 1, 1);
                    }
                }
            }
        }

        /// <summary>
        /// sets a new "random" start for the maze
        /// </summary>
        public void randomNewStart()
        {
            int currentDistance = start.distanceFrom(end);
            int newRow, newCol;
            Cell newStart;
            Random rand = new Random();
            //sets a new start that is far away enough from the goal cell
            do
            {
                newRow = rand.Next(n);
                newCol = rand.Next(m);
                newStart = getCell(newRow, newCol);
            }
            while (newStart.distanceFrom(end) + 1 < currentDistance || (newRow == start.Row && newCol == start.Col));
            this.start = newStart;
        }

        /// <summary>
        /// set value in a cell int the int matrix 
        /// </summary>
        /// <param name="i">row position</param>
        /// <param name="j">column position</param>
        /// <param name="value">to set</param>
        public void setMatrixCell(int i, int j, int value)
        {
            matrixMaze[i ,j] = value;
        }

        /// <summary>
        /// return a string of the maze in a more visible format.
        /// used basically for testings
        /// </summary>
        /// <returns>the maze as string with lines</returns>
        public string ToLineString()
        {
            int n2 = 2 * n - 1, m2 = 2 * m - 1;
            string st = "";
            for (int i = 0; i < n2; i++)
            {
                for (int j = 0; j < m2; j++)
                    st += matrixMaze[i, j];
                st += "\n";
            }

            return st;
        }
    }
}
