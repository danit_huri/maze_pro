﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// implemented priority queue for object T with comparer K
    /// </summary>
    /// <typeparam name="T">element type of the queue</typeparam>
    /// <typeparam name="K">the comparer of T elements</typeparam>
    public class PriorityQueue<T, K> where K : IComparer<T>
    {   
        /// <summary>
        /// list of elements of the queue
        /// </summary>
        private List<T> queueList;
        private K queueComparer;
       
        /// <summary>
        /// constructor of the PriorityQueue. initializing the list of the queue
        /// </summary>
        public PriorityQueue() {
            this.queueList = new List<T>();
            
        }

        /// <summary>
        /// setting the comparer
        /// </summary>
        /// <param name="comp">  </param>
        public void setComparer(K comp)
        {
            this.queueComparer = comp;
        }

        /// <summary>
        /// adding new item to the queue according to its priority
        /// </summary>
        /// <param name="item"> the new item to add </param>
        public void Enqueue(T item)
        {
            queueList.Add(item);
            int ci = queueList.Count - 1; // child index; start at end
            while (ci > 0)
            {
                int pi = (ci - 1) / 2; // parent index
                if (queueComparer.Compare(queueList[ci], (queueList[pi])) >= 0) {
                    break; // child item is larger than (or equal) parent so we're done
                }
                T temp = queueList[ci];
                queueList[ci] = queueList[pi];
                queueList[pi] = temp;
                ci = pi;
            }
        }

        /// <summary>
        /// returns the item at the head of the queue, and eliminates it from the queue
        /// </summary>
        /// <returns> the item at the head of the queue </returns>
        public T Dequeue() {
            // assumes pq is not empty; up to calling code
            int li = queueList.Count - 1; // last index (before removal)
            T frontItem = queueList[0];   // fetch the front
            queueList[0] = queueList[li];
            queueList.RemoveAt(li);

            --li; // last index (after removal)
            int pi = 0; // parent index. start at front of pq
            while (true) {
                int ci = pi * 2 + 1; // left child index of parent
                if (ci > li) break;  // no children so done
                int rc = ci + 1;     // right child
                if (rc <= li && queueComparer.Compare(queueList[rc], queueList[ci]) < 0) // if there is a rc (ci + 1), and it is smaller than left child, use the rc instead
                {
                    ci = rc;
                }
                if (queueComparer.Compare(queueList[pi], queueList[ci]) <= 0)
                {
                    break; // parent is smaller than (or equal to) smallest child so done
                }
                T tmp = queueList[pi];
                queueList[pi] = queueList[ci];
                queueList[ci] = tmp; // swap parent and child
                pi = ci;
            }
            return frontItem;
        }

        /// <summary>
        /// returns the item at the head of the queue without eliminating it from the queue
        /// </summary>
        /// <returns> the item at the head of the queue </returns>

        public T Peek() {
            T frontItem = queueList[0];
            return frontItem;
        }

        /// <summary>
        /// returns the size of this queue
        /// </summary>
        /// <returns> the size of this queue </returns>
        public int Size()
        {
            return queueList.Count;
        }

        public bool isContain(T item)
        {
            if (queueList.Contains(item)) {
                return true;
            }
            return false;
        }

        /// <summary>
        /// return element T that equals to the given T item
        /// </summary>
        /// <param name="item">T to search</param>
        /// <returns>the equal element if exist</returns>
       public T FindItem(T item)
        {
            if (isContain(item))
            {
                int index = queueList.IndexOf(item);
                return queueList.ElementAt(index);
            } else
            {
                return default(T);
            }
        }

        
    } 

}

