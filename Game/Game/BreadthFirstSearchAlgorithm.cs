﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// search algorithm of type Breadth First
    /// </summary>
    /// <typeparam name="T">the object of the state</typeparam>
    /// <typeparam name="K">the object we search on</typeparam>
    public class BreadthFirstSearchAlgorithm<T, K> : Searcher<T, K>
    {
        /// <summary>
        /// the closed list of states (already evaluated)
        /// </summary>
        List<State<T>> closed;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="myComparer">comparer for the open list</param>
        public BreadthFirstSearchAlgorithm(IComparer<State<T>> myComparer)
        {
            setOpenComparer(myComparer);
            closed = new List<State<T>>();
        }

        /// <summary>
        /// the search algorithm
        /// </summary>
        /// <param name="searchable">the ISearchable to search with</param>
        /// <returns>the solution from the search</returns>
        public override ISolution<T, K> search(ISearchable<T, K> searchable)
        {
            // Searcher's abstract method overriding
            addOpenList(searchable.getInitialState()); // inherited from Searcher
            double stateIndex = 0;
            State<T> goalState = searchable.getGoalState();
            State<T> currentState = popOpenList();  // inherited from Searcher, removes the best state
            while (!currentState.Equals(goalState))
            {
                
                List<State<T>> succerssors = searchable.getAllPossibleStates(currentState);
                closed.Add(currentState);
                foreach (State<T> s in succerssors)
                {
                    stateIndex++;
                    if (!closed.Contains(s) && !openContains(s))
                    {
                        s.CameFrom = currentState;
                        s.Cost = stateIndex;
                        addOpenList(s);
                    }
                }
                currentState = popOpenList();
            }
            return BackTrace(searchable, currentState); // private method, back traces through the parents
        }

        /// <summary>
        /// this function used in the end of the search process.
        /// extracting list of states from the goal state to the start, 
        /// and getting the solved object according to it.
        /// </summary>
        /// <param name="searchable"> the ISearchable object</param>
        /// <param name="goalState">the goal state</param>
        /// <returns>the solution that the algorithm found</returns>
        private ISolution<T, K> BackTrace(ISearchable<T, K> searchable, State<T> goalState)
        {
            List<State<T>> mySolList = new List<State<T>>();
            State<T> currentState = goalState;
            while (currentState != null)
            {
                mySolList.Add(currentState);
                currentState = currentState.CameFrom;
            }

            //applying the solution
            ISolution<T, K> fromSearchable = searchable.getInitialSolution();
            fromSearchable.applySolution(mySolList);

            return fromSearchable;
        }
    }
}
