﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// generates a K object in random method
    /// </summary>
    /// <typeparam name="T">the object of State</typeparam>
    /// <typeparam name="K">the object it generates</typeparam>
    public class RandomMatrixAlgorithm<T, K>: Generator<T, K>
    {
        /// <summary>
        /// generates the K object
        /// </summary>
        /// <param name="generable">the relevant IGenerable</param>
        /// <returns>the generated K object</returns>
        public override K Generate(IGenerable<T, K> generable)
        {
            //getting random initial state from generable
            State<T> initial = generable.getInitialState();
            //initialize cost of start to 0
            initial.Cost = 0;
            List<State<T>> closedList = new List<State<T>>();
            List<State<T>> possibleStates;
            State<T> randStateFromClosedList, randStateToAdd;
            //adding the start state to the closed list
            closedList.Add(initial);
            Random rnd = new Random();
            int randomNum;
            int numOfFinalStates = generable.stateLimit();

            while (numOfFinalStates != closedList.Count)
            {
                do
                {
                    randomNum = rnd.Next(closedList.Count);
                    randStateFromClosedList = closedList.ElementAt(randomNum);
                    possibleStates = generable.getPossibleStates(randStateFromClosedList);
                }
                //the cell has not a vacant neighbor
                while (possibleStates.Count == 0);

                //if after the loop flag=true it means that i have neighbor to add

                List<State<T>> tempPossibleStates = new List<State<T>>(possibleStates);
                foreach (State<T> s in tempPossibleStates)
                {
                    if (closedList.Contains(s))
                    {
                        possibleStates.Remove(s);
                    }
                }

                if (possibleStates.Count != 0)
                {
                    randomNum = rnd.Next(possibleStates.Count);

                    randStateToAdd = possibleStates.ElementAt(randomNum);
                    //editing the state that going to add to the group/ his cost and the state he came from it.
                    randStateToAdd.CameFrom = randStateFromClosedList;
                    randStateToAdd.Cost = randStateFromClosedList.Cost + 1;
                    closedList.Add(randStateToAdd);
                    //informer generable that this state is selected to do
                    generable.stateIsSelected(randStateToAdd);
                }
            }

            State<T> maxState = initial;
            double max = 0;

            foreach(State<T> s in closedList)
            {
                if (s.Cost > max)
                {
                    max = s.Cost;
                    maxState = s;
                }
            }

            generable.updateGoalState(maxState);

            return generable.getObject();
        }
    
    }
}
