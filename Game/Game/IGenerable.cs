﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// an interface for objects that communicates with the generation algorithms
    /// </summary>
    /// <typeparam name="T">the object of state</typeparam>
    /// <typeparam name="K">the object to generate</typeparam>
    public interface IGenerable<T, K>
    {
        /// <summary>
        /// returns the initial state of the generation
        /// </summary>
        /// <returns>the start state</returns>
        State<T> getInitialState();
        /// <summary>
        /// updating the end point in the object
        /// </summary>
        /// <param name="s">the goal state</param>
        void updateGoalState(State<T> s);
        /// <summary>
        /// getting the successors of a state
        /// </summary>
        /// <param name="s">the state to use</param>
        /// <returns>the successors</returns>
        List<State<T>> getPossibleStates(State<T> s);
        /// <summary>
        /// return if the state is already selected
        /// </summary>
        /// <param name="s">a state</param>
        void stateIsSelected(State<T> s);
        /// <summary>
        /// return the max number of possible states
        /// </summary>
        /// <returns>max number of possible states</returns>
        int stateLimit();
        /// <summary>
        /// return the generated object
        /// </summary>
        /// <returns>the object</returns>
        K getObject();
    }
}
