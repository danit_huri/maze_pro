﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace Game
{
    /// <summary>
    /// IGenerable for Maze objects
    /// </summary>
    public class GenerableMaze: IGenerable<Cell, Maze>
    {
        /// <summary>
        /// the maze to initialize
        /// </summary>
        private Maze maze;

        /// <summary>
        /// constructor
        /// </summary>
        public GenerableMaze()
        {
            //getting the size from the appconfig file
            int N = int.Parse(ConfigurationManager.AppSettings["N"]);
            int M = int.Parse(ConfigurationManager.AppSettings["M"]);
            
            this.maze = new Maze(N, M);
        }

        /// <summary>
        /// returns the initial state of the generation.
        /// </summary>
        /// <returns>the start state</returns>
        public State<Cell> getInitialState() {
            //choosing a random cell from the maze to start with
            Random rnd = new Random();
            int x = rnd.Next(maze.N);
            int y = rnd.Next(maze.M);
            State<Cell> initialNode = new State<Cell>(this.maze.getCell(x, y));
            this.maze.Start = this.maze.getCell(x, y);
            return initialNode;
        }

        /// <summary>
        /// return the max number of possible states, according to the size of the maze
        /// </summary>
        /// <returns>max number of possible states</returns>
        public int stateLimit()
        {
            return this.maze.M * this.maze.N;
        }

        /// <summary>
        /// updating the end point in the maze
        /// </summary>
        /// <param name="s">the goal state</param>
        public void updateGoalState(State<Cell> s)
        {
            maze.End = s.getState();
            maze.generateMatrixMaze();
        }

        /// <summary>
        /// getting the successors of a state
        /// </summary>
        /// <param name="s">the state to use</param>
        /// <returns>the successors</returns>
        public List<State<Cell>> getPossibleStates(State<Cell> s)
        {
            Cell cell = s.getState();
            List<State<Cell>> posibleStates = new List<State<Cell>>();
            if (cell.isHasWallUp())
            {
                State<Cell> posibleState = new State<Cell>(maze.getCell(cell.Row - 1, cell.Col));
                posibleStates.Add(posibleState);
            }
            if (cell.isHasWallDown())
            {
                State<Cell> posibleState = new State<Cell>(maze.getCell(cell.Row + 1, cell.Col));
                posibleStates.Add(posibleState);
            }

            if (cell.isHasWallRight())
            {
                State<Cell> posibleState = new State<Cell>(maze.getCell(cell.Row, cell.Col + 1));
                posibleStates.Add(posibleState);
            }

            if (cell.isHasWallLeft())
            {
                State<Cell> posibleState = new State<Cell>(maze.getCell(cell.Row, cell.Col - 1));
                posibleStates.Add(posibleState);
            }

            return posibleStates;
        }

        /// <summary>
        /// return if the state is already selected
        /// </summary>
        /// <param name="s">a state</param>
        public void stateIsSelected(State<Cell> s)
        {
            Cell selectedCell = s.getState();
            Cell cameFromCell = s.CameFrom.getState();
            if (selectedCell.Row == cameFromCell.Row + 1)
            {
                selectedCell.SetUpPass();
                cameFromCell.SetDownPass();
            }
            if (selectedCell.Row == cameFromCell.Row - 1)
            {
                selectedCell.SetDownPass();
                cameFromCell.SetUpPass();
            }
            if (selectedCell.Col == cameFromCell.Col - 1)
            {
                selectedCell.SetRightPass();
                cameFromCell.SetLeftPass();
            }
            if (selectedCell.Col == cameFromCell.Col + 1)
            {
                selectedCell.SetLeftPass();
                cameFromCell.SetRightPass();
            }
        }

        /// <summary>
        /// return the generated maze
        /// </summary>
        /// <returns>the maze</returns>
        public Maze getObject()
        {
            return maze;
        }

        
    }
}
