﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// abstract class for generation algorithms
    /// </summary>
    /// <typeparam name="T">the object of State</typeparam>
    /// <typeparam name="K">the object it generates</typeparam>
    public abstract class Generator<T, K>:IAlgorithm
    {
        /// <summary>
        /// generates the K object
        /// </summary>
        /// <param name="generable">the relevant IGenerable</param>
        /// <returns>the generated K object</returns>
        public abstract K Generate(IGenerable<T, K> generable);
    }
}
