﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// generates a K object in DFS method
    /// </summary>
    /// <typeparam name="T">the object of State</typeparam>
    /// <typeparam name="K">the object it generates</typeparam>
    public class DepthFirstSearchAlgorithm<T, K> : Generator<T, K>
    {
        /// <summary>
        /// generates the K object
        /// </summary>
        /// <param name="generable">the relevant IGenerable</param>
        /// <returns>the generated K object</returns>
        public override K Generate(IGenerable<T, K> generable)
        {
            //getting random initial state from generable
            State<T> initial = generable.getInitialState();
            State<T> currentState = initial;
            //initialize cost of start to 0
            initial.Cost = 0;
            Stack<State<T>> stateStack = new Stack<State<T>>();
            List<State<T>> closedList = new List<State<T>>();
            List<State<T>> possibleStates;
            State<T> randStateToAdd;
            //adding the start state to the closed list
            closedList.Add(initial);
            Random rnd = new Random();
            int randomNum;
            int numOfFinalStates = generable.stateLimit();

            while (numOfFinalStates != closedList.Count)
            {

                possibleStates = generable.getPossibleStates(currentState);

                //removing the visited states
                List<State<T>> tempPossibleStates = new List<State<T>>(possibleStates);
                foreach (State<T> s in tempPossibleStates)
                {
                    if (closedList.Contains(s))
                    {
                        possibleStates.Remove(s);
                    }
                }

                //if the current state has possible following states
                if (possibleStates.Count != 0)
                {
                    randomNum = rnd.Next(possibleStates.Count);

                    randStateToAdd = possibleStates.ElementAt(randomNum);
                    //editing the state that going to add to the group/ his cost and the state he came from it.
                    randStateToAdd.CameFrom = currentState;
                    randStateToAdd.Cost = currentState.Cost + 1;
                    closedList.Add(randStateToAdd);
                    stateStack.Push(currentState);
                    currentState = randStateToAdd;
                    //informer generable that this state is selected to do
                    generable.stateIsSelected(randStateToAdd);
                } else if(stateStack.Count != 0) // else if stack is not empty
                {
                    currentState = stateStack.Pop();
                }
            }

            State<T> maxState = initial;
            double max = 0;

            foreach (State<T> s in closedList)
            {
                if (s.Cost > max)
                {
                    max = s.Cost;
                    maxState = s;
                }
            }

            generable.updateGoalState(maxState);

            return generable.getObject();
        }
    }

}
