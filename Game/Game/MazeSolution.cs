﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// solution of type: cell, maze
    /// </summary>
    public class MazeSolution: ISolution<Cell, Maze>
    {
        /// <summary>
        /// maze to hold the solution
        /// </summary>
        private Maze maze;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="m">the maze to apply the solution on</param>
        public MazeSolution(Maze m)
        {
            this.maze = new Maze(m);
        }

        /// <summary>
        /// applying the solution according to the resolt of states from an outside algorithm
        /// </summary>
        /// <param name="sol">the list of states to apply</param>
        public void applySolution(List<State<Cell>> sol)
        {
            foreach (State<Cell> scell in sol)
            {
                Cell currentCell = scell.getState();
                maze.setMatrixCell(currentCell.Row *2, currentCell.Col*2, 2);
                State<Cell> parentState = scell.CameFrom;
                if (parentState != null) {
                    Cell parent = parentState.getState();
                    if (currentCell.Row > parent.Row)
                    {
                        maze.setMatrixCell((currentCell.Row * 2) - 1, currentCell.Col * 2, 2);
                    } else if (currentCell.Row < parent.Row)
                    {
                        maze.setMatrixCell((currentCell.Row * 2) + 1, currentCell.Col * 2, 2);
                    } else if (currentCell.Col > parent.Col)
                    {
                        maze.setMatrixCell((currentCell.Row * 2) , (currentCell.Col * 2) - 1, 2);
                    } else if (currentCell.Col < parent.Col)
                    {
                        maze.setMatrixCell((currentCell.Row * 2) , (currentCell.Col * 2) + 1, 2);
                    }
                }
            }
        }

        /// <summary>
        /// return the object with the solution in it
        /// </summary>
        /// <returns>the object ith solution</returns>
        public Maze getObject()
        {
            return this.maze;
        }

    }
}
