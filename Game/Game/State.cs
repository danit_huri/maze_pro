﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// this object is for States in algorithms
    /// </summary>
    /// <typeparam name="T">object of the state</typeparam>
    public class State<T>
    {
        private T state;       // the state represented by a T
        private double cost;// cost to reach this state (set by a setter)
        private State<T> cameFrom;  // the state we came from to this state (setter)

        /// <summary>
        /// cpnstructor
        /// </summary>
        /// <param name="state"> the object of the state </param>
        public State(T state)
        {
            this.state = state;
        }

        /// <summary>
        /// getter for the state object
        /// </summary>
        /// <returns>this state object</returns>
        public T getState()
        {
            return this.state;
        }

        /// <summary>
        /// getter and setter for the cost member
        /// </summary>
        public double Cost
        {
            get { return this.cost; }
            set { this.cost = value; }
        }

        /// <summary>
        /// getter and setter for the cameFrom member
        /// </summary>
        public State<T> CameFrom
        {
            get { return this.cameFrom; }
            set { this.cameFrom = value; }
        }

        /// <summary>
        /// overriding the equal method. 
        /// this function return equality if the objects of the states are equal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) // we override Object's Equals method
        {
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            State<T> s = obj as State<T>;
            if ((System.Object)s == null)
            {
                return false;
            }

            // Return true if the fields match:
            return s.state.Equals(this.state);
           
        }

    }
}
