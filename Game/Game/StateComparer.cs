﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// comparer for State<T> objects
    /// </summary>
    /// <typeparam name="T">the object of the State</typeparam>
    public class StateComparer<T> : IComparer<State<T>>
    {
        /// <summary>
        /// this function compare between 2 States according to their costs
        /// </summary>
        /// <param name="x">State 1</param>
        /// <param name="y">State 2</param>
        /// <returns>0 if equal, -1 if x smaller than y, and 1 otherwise</returns>
        public int Compare(State<T> x, State<T> y)
        {
            if (x.Cost == y.Cost)
                return 0;
            else if (x.Cost < y.Cost)
                return -1;
            else
                return 1;
        }
    }
}
