﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// Cell object. can be a cell of any type of grid
    /// </summary>
   public class Cell
    {
        // up-0 right-1 down-2 left-3
        // 0-no edge 1-is edge 2-frame
        private int[] positions { set; get; }
        //position
        private int row;
        private int col;

        /// <summary>
        /// constructor. for empty cell
        /// </summary>
        public Cell() { }
        /// <summary>
        /// constructor. and initialize
        /// </summary>
        /// <param name="x">row position</param>
        /// <param name="y">column position</param>
        public Cell(int x, int y)
        {
            //initialize walls - setting 4 walls to this cell
            positions = new int[4];
            for (int i = 0; i < 4; i++)
                positions[i] = 1;
            this.row = x;
            this.col = y;
        }

        /// <summary>
        /// constructor. deep copy
        /// </summary>
        /// <param name="cell">the cell to copy</param>
        public Cell(Cell cell)
        {
            positions = new int[4];
            for (int i = 0; i < 4; i++)
                positions[i] = (cell.getPositions())[i];
            this.row = cell.Row;
            this.col = cell.Col;

        }

        /// <summary>
        /// getter and setter for row member
        /// </summary>
        public int Row
        {
            get { return this.row; }
            set { this.row = value; }
        }

        /// <summary>
        /// getter and setter for col member
        /// </summary>
        public int Col
        {
            get { return this.col; }
            set { this.col = value; }
        }

        /// <summary>
        /// return the positions array
        /// </summary>
        /// <returns>positions</returns>
        public int[] getPositions()
        {
            return this.positions; 
        }

        /// <summary>
        /// setting the upper side as frame
        /// </summary>
        public void SetUpFrame()
         {
            this.positions[0] = 2;
         }

        /// <summary>
        /// setting the bottom side as frame
        /// </summary>
        public void SetDownFrame()
        {
            this.positions[2] = 2;
        }

        /// <summary>
        /// setting the right side as frame
        /// </summary>
        public void SetRightFrame()
        {
            this.positions[1] = 2;
        }

        /// <summary>
        /// setting the left side as frame
        /// </summary>
        public void SetLeftFrame()
        {
            this.positions[3] = 2;
        }

        /// <summary>
        /// setting the upper side as passage
        /// </summary>
        public void SetUpPass()
        {
            this.positions[0] = 0;
        }

        /// <summary>
        /// setting the bottom side as passage
        /// </summary>
        public void SetDownPass()
        {
            this.positions[2] = 0;
        }

        /// <summary>
        /// setting the right side as passage
        /// </summary>
        public void SetRightPass()
        {
            this.positions[1] = 0;
        }

        /// <summary>
        /// setting the left side as passage
        /// </summary>
        public void SetLeftPass()
        {
            this.positions[3] = 0;
        }

        /// <summary>
        /// setting the upper side as wall
        /// </summary>
        public void SetUpWall()
        {
            this.positions[0] = 1;
        }

        /// <summary>
        /// setting the bottom side as wall
        /// </summary>
        public void SetDownWall()
        {
            this.positions[2] = 1;
        }

        /// <summary>
        /// setting the right side as wall
        /// </summary>
        public void SetRightWall()
        {
            this.positions[1] = 1;
        }

        /// <summary>
        /// setting the left side as wall
        /// </summary>
        public void SetLeftWall()
        {
            this.positions[3] = 1;
        }

        /// <summary>
        /// return if the upper side is a passage
        /// </summary>
        /// <returns>if the upper side is a passage</returns>
        public bool canPassUp()
        {
            if (this.positions[0] == 0)
                return true;
            else return false;
        }

        /// <summary>
        /// return if the bottom side is a passage
        /// </summary>
        /// <returns>if the bottom side is a passage</returns>
        public bool canPassDown()
        {
            if (this.positions[2] == 0)
                return true;
            else return false;
        }

        /// <summary>
        /// return if the right side is a passage
        /// </summary>
        /// <returns>if the right side is a passage</returns>
        public bool canPassRight()
        {
            if (this.positions[1] == 0)
                return true;
            else return false;
        }

        /// <summary>
        /// return if the left side is a passage
        /// </summary>
        /// <returns>if the left side is a passage</returns>
        public bool canPassLeft()
        {
            if (this.positions[3] == 0)
                return true;
            else return false;
        }

        /// <summary>
        /// return if the upper side is a wall
        /// </summary>
        /// <returns>if the upper side is a wall</returns>
        public bool isHasWallUp()
        {
            if (this.positions[0] == 1)
                return true;
            else return false;
        }

        /// <summary>
        /// return if the bottom side is a wall
        /// </summary>
        /// <returns>if the bottom side is a wall</returns>
        public bool isHasWallDown()
        {
            if (this.positions[2] == 1)
                return true;
            else return false;
        }

        /// <summary>
        /// return if the right side is a wall
        /// </summary>
        /// <returns>if the right side is a wall</returns>
        public bool isHasWallRight()
        {
            if (this.positions[1] == 1)
                return true;
            else return false;
        }

        /// <summary>
        /// return if the left side is a wall
        /// </summary>
        /// <returns>if the left side is a wall</returns>
        public bool isHasWallLeft()
        {
            if (this.positions[3] == 1)
                return true;
            else return false;
        }

        /// <summary>
        /// calculates distance from another cell in on a grid
        /// </summary>
        /// <param name="other">the other cell</param>
        /// <returns>the distance</returns>
        public int distanceFrom(Cell other)
        {
            return ((Math.Abs(this.row - other.row)) - (Math.Abs(this.col - other.col)));

        }

        /// <summary>
        /// returns if this cell equals to the given object (a cell)
        /// checks for equality according to the position of the cell in the grid.
        /// </summary>
        /// <param name="obj">the other cell</param>
        /// <returns>if equal</returns>
        public override bool Equals(object obj) // we override Object's Equals method
        {
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Cell c = obj as Cell;
            if ((System.Object)c == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (c.Row == this.row) && (c.Col == this.col);

        }
    }
}
