﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// an interface for objects that communicates with the search algorithms
    /// </summary>
    /// <typeparam name="T">the object of state</typeparam>
    /// <typeparam name="K">the object to search on</typeparam>
    public interface ISearchable<T, K>
    {
        /// <summary>
        /// returns the initial state of the searching
        /// </summary>
        /// <returns>the start state</returns>
        State<T> getInitialState();
        /// <summary>
        /// returns the goal state for the search
        /// </summary>
        /// <returns>the goal state</returns>
        State<T> getGoalState();
        /// <summary>
        /// getting the successors of a state
        /// </summary>
        /// <param name="s">the state to use</param>
        /// <returns>the successors</returns>
        List<State<T>> getAllPossibleStates(State<T> s);
        /// <summary>
        /// return new ISolution for this T,K objects
        /// </summary>
        /// <returns>the new solution</returns>
        ISolution<T, K> getInitialSolution();
        /// <summary>
        /// return the object
        /// </summary>
        /// <returns>the object</returns>
        K getObject();
    }
}
