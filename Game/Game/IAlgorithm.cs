﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    /// <summary>
    /// general interface to indicate that a class is an algorithm
    /// </summary>
    public interface IAlgorithm
    {
    }
}
