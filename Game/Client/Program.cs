﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Client
{
    /// <summary>
    /// main class for the client program
    /// </summary>
    class Program
    {
        /// <summary>
        /// main method of the client project
        /// </summary>
        /// <param name="args">user command input</param>
        static void Main(string[] args)
        {
            //creating TCP connection with using the appconfig file settings
            string ip = ConfigurationManager.AppSettings["IP"];
            int portNumber = int.Parse(ConfigurationManager.AppSettings["PORT"]);
            TCPClient tcpConnection = new TCPClient();
            tcpConnection.connectServer(ip, portNumber);
        }
       
    }
}
