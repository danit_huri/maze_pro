﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Client
{
    /// <summary>
    /// handles the TCP connection sending data to server
    /// </summary>
    public class TCPSendHandler
    {
        /// <summary>
        /// socket of the server
        /// </summary>
        private Socket server;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="sock">the socket of the server</param>
        public TCPSendHandler(Socket sock)
        {
            server = sock;
        }

        /// <summary>
        /// send masseges to the server, when "exit" massege sent we close the connection.
        /// </summary>
        public void handle()
        {
            while (true)
            {
                string input = Console.ReadLine();
                if (input == "exit")
                {
                    break;
                }
                server.Send(Encoding.ASCII.GetBytes(input));   
            }
            server.Shutdown(SocketShutdown.Both);
            server.Close();
        }
    }
}
