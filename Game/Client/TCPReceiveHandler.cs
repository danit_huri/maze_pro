﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading.Tasks;

namespace Client
{
    /// <summary>
    /// handles the TCP connection receiving data from server
    /// </summary>
    public class TCPReceiveHandler
    {
        /// <summary>
        /// socket of the server
        /// </summary>
        private Socket server;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="sock">the socket of the server</param>
        public TCPReceiveHandler (Socket sock)
        {
            server = sock;
        }

        /// <summary>
        /// receive masseges from the server
        /// </summary>
        public void handle()
        {
            while (true){
                byte[] data = new byte[65536];
                int recv = server.Receive(data);
                string stringData = Encoding.ASCII.GetString(data, 0, recv);
                Console.WriteLine(stringData);
            }
        }
    }
}
