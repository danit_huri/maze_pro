﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Configuration;

namespace Client
{
    /// <summary>
    /// TCP connection socket and tasks
    /// </summary>
    public class TCPClient
    {
        /// <summary>
        /// connecting to the server
        /// </summary>
        /// <param name="IP">ip of the server</param>
        /// <param name="portNumber">pport number of the server</param>
        public void connectServer(string IP, int portNumber)
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(IP), portNumber);
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                server.Connect(ipep);
                TCPReceiveHandler reciever = new TCPReceiveHandler(server);
                TCPSendHandler sender = new TCPSendHandler(server);
                //tasks of the client for sending some commands one by one
                List<Task> myTasks = new List<Task>();
                myTasks.Add(Task.Factory.StartNew(reciever.handle));
                myTasks.Add(Task.Factory.StartNew(sender.handle));
                //wating all the trhreads to finish
                Task.WaitAll(myTasks.ToArray());
            }
            //erroe in connection
            catch (SocketException e) {
                Console.WriteLine("Unable to connect to server." + e.ToString());
            }

        }
    }
}
